<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	if (email != null && email != "") {
		url = url + '&email=' + email;
		window.location.href=url;
	}
}
</script>
<?php include("menu_geral.php"); ?>
<br>
<br>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>&Aacute;reas
              Geogr&aacute;ficas </strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <table>
	  <tr align="center"><td width="548"  valign="top" align="center">
	  <?php if (isset($busca)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=AG1&acao=IM&busca=<?= isset($busca) ? $busca : ""; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=AG1&acao=EM&busca=<?= isset($busca) ? $busca : ""; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	  <?php } else if (isset($area)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=AG2&acao=IM&areaDesc=<?php echo $areaDesc; ?>&area=<?php echo $area; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=AG2&acao=EM&areaDesc=<?php echo $areaDesc; ?>&area=<?php echo $area; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	   <?php } ?>
      </td></tr>
     </table>
      <br>
      <br>
	   <form name="form1" method="post" action="areas_geograficas.php">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr>
          <td><p><strong>Busca simples</strong> - digite
              o nome da &aacute;rea no campo abaixo</p></td>
        </tr>
        <tr>
          <td height="1" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td>
              <input name="busca" type="text" id="busca" size="80" maxlength="255" value="<?= isset($busca) ? $busca : ""; ?>">
              <input name="Submit" type="submit" class="botaoLogin" value="Buscar" class="btn btn-success"> </td>
        </tr>
      </table>
      </form>
<?php
if (isset($busca)) {

	$busca = strtoupper($busca);

	function untree($parent, $level) {
		$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"areas_geograficas.php?areaDesc=$branch->ag_descricao&area=$branch->ag_id\" class=\"preto\">$branch->ag_descricao</a><br>";
	            echo $echo_this;
	            $rename_level = $level;
    	        untree($branch->ag_id, ++$rename_level);
            }
        }
    }
	$compsql = $db->query("SELECT * FROM area_geografica WHERE UCASE(ag_descricao) LIKE '$busca%' AND ag_pertence=0 ORDER BY ag_descricao");
	echo "<p class=\"textoPreto10px\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
	$i = 1;
	while ($ag = $compsql->fetch_object()) {
		echo "<br>$i. <a href=\"areas_geograficas.php?areaDesc=$ag->ag_descricao&area=$ag->ag_id\" class=\"preto\"><strong>$ag->ag_descricao</strong></a><br>";
	    untree($ag->ag_id, 1);
		$i++;
	}
	echo "</p>";

} // fim do if que verifica se uma busca foi feita

if (isset($area)) {

	$sql = "SELECT * FROM area_geografica,rel_geo_experiencia,experiencia WHERE ag_id='$area' AND ag_id=rge_id_geo ".
		   "AND ex_id=rge_id_experiencia AND ex_liberacao='S'";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes experiências para a �rea <b>\"$areaDesc\"</b>:<br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";

			$i++;

		}

		echo "</p>";

	}
	else {

		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta �rea!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados

} // fim do if que verifica se o usuário clicou em alguma �rea geogr�fica
?>

	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
