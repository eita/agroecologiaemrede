<?php
// por daniel tygel (dtygel@fbes.org.br) em 2009
// last update: 2021

mb_internal_encoding("UTF-8");
require_once "../mapeo/config.php";
require_once "../mapeo/api_legacy.php";
require_once "{$dir['apoio']}funcoes_comuns.php";
$db = conecta($bd);


if ($_GET['runScripts']) {
	// $sql = "SELECT * FROM mapeo_mapa_campos WHERE frm='frm_instituicao'";
	// faz_query($sql);
	echo 'uuuuuuuuuuuuuuuppppppppppddddddddddddddaaaaaaaaaaaattttttttteeeeeeeeeddddddd';exit;
}

$visao=isset($_REQUEST['visao']) ? $_REQUEST['visao'] : 'tabela';
$mostratabela = ($visao=='tabela');
$lingua = ($_REQUEST['lingua']) ? $_REQUEST['lingua'] : $_COOKIE['agrorede_lingua'];
if (!$lingua) {
	$lingua = $padrao_lingua;
}
$conta=isset($_REQUEST['conta']) ? $_REQUEST['conta'] : '';
$resgataTotaisTipofrms = $conta && isset($_REQUEST['resgataTipofrms']) && $_REQUEST['resgataTipofrms'];
$resgataTotaisTemas = $conta && isset($_REQUEST['resgataTemas']) && $_REQUEST['resgataTemas'];
$resgataTotaisMapeamentos = $conta && isset($_REQUEST['resgataMapeamentos']) && $_REQUEST['resgataMapeamentos'];
$resgataUltimas = isset($_REQUEST['resgataUltimas']) ? $_REQUEST['resgataUltimas'] : '';
if (!$conta && !$resgataUltimas) {
	if (isset($_REQUEST['resgataTemas']) && $_REQUEST['resgataTemas']) {
		faz_header('json');
		$sql = "SELECT * FROM lista_areas_tematicas ORDER BY nome";
		$json = faz_query($sql, '', 'object');
		echo json_encode($json);
		exit;
	} else if (isset($_REQUEST['resgataTipofrms']) && $_REQUEST['resgataTipofrms']) {
		faz_header('json');
		$sql = "SELECT DISTINCT tipo, tipo_nome FROM mapeo_formularios ORDER BY tipo_nome";
		$json = faz_query($sql, '', 'object');
		echo json_encode($json);
		exit;
	} else if (isset($_REQUEST['resgataMapeamentos']) && $_REQUEST['resgataMapeamentos']) {
		faz_header('json');
		$sql = "SELECT id, nome FROM lista_mapeamentos a WHERE publica=1 ORDER BY id";
		$json = faz_query($sql, '', 'object');
		echo json_encode($json);
		exit;
	}
} else if ($resgataTotaisTipofrms || $resgataTotaisTemas || $resgataTotaisMapeamentos || $resgataUltimas) {
	$_REQUEST['tipofrm'] = 'todos';
}

$tipofrms = [];
$tipofrms_nome = [];
$nomefrm = $_REQUEST['nomeFrm'];

if (!defined('EMBEDADO')) {
	define('EMBEDADO', isset($_REQUEST['embedado']));
}
$lingua = ($_REQUEST['lingua'])
	? $_REQUEST['lingua']
	: $_COOKIE['agrorede_lingua'];
if (!$lingua) {
	$lingua = $padrao_lingua;
}

if ($nomefrm) {
	$sql = "SELECT tipo, tipo_nome FROM mapeo_formularios WHERE nomefrm = '".$nomefrm."' LIMIT 1";
	$res = faz_query($sql,'','object');
	$tipofrms = [$res[0]->tipo];
	$tipofrms_nome[$res[0]->tipo] = traduz($res[0]->tipo_nome, $lingua);
} else if (isset($_REQUEST['tipofrm']) && $_REQUEST['tipofrm'] == 'todos') {
	$sql = "SELECT DISTINCT tipo, tipo_nome FROM mapeo_formularios";
	$res = faz_query($sql,'','object');
	foreach ($res as $r) {
		$tipofrms[] = $r->tipo;
		$tipofrms_nome[$r->tipo] = traduz($r->tipo_nome, $lingua);
	}
} else {
	$tipofrms = (isset($_REQUEST['tipofrm']))
		? explode('|', $_REQUEST['tipofrm'])
		: ['experiencia'];
	$sql = "SELECT DISTINCT tipo, tipo_nome FROM mapeo_formularios WHERE tipo IN ('" . implode("', '", $tipofrms) . "')";
	$res = faz_query($sql,'','object');
	foreach ($res as $r) {
		$tipofrms[] = $r->tipo;
		$tipofrms_nome[$r->tipo] = traduz($r->tipo_nome, $lingua);
	}
}

$outputFormat = (isset($_REQUEST['outputFormat']) && in_array($_REQUEST['outputFormat'], array('xml','json','html')))
	? $_REQUEST['outputFormat']
	: 'xml';
$idpt = isset($_REQUEST['idPt'])
	? $_REQUEST['idPt']
	: '';
$id = isset($_REQUEST['id'])
	? $_REQUEST['id']
	: '';
$datai = isset($_REQUEST['datai'])
	? $_REQUEST['datai']
	: '';
$dataf = isset($_REQUEST['dataf'])
	? $_REQUEST['dataf']
	: '';
$mapeamento = isset($_REQUEST['mapeamento'])
	? $_REQUEST['mapeamento']
	: '';
$tipodata = isset($_REQUEST['tipodata'])
	? $_REQUEST['tipodata']
	: '';
$pgAtual = isset($_REQUEST['pgLista'])
	? $_REQUEST['pgLista']
	: 1;
$orderby = isset($_REQUEST['orderby'])
	? $_REQUEST['orderby']
	: 'nome';
$order = isset($_REQUEST['order'])
	? ' '.$_REQUEST['order']
	: '';
$resgataFiltros = isset($_REQUEST['resgataFiltros'])
	? $_REQUEST['resgataFiltros']
	: '';
if ($resgataFiltros) {
	$outputFormat = 'json';
}

$filtroGenerico = resgata_filtros('dispositivo', true);
$geos = explode('|',$_REQUEST[$campo[$filtroGenerico->tab]->localizacao]);
if ($geos) {
	rsort($geos);
	$geo = organiza_geos($geos[0],$_REQUEST['distmax'],$lingua);
}
$f=isset($_REQUEST['f'])
	? $_REQUEST['f']
	: '';
$include_images = isset($_REQUEST['include_images'])
	? $_REQUEST['include_images']
	: true;

$sql = "SELECT * FROM mapeo_formularios WHERE tipo IN ('" . implode("', '", $tipofrms) . "')";
if ($filtroGenerico->sels['nomefrm']) {
	$sql .= " AND nomefrm IN (";
	foreach ($filtroGenerico->sels['nomefrm'] as $o)
		$sql .= "'".$o."',";
	$sql = substr($sql,0,-1).")";
}
$frms = faz_query($sql,'','object');
unset($filtroGenerico->sels['nomefrm'],$filtroGenerico->mapa['nomefrm']);

// Casos sem filtros (totais):
if ($resgataTotaisTipofrms || $resgataTotaisTemas || $resgataTotaisMapeamentos || $resgataUltimas) {
	if ($resgataUltimas) {
		$json = new stdClass();
		foreach ($frms as $frm) {
			if (!in_array($frm->nomefrm, ['frm_exp_cca', 'frm_exp_geral'])) {
				$sql = "SELECT a.{$campo[$frm->nomefrm]->id} as id, a.{$campo[$frm->nomefrm]->criacao} as criacao FROM `{$frm->nomefrm}` a, `lista_mapeamentos` b WHERE a.{$campo[$frm->nomefrm]->mapeamento}=b.id AND b.publica=1 AND `{$campo[$frm->nomefrm]->status}`!='R' ORDER BY a.`{$campo[$frm->nomefrm]->criacao}` DESC limit 1";
				$res = faz_query($sql, '', 'object');
				$json->{$frm->tipo} = $res[0];
			}
		}
	} else {
		$sql = "SELECT id FROM lista_areas_tematicas ORDER BY nome";
		$areas_tematicas = faz_query($sql, '', 'array')[0];
		$sql = "SELECT id FROM lista_mapeamentos ORDER BY nome";
		$mapeamentos = faz_query($sql, '', 'array')[0];
		$json = [];
		if ($resgataTotaisTipofrms) {
			foreach ($tipofrms as $k) {
				$t = new stdClass();
				$t->total = 0;
				$t->porTema = array_fill_keys($areas_tematicas, 0);
				$t->porMapeamento = array_fill_keys($mapeamentos, 0);
				$json[$k] = $t;
			}
		} else if ($resgataTotaisTemas) {
			foreach ($areas_tematicas as $k) {
				$t = new stdClass();
				$t->total = 0;
				$t->porTipofrm = array_fill_keys($tipofrms, 0);
				$t->porMapeamento = array_fill_keys($mapeamentos, 0);
				$json[$k] = $t;
			}
		} else if ($resgataTotaisMapeamentos) {
			foreach ($mapeamentos as $k) {
				$t = new stdClass();
				$t->total = 0;
				$t->porTipofrm = array_fill_keys($tipofrms, 0);
				$t->porTema = array_fill_keys($areas_tematicas, 0);
				$json[$k] = $t;
			}
		}
		foreach ($frms as $frm) {
			$from = "`{$frm->nomefrm}`, `lista_mapeamentos`";
			$where = "`publicada`=1 AND {$campo[$frm->nomefrm]->status}<>'R' AND {$campo[$frm->nomefrm]->mapeamento}=`lista_mapeamentos`.id AND `lista_mapeamentos`.publica=1";
			if ($frm->tab_base_comum) {
				$from .= ", {$frm->tab_base_comum}";
				$where .= " AND {$frm->nomefrm}.{$campo[$frm->nomefrm]->id} = {$frm->tab_base_comum}.{$campo[$frm->tab_base_comum]->id}";
			}
			$sql = "SELECT {$campo[$frm->nomefrm]->mapeamento} as mapeamento, {$campo[$frm->nomefrm]->areas_tematicas} as areas_tematicas FROM {$from} WHERE {$where}";
			$dados = faz_query($sql, '', 'object');
			foreach ($dados as $d) {
				$dAreasTematicas = explode('|', $d->areas_tematicas);
				if ($resgataTotaisTipofrms) {
					$json[$frm->tipo]->total++;
					$json[$frm->tipo]->porMapeamento[$d->mapeamento]++;
					foreach ($dAreasTematicas as $dAreaTematica) {
						if (in_array($dAreaTematica, $areas_tematicas)) { //TODO: mapear antigos temas para novos
							$json[$frm->tipo]->porTema[$dAreaTematica]++;
						}
					}
				} else if ($resgataTotaisTemas) {
					foreach ($dAreasTematicas as $dAreaTematica) {
						if (in_array($dAreaTematica, $areas_tematicas)) { //TODO: mapear antigos temas para novos
							$json[$dAreaTematica]->total++;
							$json[$dAreaTematica]->porTipofrm[$frm->tipo]++;
							$json[$dAreaTematica]->porMapeamento[$d->mapeamento]++;
						}
					}
				} else if ($resgataTotaisMapeamentos) {
					$json[$d->mapeamento]->total++;
					$json[$d->mapeamento]->porTipofrm[$frm->tipo]++;
					foreach ($dAreasTematicas as $dAreaTematica) {
						if (in_array($dAreaTematica, $areas_tematicas)) { //TODO: mapear antigos temas para novos
							$json[$d->mapeamento]->porTema[$dAreaTematica]++;
						}
					}
				}
			}
		}
	}
	faz_header('json');
	echo json_encode($json);
	exit;
}


// Busca comum, com ou sem filtros:
// Aqui eu começo a preparar o resultado (a raiz é o FAREJA):
switch ($outputFormat) {
	case 'xml':
	case 'html':
		$docxml = new DOMDocument("1.0");
		$docxml->formatOutput=true;
		$node = $docxml->CreateElement("fareja");
		$raiz = $docxml->AppendChild($node);
		break;
	case 'json':
		$json = new stdClass();
		break;
}

//Aqui monto o resultado conforme o pedido realizado
switch ($f) {
	//////////////////////////////////
	//   f = 'titulo'
	/////////////////////////////////
	case 'titulo':
		$outputFormat = 'html';
		$tab = ($frms[0]->tab_base_comum)
			? $frms[0]->tab_base_comum
			: $frms[0]->nomefrm;
		$sql = "SELECT {$campo[$tab]->nome} FROM {$tab} WHERE `{$campo[$tab]->id}`='{$id}'";
		$res = faz_query($sql, '', 'array');
		$html = $res[0][0];
		break;

	//////////////////////////////////
	//   f = 'ficha'
	/////////////////////////////////
	case 'ficha':
		$outputFormat = 'json';
		$frm = $frms[0];
		$json = fichaFormataDados($frm, $id);
		if ($json->error) {
			break;
		}

		switch ($tipofrms[0]) {
			case 'experiencia':

				$json->AG = $json->localizacao;

				if (is_numeric($id)) {
					$sql = "SELECT a.in_id, a.in_nome, a.in_liberacao FROM experiencia_autor, frm_instituicao a WHERE exa_id_inst=in_id AND exa_id_experiencia='$id' AND in_id NOT IN ('" . implode("', '", array_keys($json->instituicoes)) . "')";
					$instAutor = faz_query($sql, '', 'object_assoc', 'in_id');
					$json->instituicoes = $json->instituicoes + $instAutor;

					$sql = "SELECT a.in_id, a.in_nome, a.in_liberacao FROM experiencia_relator, frm_instituicao a WHERE exr_id_inst=in_id AND exr_id_experiencia='$id'";
					$instRelator = faz_query($sql, '', 'object_assoc', 'in_id');
					$json->instituicoes = $json->instituicoes + $instRelator;

					$sql = "SELECT * FROM experiencia_arquivo WHERE ea_id_experiencia='$id'";
					$queryAnexos = $db->query($sql);
					$numAnexos = $queryAnexos->num_rows;

					while ($rowAnexo = $queryAnexos->fetch_object()) {
						if (file_exists($dir['upload'].$rowAnexo->ea_arquivo)) {
							$anexoExiste = false;
							foreach ($json->anexos as $anexo) {
								if ($anexo->arq == $rowAnexo->ea_arquivo) {
									$anexoExiste = true;
									break;
								}
							}
							if (!$anexoExiste) {
								$t = new stdClass();
								$t->arq = $rowAnexo->ea_arquivo;
								$t->mimetype = mime_content_type($dir['upload'].$rowAnexo->ea_arquivo);
								$t->filesize = filesize($dir['upload'].$rowAnexo->ea_arquivo);
								$json->anexos[] = $t;
							}
						}
					}
				}

				$json->ano_publicacao = ($json->ano_publicacao && $json->ano_publicacao!='0000')
					? $json->ano_publicacao
					: substr($json->criacao,0,4);

				break;
			case 'dispositivo':
				break;
			case 'instituicao':

					// Lista de experiências e de imagens
					$json->arqs = array();
					$json->imgs = array();
					$json->experienciasComoAutora = array();
					if ($numAutora>0) {
						while ($row = $queryAutora->fetch_object()) {
							$json->experienciasComoAutora[$row->ex_id] = $row;
						}
						// Imagens das experiências relacionadas a esta instituição
						$sql = "SELECT * FROM frm_exp_base_comum WHERE ex_id IN (".implode(array_keys($json->experienciasComoAutora),',').")";
						$queryAnexos = $db->query($sql);
						$numAnexos = $queryAnexos->num_rows;
						while($rowAnexos = $queryAnexos->fetch_object()) {
							if ($rowAnexos->ex_anexos) {
								$anexos = explode('|',$rowAnexos->ex_anexos);
								if ($anexos) {
									foreach ($anexos as $arq) {
										if (is_object($arq)) {
											$val = json_decode($arq);
											$arquivo = $val->arq;
										} else {
											$arquivo = $arq;
										}
										if (file_exists($dir['upload'].$arquivo) && is_array(getimagesize($dir['upload'].$arquivo))) {
											$newImage = array(
											'arquivo'=>$arquivo,
											'arquivoResized'=>getResizedImgURL($arquivo,1024,800),
											'ex_id'=>$rowAnexos->ex_id,
											'ex_descricao'=>$json->experienciasComoAutora[$rowAnexos->ex_id]->ex_descricao,
											'relacao'=>'autora'
											);
											$json->imgs[] = $newImage;
											$json->experienciasComoAutora[$rowAnexos->ex_id]->imgs[] = $newImage;
											$json->arqs[]=$arquivo;
										}
									}
								}
							}
						}
					}

					// Experiências em que esta instituição foi relatora:
					if (is_numeric($id)) {
						$sql = "SELECT * FROM experiencia_relator,experiencia WHERE exr_id_experiencia=ex_id AND exr_id_inst='{$id}' order by ex_chamada";
						$queryRelatora = $db->query($sql);
						$numRelatora = $queryRelatora->num_rows;

						// Lista de experiências e de imagens
						$json->experienciasComoRelatora = array();
						if ($numRelatora>0) {
							while ($row = $queryRelatora->fetch_object()) {
								$json->experienciasComoRelatora[$row->ex_id] = $row;
							}
							// Imagens das experiências relacionadas a esta instituição
							$sql = "SELECT * FROM frm_exp_base_comum WHERE ex_id IN (".implode(array_keys($json->experienciasComoRelatora),',').")";
							$queryAnexos = $db->query($sql);
							$numAnexos = $queryAnexos->num_rows;
							while($rowAnexos = $queryAnexos->fetch_object()) {
								if ($rowAnexos->ex_anexos) {
									$anexos = explode('|',$rowAnexos->ex_anexos);
									if ($anexos) {
										foreach ($anexos as $arq) {
											if (is_object($arq)) {
												$val = json_decode($arq);
												$arquivo = $val->arq;
											} else {
												$arquivo = $arq;
											}
											if (file_exists($dir['upload'].$arquivo) && is_array(getimagesize($dir['upload'].$arquivo)) && !in_array($arquivo, $arqs)) {
												$newImage = array(
													'arquivo'=>$arquivo,
													'arquivoResized'=>getResizedImgURL($arquivo,1024,800),
													'ex_id'=>$rowAnexos->ex_id,
													'ex_descricao'=>$experienciasComoRelatora[$rowAnexos->ex_id]->ex_descricao,
													'relacao'=>'relatora'
												);
												$json->imgs[] = $newImage;
												$json->experienciasComoRelatora[$rowAnexos->ex_id]->imgs[] = $newImage;
												$json->arqs[]=$arquivo;
											}
										}
									}
								}
							}
						}

					}

					// Áreas Geográficas:
					$sql = "SELECT * FROM rel_geo_inst,area_geografica WHERE rgi_id_geo=ag_id AND rgi_id_inst='$id'";
					$json->AGs = faz_query($sql, '', 'object');

					shuffle($json->imgs);
				break;
		}
		$sql = "select * from lista_mapeamentos where publica=1";
		$json->lista_mapeamentos = faz_query($sql, '', 'object_assoc', 'id');
		$sql = "select * from lista_areas_tematicas";
		$json->lista_areas_tematicas = faz_query($sql, '', 'object_assoc', 'id');
		break;

	//////////////////////////////////
	//   f = 'infos'
	/////////////////////////////////
	case 'infos':
		$tipo = $frms[0]->tipo;
		if (!$txt) {
			include $dir['apoio']."textos/".$lingua.'.php';
			foreach ($txt as $i=>$t) {
				$txt[$i]=$t;
			}
		}
		$imgs = [];
		$pt = fichaFormataDados($frms[0], $idpt);
		$filtros[$frms[0]->tipo] = resgata_filtros($frms[0]->tipo);
		unset($pt->descricao);
		ob_start();
		?>
		<div class="card card-<?= $tipo ?> card-aer">
			<?php include("../mapeo/conteudo/mostra_tabela_card_{$tipo}.php"); ?>
		</div>
		<?php
		$html = ob_get_contents();
    	ob_end_clean();
		// pR($html);exit;
		switch ($outputFormat) {
			case 'xml':
			case 'html':
				$node = $docxml->CreateElement("markers");
				$nivel1 = $raiz->AppendChild($node);
				$node = $docxml->createElement("info");
				$newnode = $nivel1->appendChild($node);
				$newnode->setAttribute("msg", $html);
				break;
			case 'json':
				$json->markers = array();
				$json->info = $html;
				break;
		}
		break;

	//////////////////////////////////
	//   f = 'pts'
	/////////////////////////////////
	case 'pts':
	default:
		// Dados iniciais de configuração
		switch ($outputFormat) {
			case 'xml':
			case 'html':
				$node = $docxml->CreateElement("config");
				$nivel1 = $raiz->AppendChild($node);
				$nivel1->SetAttribute("tipo", $icone_tipo_padrao);
				$nivel1->SetAttribute("cor", $icone_cor_padrao);
				// Agora os pontos:
				$node = $docxml->CreateElement("markers");
				$nivel1 = $raiz->AppendChild($node);
				break;
			case 'json':
				$t = new stdClass();
				$t->tipo = $icone_tipo_padrao;
				$t->cor = $icone_cor_padrao;
				$json->config = $t;
				$json->markers = array();
				break;
		}

		// Loop nos formulários:
		$dados = ($conta || $resgataFiltros)
			? 0
			: [];
		$filtros = [];
		foreach ($frms as $frm) {
			$filtros[$frm->tipo] = resgata_filtros($frm->tipo);
			$filtroAtual = $filtros[$frm->tipo];
			unset($leftjoin);
			$cmpo = $campo[$frm->nomefrm];
			$sql = "SELECT '". $frm->tipo ."' as 'tipo', '".$frm->nomefrm."' as 'nomefrm'";
			if ($cmpo->descricao && $mostratabela && !$conta && !$resgataFiltros) {
				$sql .= ", ".$cmpo->descricao." AS 'descricao'";
				$sql .= ", ".$cmpo->criacao." AS 'criacao'";
			}
			foreach ($filtroAtual->geraXML as $gcampo=>$gnome) {
				if ($gnome == 'abrangencia_latlng') {
					$sql .= ", REPLACE(a.".$gcampo.",'0,00/0,00;0,00/0,00','') as 'abrangencia_latlng'";
				} else {
					$sql .= ($cmpo->{$gnome})
						? ", a.".$gcampo." AS '".$gnome."'"
						: ", a.".$gcampo;
				}
			}

			if ($geo->distmax)
				$sql.= ", ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." as distancia";

			// FROM:
			$from = ' FROM `lista_mapeamentos` as LM, ';
			$from .= ($frm->tab_base_comum)
				? "`{$frm->nomefrm}` as b, `{$frm->tab_base_comum}` as a"
				: "`{$frm->nomefrm}` as a";

			// WHERE:
			$where = " WHERE `{$cmpo->nome}` != '' AND {$campo[$frm->nomefrm]->mapeamento}=LM.id AND LM.publica=1";
			if ($frm->tab_base_comum)
				$where .= " AND b.".$cmpo->id."=a.".$campo[$frm->tab_base_comum]->id;
			if ($geo->distmax)
				$where.=" AND ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." < ".$geo->distmax;
			if ($datai || $dataf) {
				if ($tipodata=="ano")
					$where .= " AND YEAR(".$cmpo->localizacao.")";
					elseif ($tipodata="mes")
						$where .= " AND MONTH(".$cmpo->localizacao.")";
							else $where .= " AND ".$cmpo->localizacao;
				if ($datai && $dataf) {
					$datatxt = " BETWEEN $datai AND $dataf";
				} elseif ($datai) {
					$datatxt = ">=$datai";
				} else {
					$datatxt = "<=$dataf";
				}
				$where.=$datatxt;
			}
			// Este é para prevenir do campo de localização não seguir a regra de ter 11 dígitos.
			if ($gcampo == $cmpo->localizacao)
				$where.=" AND LENGTH(".$cmpo->localizacao.")=11 AND LOCATE(' ',".$cmpo->localizacao.")=0";
			// WHEREs dos filtros (se houver!):
			if (isset($filtroAtual->sels) && $filtroAtual->sels) {
				foreach ($filtroAtual->sels as $sel_campo=>$sel_valor) {
					$cmdo = $filtroAtual->cmd[$sel_campo];
					// Antes de mais nada eu percorro a árvore se o campo é de árvore, para que a escolha do usuário implique também na escolha dos filhos do item escolhido!
					$sub_sel_valor = array();
					if ($filtroAtual->mapa[$sel_campo]->tipo_form == 'arvore') {
						foreach ($sel_valor as $v) {
							$campo_id = $cmdo->campo_id;
							$sub_sel_valor[$v][] = $v;
							if ($arvore = resgata_arvore($cmdo->apoio,$campo_id,$cmdo->campo_nome,$cmdo->campo_mae,$v))
								foreach ($arvore as $a) {
									//if (!in_array($a->$campo_id,$sel_valor))
										//$sel_valor[] = $a->$campo_id;
									$sub_sel_valor[$v][] = $a->$campo_id;
								}
						}
					}
					// Pronto, agora eu gero o $where, dependendo se é o campo de localização ou se é dos demais. Não gero $where se o distmax estava definido e é de localização, pois já foi feito o where lá em cima.
					if ($sel_campo == $cmpo->localizacao && !$geo->distmax) {
						if (is_array($sel_valor)) {
							rsort($sel_valor);
							$sel_valor=$sel_valor[0];
						}
						$where .= " AND SUBSTRING(".$cmpo->localizacao.",1,".strlen($sel_valor).")='".$sel_valor."'";
					} elseif (!$geo->distmax) {
						$where .= " AND (";
						$tipo_juncao = " AND ";
						$tipo_juncao_tam = strlen($tipo_juncao);
						foreach ($sel_valor as $v) {
							if ($sub_sel_valor[$v]) {
								$where .= "(";
								foreach ($sub_sel_valor[$v] as $s)
									$where .= "LOCATE('".$s."', ".$sel_campo.")>0 OR ";
								$where = substr($where,0,-4).") ".$tipo_juncao;
							} else
								$where .= "LOCATE('".$v."', ".$sel_campo.")>0".$tipo_juncao;

						}
						$where = substr($where,0,-$tipo_juncao_tam)." )";
					}
				}
			}


			//Ainda preciso fazer o foreach de um campo de buscas dentro da variável $filtroAtual!
			if ($filtroAtual->textoBusca && is_array($cmpo->textoBusca)) {
				$where .= " AND (";
				foreach ($cmpo->textoBusca as $c)
					$where .= $c." LIKE '%".$filtroAtual->textoBusca."%' OR ";
				$where = substr($where,0,-4).")";
			}

			// ORDER BY:
			$orderbyEsp = (isset($cmpo->$orderby))
				? $cmpo->$orderby
				: $orderby;
			$orderbySql = " ORDER BY ";
			if ($geo->distmax) {
				$orderbySql.="distancia, ";
			}
			$orderbySql .= $orderbyEsp.$order;

			// Aqui nós adicionamos as definições de ícones de tabelas de apoio em que o $cmdo_mapa->icone está definido. Isso é algo para lembrar: sempre que eu colocar $cmdo_mapa->icone ou cor = 1, devem estar definidas as colunas icone_cor e icone_tipo na tabela de apoio!! Se estas 2 colunas não existirem, dá erro!
			foreach ($filtroAtual->mapa as $m) {
				//if ($m>campo != 'ex_areas_tematicas') {
				$cmdo_mapa = $filtroAtual->cmd_mapa[$m->campo];
				$cmdo = $filtroAtual->cmd[$m->campo];
				// Um eventual where do $cmdo_mapa tem primazia sobre o where do $cmdo:
				if ($cmdo->apoio && $filtroAtual->opcoes[$m->campo]) {
					if ($cmdo_mapa->where)
						$cmdo->where = $cmdo_mapa->where;
					// WHEREs que vêm no cmdo ou cmdo_mapa para restringir valores das tabelas de apoio:
					if ($cmdo->where) {
						$cmdo->where = str_replace('{campo_id}',$m->campo,$cmdo->where);
						$cmdo->where = str_replace('{campo_nome}',$cmdo->campo_nome,$cmdo->where);
						$where .= " AND ".$cmdo->where;
					}
					// Definições de cor e tipo de ícone vindos das tabelas de apoio:
					if ($cmdo_mapa->icone) {
						$sql .= ($include_images && $frm->tipo=='experiencia')
							? ", ANY_VALUE(".$cmdo->apoio.".icone_cor) as ".$cmdo->apoio."_icone_cor, GROUP_CONCAT(".$cmdo->apoio.".icone_tipo) as ".$cmdo->apoio."_icone_tipo"
							: ", ".$cmdo->apoio.".icone_cor as ".$cmdo->apoio."_icone_cor, ".$cmdo->apoio.".icone_tipo as ".$cmdo->apoio."_icone_tipo";
						$leftjoin .= " LEFT JOIN ".$cmdo->apoio." ON ".$cmdo->apoio.".".$cmdo->campo_id."=".$m->campo;
						$where .= " AND (".$cmdo->apoio.".icone_cor<>'-1' AND ".$cmdo->apoio.".icone_tipo<>'-1')";
						$tabs_definem_icones[]=$cmdo->apoio;
					}
				}
			}
			if ($include_images && $frm->tipo=='experiencia') {
				$leftjoin .= " LEFT JOIN experiencia_arquivo d ON a.ex_id=d.ea_id_experiencia";
				$sql .= ", GROUP_CONCAT(d.ea_arquivo SEPARATOR ',') AS imgs";
				$groupby = " GROUP BY a.ex_id";
			} else {
				$groupby = "";
			}

			$sql.=$from.$leftjoin.$where.$groupby.$orderbySql;
			// echo $sql.chr(10).chr(10);exit;
				if ($conta || $resgataFiltros) {
					$dados += faz_query($sql,'','num');
				} else {
					$res = faz_query($sql,'','object');
					if ($res)
						$dados = array_merge($dados,$res);
				}
		} // fim do loop nos formulários do tipo $tipofrm
		//exit;
		// pR($dados,1);
		// pR($conta);exit;
		if ($conta) {
			// Se era pra contar, o resultado tem apenas o total de pontos:
			switch ($outputFormat) {
				case 'xml':
				case 'html':
					$node = $docxml->createElement("pt");
					$newnode = $nivel1->appendChild($node);
					$newnode->setAttribute("num", $dados);
					break;
				case 'json':
					$json = new stdClass();
					$json->num = $dados;
					break;
			}
		} else if ($resgataFiltros) {
			if (!$txt) {
				include $dir['apoio']."textos/".$lingua.'.php';
				foreach ($txt as $i=>$t) {
					$txt[$i]=$t;
				}
			}
			$sels = $filtroGenerico->sels;
			if ($filtroGenerico->textoBusca) {
				$sels['textoBusca'] = $filtroGenerico->textoBusca;
			}
			if ($filtroGenerico->tipofrmBusca) {
				$sels['tipofrm'] = $filtroGenerico->tipofrmBusca;
			}
			$json->html = mostraSelecaoAtual($dados, $filtroGenerico);
			$json->sels = $sels;
		} else {
			// Foi feito o select que tirou os pontos. Entretanto, é preciso agora acertar mais detalhes, depedendo se estou gerando dados para mapa ou para tabela:
			// Primeiramente, ordenar todos os dados:
			if (count($frms) > 1) {
				if ($orderby == 'dt_criacao') {
					$orderby = 'criacao';
				}
				$dados = sortAssociativeArrayByKey($dados, $orderby, trim($order));
			}

			$paises = $cidades = $abrangencias = $latlngs = array();
			$n = count($dados);
			// Os resultados devem ser limitados de acordo com o parâmetro do usuário, se for lista. Se for mapa, aparece tudo!
			if ($mostratabela) {
				$pgs = ceil($n/$padrao_itens_por_pagina);
				switch ($outputFormat) {
					case 'xml':
					case 'html':
						$numnode = $docxml->createElement("num");
						$novonode = $raiz->AppendChild($numnode);
						$novonode->setAttribute("num", $n);
						$novonode->setAttribute("pgs", $pgs);
						break;
					case 'json':
						$t = new stdClass();
						$t->num = $n;
						$t->pgs = $pgs;
						$json->num = $t;
						break;
				}
				if (!isset($_REQUEST['mostraTodos'])) {
					if ($pgAtual>1) {
						$min = ($pgAtual-1)*$padrao_itens_por_pagina;
						$dados = array_slice($dados,$min);
					}
					$dados = array_slice($dados,0,$padrao_itens_por_pagina);
					$n = count($dados);
				}
			}
			foreach ($dados as $i=>$d) {
				$filtroAtual = $filtros[$d->tipo];
				$t = $d->localizacao;
				if (($t &&
					(substr($t,2,1) == '.' && substr($t,5,1) == '.' && strlen($t) == 11) ||
					(substr($t,2,1) == '.' && strlen($t) == 5) ||
					(strlen($t) == 2)
				)) {
					if ($mostratabela) {
						// Se é o resultado para tabela, precisarei dos nomes das cidades e estados:
						if (!in_array(substr($t,0,2),$paises))
							$paises[]=substr($t,0,2);
						if (!in_array($t,$cidades))
							$cidades[]=$t;
					} else {
						// Se é para mapa, só pego as cidades e estados para o caso desta tabela não ter já alimentado automaticamente o localizacao_lat!
						if (!$campo[$filtroAtual->tab]->localizacao_lat) {
							if (!in_array(substr($t,0,2),$paises))
								$paises[]=substr($t,0,2);
							if (!in_array($t,$cidades))
								$cidades[]=$t;
						}

						// e aqui é também só para mapa: incluir os pontos de abrangência e também definir a cor e tipo do ícone dos dados alimentados pelo mysql:
						/*if ($d->abrangencia_cidades) {
							$tmp = explode('|',$d->abrangencia_cidades);
							foreach ($tmp as $t) {
								if ($t) {
									if (!in_array(substr($t,0,2),$paises))
										$paises[]=substr($t,0,2);
									if (!in_array($t,$cidades))
										$cidades[]=$t;
									$abrangencias[$t][]=$d->id;
								}
							}
						}*/
						if ($d->abrangencia_latlng) {
							$tmp1 = explode(';',$d->abrangencia_latlng);
							foreach ($tmp1 as $t) {
								$tmp2 = explode('/',str_replace(',','.',$t));
								if ((floatval($tmp2[0]) && floatval($tmp2[1]))) {
									$tmpid=$d->id;
									$t = new stdClass();
									$t->tipo = $d->tipo;
									$t->id_mae=$tmpid;
									$t->localizacao_lat=$tmp2[0];
									$t->localizacao_lng=$tmp2[1];
									$t->icone_tipo = 'Pontogde';
									$t->icone_cor = 'Verde';
									$dados[$n] = $t;
									// para ficar hachurado, veremos!
									if ($mostra_hachurado) {
										$dados[$i]->localizacao_lat .= ','.$dados[$n]->localizacao_lat;
										$dados[$i]->localizacao_lng .= ','.$dados[$n]->localizacao_lng;
									}
									$n++;
								}
							}
						}
						if ($tabs_definem_icones) {
							$cores = $tipos = array();
							foreach ($tabs_definem_icones as $apoio) {
								$txt_cor = $apoio.'_icone_cor';
								$txt_tipo = $apoio.'_icone_tipo';
								if ($d->$txt_cor)
									$cores[]=$d->$txt_cor;
								if ($d->$txt_tipo)
									$tipos[]=$d->$txt_tipo;
								unset($dados[$i]->$txt_cor,$dados[$i]->$txt_tipo);
							}
							if (count($cores))
								$dados[$i]->icone_cor = $cores[0];
							if (count($tipos))
								$dados[$i]->icone_tipo = $tipos[0];
						}
					} // fim do else do if $mostratabela
				}
			} // fim do loop nos dados
			// Agora resgato os dados de latitude e longitude das cidades que são abrangência e/ou das cidades de tabelas que não tenham lat/lng definidas e apenas cidade:
			if (count($paises)) {
				foreach ($paises as $pais) {
					$sql = "
						SELECT a.id, a.nome as cidade, b.nome as estado, c.nome as pais, a.lat, a.lng
						FROM _".$pais." as a
							LEFT JOIN _".$pais."_maes as b ON a.id_mae=b.id
							LEFT JOIN __paises as c ON c.id='".$pais."'
						WHERE a.id IN (";
					foreach ($cidades as $c) {
						if (substr($c,0,2)==$pais)
							$sql.="'".$c."', ";
					}
					$sql = substr($sql,0,-2).")";
					$res = faz_query($sql,'','object');
					if ($res) {
						$latlngs = array_merge($latlngs,$res);
					}
				}
				// Definição das lat/lng se não estão na tabela:
				foreach ($dados as $n=>$dado) {
					if (!$campo[$filtros[$dado->tipo]->tab]->localizacao_lat || $mostratabela) {
						foreach ($latlngs as $latlng) {
							if ($latlng->id == $dado->localizacao) {
								if (!$mostratabela) {
									$dados[$n]->localizacao_lat = $latlng->lat;
									$dados[$n]->localizacao_lng = $latlng->lng;
								} else {
									// Se vai mostrar a tabela, devo colocar a cidade/estado/pais:
									$dados[$n]->cidadeNome = $latlng->cidade;
									$dados[$n]->estadoNome = $latlng->estado;
									$dados[$n]->paisNome = traduz($latlng->pais,$lingua);
								}
							}
						}
					}
				}

				// Definição das abrangências:
				foreach ($latlngs as $latlng) {
					if ($abrangencias[$latlng->id]) {
						foreach ($abrangencias[$latlng->id] as $a) {
							$n++;
							$t = new stdClass();
							$t->id_mae=$a;
							//$t->cidade=$latlng->id;
							$t->localizacao_lat=$latlng->lat;
							$t->localizacao_lng=$latlng->lng;
							$t->icone_tipo = 'Pontogde';
							$t->icone_cor = 'Verde';
							$dados[$n] = $t;

							// para ficar hachurado, veremos!
							if ($mostra_hachurado)
							foreach ($dados as $di=>$da)
							if ($da->id == $a) {
								$dados[$di]->localizacao_lat .= ','.$latlng->lat;
								$dados[$di]->localizacao_lng .= ','.$latlng->lng;
							}
						}
					}
				}
			}

			// Pronto, criei a tabela de dados final. O lance agora é tranformá-la no XML para envio (já fazendo os desvios aleatórios das latitudes e longitudes que batem no mesmo ponto (monitorados por $lats e $lngs):
			$lats = $lngs = array();
			// pR($dados);exit;
			foreach ($dados as $d) {
				if (!$mostratabela && (!$d->localizacao_lat || !$d->localizacao_lng)) {
					continue;
				}
				if (in_array($d->localizacao_lng,$lngs) || in_array($d->localizacao_lat,$lats)) {
					$d->localizacao_lng+=rand(-500,0)*(0.03/500);
					$d->localizacao_lat+=rand(-500,0)*(0.03/500);
				} else {
					$lngs[]=$d->localizacao_lng;
					$lats[]=$d->localizacao_lat;
				}

				$o = new stdClass();
				if ($d->id) {
					$o->id = $d->id;
					$o->nomefrm = $d->nomefrm;
					$o->tipo = $d->tipo;
					$o->nome = $d->nome;
					// Se é lista, mostra-se, além do nome, também a cidade, estado, país e o resumo pelo overlib
					if ($mostratabela) {
						$o = new stdClass();
						foreach ($d as $k=>$v) {
							$vs = explode('|', $v);
							$newVs = [];
							foreach ($vs as $vv) {
								$decoded = json_decode($vv);
								$newVs[] = ($decoded) ? $decoded : $vv;
							}
							if (count($newVs) == 1) {
								$o->$k = is_object($newVs[0]) ? json_encode($newVs[0]) : $newVs[0];
							} else {
								$o->$k = json_encode($newVs);
							}
						}
						$o->url = str_replace('{id}',$d->id,$dir['mostra_'.$tipofrm.'_URL']);
						$o->cidade = $d->cidadeNome;
						$o->estado = $d->estadoNome;
						$o->pais = $d->paisNome;
						unset($o->cidadeNome, $o->estadoNome, $o->paisNome);
						// $o->descricao = $d->descricao;
						if (isset($d->criacao)) {
							$o->criacao = $d->criacao;
						}
						// pR($o);exit;
						// $o->imgs = $d->imgs;
					}
				} elseif ($d->id_mae) {
					$o->tipo = $d->tipo;
					$o->id_mae = $d->id_mae;
				}
				$o->lat = $d->localizacao_lat;
				$o->lng = $d->localizacao_lng;
				if ($d->distancia)
					$o->distancia = $d->distancia;
				if ($d->icone_cor)
					$o->cor = $d->icone_cor;
				if ($d->icone_tipo)
					$o->tipo = $d->icone_tipo;
				// if ($include_images && $d->imgs) {
				// 	$o->imgs = $d->imgs;
				// }
				if ($mostratabela) {
					foreach ($filtros[$d->tipo]->geraXML as $gcampo=>$gnome) {
						if (!property_exists($o, $gcampo) && !property_exists($o, $gnome)) {
							$o->{$gcampo} = $d->{$gcampo};
						}
					}
				}

				switch($outputFormat) {
					case 'xml':
					case 'html':
						$node = $docxml->createElement("pt");
						$newnode = $nivel1->appendChild($node);
						foreach(get_object_vars($o) as $key=>$value) {
							$newnode->setAttribute($key, $value);
						}
						break;
					case 'json':
						$json->markers[] = $o;
						break;
				}
			}
		} // fim do if $conta
	break;
} // FIM DO SWITCH $f.

faz_header($outputFormat);

switch ($outputFormat) {
	case 'xml':
		echo $docxml->saveXML();
		break;
	case 'json':
		echo json_encode($json);
		break;
	case 'html':
		if (in_array($f, ['titulo', 'ficha'])) {
			echo $html;
		} else {
			if (!$txt) {
				include $dir['apoio']."textos/".$lingua.'.php';
				foreach ($txt as $i=>$t) {
					$txt[$i]=$t;
				}
			}
			if (!isset($filtros) || count($filtros) == 0) {
				$filtros = [];
				foreach ($tipofrms as $tipofrm) {
					$filtros[$tipofrm] = resgata_filtros($tipofrm);
				}
			}
			$resultados = formata_dados_xml_agrorede(array(simplexml_load_string($docxml->saveXML())), $visao);
			$xml = $resultados->xml;
			$pts = $resultados->pts;
			$numpts = $resultados->numpts;
			$numpgs = $resultados->numpgs;
			$sql = "select id, nome from lista_areas_tematicas order by nome";
			$areas_tematicas = faz_query($sql, '', 'object_assoc', 'id');

			include("../mapeo/conteudo/mostra_tabela_wrapper.php");
		}
		break;
}

function mensagem_html($dados) {
global $cidade_ref, $id_cidade_ref, $filtros;
	$filtro = array_values($filtros)[0];
	$dados->ex_descricao=wordwrap($dados->ex_descricao,30,"<br />");
	$dados->ex_chamada=wordwrap($dados->ex_chamada,30,"<br />");
	$dados->ex_resumo=wordwrap($dados->ex_resumo,30,"<br />");
	$dados->txt_comentario=wordwrap($dados->txt_comentario,30,"<br />");
	$dados->tema=wordwrap(str_replace('|',', ',$dados->tema),30,"<br />");
	$dados->local=wordwrap(str_replace('|',', ',$dados->local),30,"<br />");
	if ($filtro->busca) {
		$dados->ex_descricao=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_descricao);
		$dados->ex_chamada=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_chamada);
		$dados->ex_resumo=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_resumo);
		$dados->ex_tema=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_tema);
	}
	$texto.=$dados->ex_descricao.'|'
		. $dados->ex_chamada.'|'
//		. $dados->ex_resumo.'|'
//		. $dados->txt_comentario.'|'
		. urlencode($dados->ea_arquivo).'|'
		. $dados->tema.'|'
		. $dados->local.'|'
		. $dados->ex_id;
	return $texto;
}

function faz_header($format = 'json') {
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: text/{$format}; charset=utf-8");
	/*
	if(in_array($_SERVER['HTTP_ORIGIN'], API_ALLOWED_ORIGINS)) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header("Content-Type: text/{$format}; charset=utf-8");
	} else {
		header("HTTP/1.1 403 Access Forbidden");
    	header("Content-Type: text/plain");
    	echo "Seu site não está autorizado a acessar esta API";
	}
	*/
}
?>
