<?php

declare(strict_types=1);
spl_autoload_register(function ($class) {
    $parts = explode('\\', $class);
    require 'mapeo/' . end($parts) . '.php';
});

$campo['frm_exp_base_comum'] = (object) [
	"textoBusca" => ['ex_descricao', 'ex_resumo'],

	"abrangencia_cidades" => "ex_abrangencia_cidades",
	"abrangencia_comunidades" => "ex_abrangencia_comunidades",
	"abrangencia_estados" => "ex_abrangencia_estados",
	"abrangencia" => "ex_abrangencia",
	"abrangencia_latlng" => "ex_abrangencia_latlng",
	"abrangencia_macrorregioes" => "ex_abrangencia_macrorregioes",
	"abrangencia_outras" => "ex_abrangencia_outras",
	"abrangencia_paises" => "ex_abrangencia_paises",
	"anexos" => "ex_anexos",
	"areas_tematicas" => "ex_areas_tematicas",
	"areas_tematicas_outras" => "ex_areas_tematicas_outras",
	"atualizacao" => "dt_atualizacao",
	"criacao" => "dt_criacao",
	"descricao" => "ex_resumo",
	"digitadora" => "cod_usuario",
	"foto" => "ex_foto",
	"fundacao" => "ex_fundacao",
	"id" => "ex_id",
	"instituicao_referencia" => "ex_instituicao_referencia",
	"instituicoes" => "ex_instituicoes",
	"lat" => "ex_lat",
	"lng" => "ex_lng",
	"localizacao" => "ex_cidade_ref",
	"localizacao_lat" => "ref_lat",
	"localizacao_lng" => "ref_lng",
	"logo" => "ex_logo",
	"mapeamento" => "ex_mapeamento",
	"nome" => "ex_descricao",
	"rede" => "ex_rede",
	"sites" => "ex_www",
	"situacao_atual" => "ex_ainda_funciona",
	"status" => "ex_liberacao",
	"sujeitos" => "ex_atores",
	"tudo" => "ex_tudo"
];

$campo['frm_exp_geral'] = $campo['frm_exp_base_comum'];
$campo['frm_exp_geral']->textoBusca = ['ex_descricao', 'ex_resumo', 'ex_chamada'];

$campo['frm_exp_cca'] = $campo['frm_exp_base_comum'];
$campo['frm_exp_cca']->textoBusca = ['ex_descricao', 'ex_resumo'];

$campo['frm_instituicao'] = (object) [
	"textoBusca" => ['in_nome', 'in_descricao'],

	"abrangencia_cidades" => "in_abrangencia_cidades",
	"abrangencia_comunidades" => "in_abrangencia_comunidades",
	"abrangencia_estados" => "in_abrangencia_estados",
	"abrangencia" => "in_abrangencia",
	"abrangencia_macrorregioes" => "in_abrangencia_macrorregioes",
	"abrangencia_outras" => "in_abrangencia_outras",
	"abrangencia_paises" => "in_abrangencia_paises",
	"anexos" => "in_anexos",
	"areas_tematicas" => "in_areas_tematicas",
	"areas_tematicas_outras" => "in_areas_tematicas_outras",
	"atualizacao" => "dt_atualizacao",
	"criacao" => "dt_criacao",
	"descricao" => "in_descricao",
	"digitadora" => "cod_usuario",
	"email" => "in_email",
	"foto" => "in_foto",
	"id" => "in_id",
	"localizacao" => "ei_cidade",
	"localizacao_lat" => "ref_lat",
	"localizacao_lng" => "ref_lng",
	"logo" => "in_logo",
	"mapeamento" => "in_mapeamento",
	"nome" => "in_nome",
	"rede" => "in_rede",
	"sites" => "in_url",
	"status" => "in_liberacao",
	"tudo" => "in_tudo"
];

$campo['frm_rede'] = (object) [
	"textoBusca" => ['rede_nome', 'rede_nome_curto'],

	"areas_tematicas" => "rede_areas_tematicas",
	"atualizacao" => "dt_atualizacao",
	"criacao" => "dt_criacao",
	"descricao" => "rede_descricao",
	"digitadora" => "cod_usuario",
	"foto" => "rede_foto",
	"id" => "rede_id",
	"localizacao" => "rede_pais",
	"logo" => "rede_logo",
	"mapeamento" => "rede_mapeamento",
	"nome" => "rede_nome_curto",
	"status" => "rede_liberacao",
	"tudo" => "rede_tudo"
];

$campo['frm_dispositivo'] = (object) [
	"textoBusca" => ['disp_nome', 'disp_descricao'],

	"abrangencia_cidades" => "disp_abrangencia_cidades",
	"abrangencia_comunidades" => "disp_abrangencia_comunidades",
	"abrangencia" => "disp_abrangencia",
	"abrangencia_estados" => "disp_abrangencia_estados",
	"abrangencia_macrorregioes" => "disp_abrangencia_macrorregioes",
	"abrangencia_outras" => "disp_abrangencia_outras",
	"abrangencia_paises" => "disp_abrangencia_paises",
	"anexos" => "disp_anexos",
	"areas_tematicas" => "disp_areas_tematicas",
	"areas_tematicas_outras" => "disp_outras_tags",
	"atualizacao" => "dt_atualizacao",
	"criacao" => "dt_criacao",
	"descricao" => "disp_descricao",
	"descricao" => "disp_descricao",
	"digitadora" => "cod_usuario",
	"email" => "disp_email_inst",
	"foto" => "disp_foto",
	"id" => "disp_id",
	"instituicao_referencia" => "disp_organizacao_referencia",
	"instituicoes" => "disp_organizacoes",
	"lat" => "disp_lat",
	"lng" => "disp_lng",
	"localizacao" => "disp_cidade_ref",
	"localizacao_lat" => "ref_lat",
	"localizacao_lng" => "ref_lng",
	"logo" => "disp_logo",
	"mapeamento" => "disp_mapeamento",
	"nome" => "disp_nome",
	"rede" => "disp_rede_id",
	"sites" => "disp_www",
	"situacao_atual" => "disp_situacao_atual",
	"status" => "disp_liberacao",
	"tudo" => "disp_tudo"
];

?>
