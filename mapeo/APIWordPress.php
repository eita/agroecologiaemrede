<?php
declare(strict_types=1);

namespace EITA;
use \Exception;

/**
 * Manage and update worPress database
 */
class APIWordPress
{
    // Classes
    private $Utils;
    private $wpConfig;

    // Public objects
    public $submission;
    public $cmpo;

    // Public vars
    public $georrefs = [];

    // Private vars
    private $mapeamento = "";
    private $campo = [];
    private $token = "";
    private $tipoFrm = "";
    private $submissions = [];
    private $existingSubmission = [];
    private $colheitaFormId = "";
    private $colheitaFormNome = "";
    private $colheitaFormQuestions = [];
    private $colheitaSubmissionId = "";
    private $colheitaSubmissionCreatedAt = "";
    private $colheitaSubmissionUpdatedAt = "";
    private $colheitaDadosEspecificosDoMapeamento = [];
    private $colheitaAnexosPath = "";
    private $campoNome = "";
    private $campoId = "";
    private $campoAtualizacao = "";
    private $campoMapeamento = "";
    private $targetField = "";
    private $targetValue = "";
    private $attachmentsIds = [];

    /**
     * params['Utils']
     * params['cmpo']
     * params['campo']
     * params['mapeamento']
     * params['wpConfig']
     * params['tipoFrm']
     * @method __construct
     * @param  array       $params (veja acima)
     */
    public function __construct(array $params)
    {
        $this->Utils = $params['Utils'];
        $this->campo = $params['campo'];
        $this->wpConfig = $params['wpConfig'];
        $this->colheitaTaxonomias = $params['colheitaTaxonomias'];
        $this->colheitaAnexosPath = $params['colheitaAnexosPath'];
        $this->populateTaxonomyStructure();
    }

    /**
     * Sets mapeamento and tipo from Colheita Form.
     * Gets all existing submissions in WordPress for current Mapeamento and Tipo
     * params['colheitaFormId']
     * params['colheitaFormNome']
     * params['mapeamento']
     * params['tipoFrm']
     * params['colheitaFormQuestions']
     * params['cmpo']
     * @method setForm
     * @param  array   $params (veja acima)
     */
    public function setForm(array $params)
    {
        $this->colheitaFormId = $params['colheitaFormId'];
        $this->colheitaFormNome = $params['colheitaFormNome'];
        $this->mapeamento = $params['mapeamento'];
        $this->tipoFrm = $params['tipoFrm'];
        $this->colheitaFormQuestions = $params['colheitaFormQuestions'];
        $this->cmpo = $params['cmpo'];
        $this->submissions[$this->colheitaFormId] = [];
        $this->campoNome = $this->cmpo->nome;
        $this->campoId = $this->cmpo->id;
        $this->campoAtualizacao = $this->cmpo->atualizacao;
        $this->campoMapeamento = $this->cmpo->mapeamento;
        $this->populateFormQuestionsStructure();
        $this->getCurrentSubmissions();
    }

    /**
     * Conecta com WordPress e gera o token
     * @method authenticate
     */
    private function authenticate()
    {
        if ($this->isValidToken()) {
            return;
        }
        $url = "{$this->wpConfig->base}/wp-json/jwt-auth/v1/token";
        $postFields = "username={$this->wpConfig->username}&password={$this->wpConfig->password}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if (!$output || !$output->token) {
            throw new Exception ("Authentication failed! Message: " . $outputRaw);
        }
        $this->token = $output->token;
    }

    /**
     * Check if token is still valid
     * @method isValidToken
     * @return bool         Is it valid?
     */
    private function isValidToken(): bool
    {
        if (!$this->token) {
            return false;
        }

        $url = "{$this->wpConfig->base}/wp-json/jwt-auth/v1/token/validate";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->token}"));
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($output);
    // $this->Utils->pT([$output, $output->code == "jwt_auth_valid_token"]);
        return ($output->code == "jwt_auth_valid_token");
    }

    /**
     * Get existing submissions in $wordPress, from this mapeamento and type, and populate submissions
     * @method getCurrentSubmissions
     */
    public function getCurrentSubmissions()
    {
        $this->authenticate();
        $args = [
            'mapeamento_slug' => $this->mapeamento,
            'per_page' => $this->wpConfig->per_page,
            'page' => 0
        ];
        $keepGoing = true;
        $pg = 1;
        while ($keepGoing) {
            $args["page"] = $pg;
            $query = http_build_query($args);
            $url = "{$this->wpConfig->base}/wp-json/wp/v2/{$this->tipoFrm}?{$query}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->token}"));
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            $newSubmissions = json_decode($output);
            if (!is_array($newSubmissions)) {
                throw new Exception("Error fetching data from WordPress! URL: {$url}. Response: {$output}");
            }
            $this->submissions[$this->colheitaFormId] = array_merge($this->submissions[$this->colheitaFormId], $newSubmissions);
            $keepGoing = (count($newSubmissions) == $this->wpConfig->per_page);
            $pg ++;
        }
    }

    /**
     * Updates WordPress Taxonomy Structure
     * @method populateTaxonomyStructure
     */
    private function populateTaxonomyStructure()
    {
        $this->authenticate();

        $args = [];
        foreach ($this->colheitaTaxonomias as $tax) {
            $args[] = $tax["APIWordPress"];
        }
        $data_json = json_encode($args);
        $header = [
            "Authorization: Bearer {$this->token}",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        ];
        $url = "{$this->wpConfig->base}/wp-json/aerbridge/v1/taxjson/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_POST, 1);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if (!$output || $output != "OK!") {
            throw new Exception ("Error populating taxonomy! Message: " . $outputRaw);
        }
        $this->Utils->pT("Successfully populated Colheita's Taxonomy Structure");
    }

    /**
     * Populates the
     * @return [type] [description]
     */
    private function populateFormQuestionsStructure()
    {
        $this->authenticate();
        $data_json = json_encode($this->colheitaFormQuestions);
        $header = [
            "Authorization: Bearer {$this->token}",
            "Content-Type: application/json",
            "Content-Length: " . strlen($data_json)
        ];
        $filename = "questions_{$this->mapeamento}_{$this->tipoFrm}_{$this->colheitaFormId}.json";
        $url = "{$this->wpConfig->base}/wp-json/aerbridge/v1/questions_structure?filename={$filename}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_POST, 1);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if (!$output || $output != "OK!") {
            throw new Exception ("Error populating form questions structure! Message: " . $outputRaw);
        }
        $this->Utils->pT("Successfully populated the form's questions structure into file " . $filename);
    }



    /**
     * Sets the submission to be worked on, based on current submission from Colheita
     * @method setSubmission
     * @param  string        $colheitaSubmissionId   The submission Id
     */
    public function setSubmission(
        string $colheitaSubmissionId,
        string $colheitaSubmissionCreatedAt,
        string $colheitaSubmissionUpdatedAt
    ) {
        if (!$colheitaSubmissionId) {
            throw new Exception("WordPress: Error! setSubmission without ID is impossible!");
        }
        $this->colheitaSubmissionId = $colheitaSubmissionId;
        $this->colheitaSubmissionCreatedAt = $colheitaSubmissionCreatedAt;
        $this->colheitaSubmissionUpdatedAt = $colheitaSubmissionUpdatedAt;
        $this->georrefs = [];
        $this->submission = [];

        $this->setExistingSubmission();
// print_r([
//  "colheitaSubmissionId" => $this->colheitaSubmissionId,
// ]);exit;
    }

    /**
     * Looks for a submission with current colheita ID
     * @method setExistingSubmission
     */
    private function setExistingSubmission()
    {
        if (!$this->colheitaSubmissionId) {
            throw new Exception('setExistingSubmission: No colheitaSubmissionId defined!');
        }
        foreach ($this->submissions[$this->colheitaFormId] as $wpSubmission) {
            $this->existingSubmission = [];
// print_r($wpSubmission->meta->colheitaId[0]);
            if ($wpSubmission->meta->colheitaId[0] == $this->colheitaSubmissionId) {
                $this->existingSubmission = (array) $wpSubmission;
                try {
                    $this->populateSubmissionAnexosList();
                } catch(Exception $e) {
                    $this->Utils->pE("Error downloading anexos! Message: " . $e->getMessage());
                }
                break;
            }
        }
// print_r(['existingSubmission'=>$this->existingSubmission, 'colheitaSubmissionId' => $this->colheitaSubmissionId]);exit;
    }

    private function populateSubmissionAnexosList() {
        if (!$this->existingSubmission["id"]) {
            throw new Exception("Not able to fetch anexos list without ID!");
        }
        $args = [
            "parent" => $this->existingSubmission["id"]
        ];
        $query = http_build_query($args);
        $url = "{$this->wpConfig->base}/wp-json/wp/v2/media?{$query}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->token}"));
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if ($outputRaw === false) {
            throw new Exception("Error trying to get attachments of submission id '{$id}'! Returned FALSE on curl_exec()");
        }

        $output = json_decode($outputRaw);
// print_r(['populateSubmissionAnexosList'=> $output]);
        if ($output === null) {
            throw new Exception("Error trying to get attachments of submission id '{$id}'! Error is {$outputRaw}");
        }
        $this->existingSubmission["attachments"] = $output;
    }

    /**
     * Removes submission(s) from WordPress. Should remove also each submission's postMeta and attachments
     * @method removePosts
     * @param  array             $ids List of the Ids of the WordPress submissions to be removeDuplicateValuesInRow
     */
    private function removePosts(array $ids, $tipoFrm = "")
    {
        $this->authenticate();
        foreach ($ids as $id) {
            $args = [
                "force" => true
            ];
            if ($tipoFrm == "") {
                $tipoFrm = $this->tipoFrm;
            }
            $query = http_build_query($args);
            $url = "{$this->wpConfig->base}/wp-json/wp/v2/{$tipoFrm}/{$id}?{$query}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer {$this->token}"));
            $outputRaw = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            $output = json_decode($outputRaw);
// $this->Utils->pT([$url, $output, $outputRaw]);exit;
            if (!$output) {
                throw new Exception("Error trying to remove submission id '{$id}'! Error is {$outputRaw}");
            }
        }
    }

    /**
     * Removes all submissions in WordPress from current form. Use with care!
     * @method removeAllCurrentSubmissions
     * @return int                         Number of submissions removed
     */
    public function removeAllCurrentSubmissions(): int
    {
        $IdsToBeRemoved = [];
        foreach ($this->submissions[$this->colheitaFormId] as $wpSubmission) {
            $IdsToBeRemoved[] = $wpSubmission->id;
        }
        $this->removePosts($IdsToBeRemoved);
        $this->submissions[$this->colheitaFormId] = [];
        return count($IdsToBeRemoved);
    }

    /**
     * Remove the submission being worked on, if it existis in WordPress
     * @method removeCurrentSubmission
     * @return bool                    Returns true if it was effectively removed from WordPress DB
     */
    public function removeCurrentSubmission():bool
    {
        if ($this->existingSubmission["id"]) {
            try {
                $this->removePosts([$this->existingSubmission["id"]]);
            } catch (Exception $e) {
                $this->Utils->pT("Error removing current submission: " . $e->getMessage());
                return false;
            }
        }
        return false;
    }

    /**
     * Remove all submissions in WordPress that are not in colheita
     * @param  array $colheitaIds The colheita submission Ids that are available
     * @return int                The number of wordpress submissions removed
     */
    public function removeOrphanedSubmissions(array $colheitaIds): int
    {
        $orphanedSubmissions = [];
        foreach ($this->submissions[$this->colheitaFormId] as $wpSubmission) {
        // print_r([$wpSubmission->meta->colheitaId[0], $colheitaIds]); exit;
            if (!in_array($wpSubmission->meta->colheitaId[0], $colheitaIds)) {
                $orphanedSubmissions[] = $wpSubmission->id;
            }
        }
// print_r(["colheitaIds" => $colheitaIds, "orphanedSubmissions" => $orphanedSubmissions]);
        if (count($orphanedSubmissions)) {
            $this->removePosts($orphanedSubmissions);
        }
        return count($orphanedSubmissions);
    }

    public function resgataSubmissionRepetidaPorNome()
    {
    }

    private function normalizeSubmission()
    {
        $normSubmission = [
            "status"  => "publish",
            "date" => $this->colheitaSubmissionCreatedAt,
            "modified" => $this->colheitaSubmissionUpdatedAt,
            "meta" => [
                "colheitaId" => $this->colheitaSubmissionId,
                "colheitaFormId" => $this->colheitaFormId
            ],
            "mapeamento_tax" => [
                $this->mapeamento => [
                    $this->colheitaFormNome
                ]
            ],
            "uf_tax" => [],
            "municipio_tax" => [],
            "uf_abrangencia_tax" => [],
            "municipio_abrangencia_tax" => []
        ];

        foreach ($this->submission as $key => $vals) {
            $genericTargetField = $this->Utils->extraiCampoGenericoSemTab($this->cmpo, $key);
            $targetField = "";
            if (array_key_exists($genericTargetField, $this->colheitaTaxonomias)) {
                $targetField = "{$genericTargetField}_tax";
                $normSubmission[$targetField] = (is_array($vals))
                    ? $vals
                    : [$vals];
            } else {
                switch ($key) {
                    case $this->campoNome:
                        $normSubmission["title"] = (is_array($vals))
                            ? $vals[0]
                            : $vals;
                        break;
                    case $this->cmpo->localizacao:
                        foreach ($vals as $val) {
                            if (!in_array($val->UF, $normSubmission["uf_tax"])) {
                                $normSubmission["uf_tax"][] = $val->UF;
                            }
                            $newMunicipio = "{$val->municipio} ({$val->UF})";
                            if (!in_array($newMunicipio, $normSubmission["municipio_tax"])) {
                                $normSubmission["municipio_tax"][] = $newMunicipio;
                            }
                        }
                        break;
                    case $this->cmpo->abrangencia_cidades:
                        foreach ($vals as $val) {
                            if (!in_array($val->UF_abrangencia, $normSubmission["uf_abrangencia_tax"])) {
                                    $normSubmission["uf_abrangencia_tax"][] = $val->UF_abrangencia;
                            }
                            $newMunicipio = "{$val->municipio_abrangencia} ({$val->UF_abrangencia})";
                            if (!in_array($newMunicipio, $normSubmission["municipio_abrangencia_tax"])) {
                                $normSubmission["municipio_abrangencia_tax"][] = $newMunicipio;
                            }
                        }
                        break;

                    default:
                        if (is_array($vals) && count($vals) == 1) {
                            $normSubmission["meta"][$genericTargetField] = $vals[0];
                        } else {
                            $normSubmission["meta"][$genericTargetField] = $vals;
                        }
                        break;
                }
            }
        }

        foreach ($this->colheitaDadosEspecificosDoMapeamento as $key => $vals) {
            if ((is_array($vals) && count($vals) > 1) || is_object($vals)) {
                $normSubmission["meta"]["{$this->mapeamento}_{$key}"] = $vals;
            } elseif (is_array($vals) && count($vals) == 1) {
                $normSubmission["meta"]["{$this->mapeamento}_{$key}"] = (is_object($vals[0]) || is_array($vals[0]))
                ? $vals[0]
                : $vals[0];
            } else {
                $normSubmission["meta"]["{$this->mapeamento}_{$key}"] = $vals;
            }
        }
        $this->submission = $normSubmission;
// print_r(["Normalized Submission"=>$normSubmission]);exit;
    }

    private function createNewWordPressSubmission(): string
    {
        $url = "{$this->wpConfig->base}/wp-json/wp/v2/{$this->tipoFrm}/";
// print_r(['url'=>$url, $this->submission]);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer {$this->token}"]);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if ($output === null || !property_exists($output, "id")) {
            throw new Exception ("Error creating new empty submission in WordPress! Error: " . $outputRaw);
        }

        return (string) $output->id;
    }

    private function writeSubmission(string $wordPressSubmissionId)
    {
        if (!$wordPressSubmissionId) {
            throw new Exception("writeSubmission: Can't write to submission without submissionId!");
        }
        $url = "{$this->wpConfig->base}/wp-json/wp/v2/{$this->tipoFrm}/{$wordPressSubmissionId}/";
// print_r(['url'=>$url, $this->submission]);exit;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer {$this->token}"]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->submission));
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if ($output === null || !property_exists($output, "id")) {
            throw new Exception ("Error writing submission with id " . $wordPressSubmissionId . " in WordPress! Error: " . $outputRaw);
        }
    }

    /**
     * Create or update submission in WordPress
     * @method createOrUpdateSubmission
     * @param  boolean                  $forceUpdate If must update regardless of datetime of last update
     * @param  array                    $colheitaDadosEspecificosDoMapeamento Specific data from mapeamento
     * @return string                                The resulting status: created, updated or notUpdated
     */
    public function createOrUpdateSubmission(array $colheitaDadosEspecificosDoMapeamento, bool $forceUpdate = false): string
    {
        if (!count($this->submission)) {
            throw new Exception('Can\'t update: There is no submission set!');
        }

        $this->colheitaDadosEspecificosDoMapeamento = $colheitaDadosEspecificosDoMapeamento;
        $status = "";
    // $this->Utils->pT($this->submission);

        if ($this->existingSubmission["id"]) {
            $wordPressSubmissionId = (string) $this->existingSubmission["id"];
            $existingSubmissionLastUpdatedAt = strtotime($this->existingSubmission["modified"]) + $this->Utils->umDia;
            $colheitaSubmissionLastUpdatedAt = strtotime($this->colheitaSubmissionUpdatedAt);
            $hasChanges = ($existingSubmissionLastUpdatedAt < $colheitaSubmissionLastUpdatedAt);
            $status = ($hasChanges || $forceUpdate)
                ? "updated"
                : "notUpdated";
        } else {
            $wordPressSubmissionId = $this->createNewWordPressSubmission();
            $status = "created";
        }

        if (in_array($status, ["created", "updated"])) {
            $this->uploadSubmissionFiles($wordPressSubmissionId);

            $this->normalizeSubmission();

            $this->writeSubmission($wordPressSubmissionId);
        }

        return $status;
    }

    private function sendFileToWordPress(string $path): string
    {
        $url = "{$this->wpConfig->base}/wp-json/wp/v2/media/";
        $file = file_get_contents($path);
        $mime_type = mime_content_type($path);
        $header = [
            "Authorization: Bearer {$this->token}",
            "Content-Type: {$mime_type}",
            "Content-Disposition: attachment; filename=\"" . basename($path) . "\""
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $file);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if ($output === null || !$output->id) {
            throw new Exception("Failed to send file " . $val->arq . " to WP! Error: " . $outputRaw);
        }
        return (string) $output->id;
    }

    private function attachWordPressFileToSubmission(string $wordPressSubmissionId, string $wordPressAnexoId, string $colheitaAnexoId, string $title)
    {
        if (!($wordPressSubmissionId && $wordPressAnexoId && $colheitaAnexoId)) {
            throw new Exception("attachWordPressFileToSubmission error. All parameters must exist! Params: " . json_encode([$wordPressSubmissionId, $wordPressAnexoId, $colheitaAnexoId, $title]));
        }
        $args = [
            "date"          => $this->colheitaSubmissionUpdatedAt,
            "slug"          => "",
            "status"        => "publish",
            "title"         => $title,
            "description"   => "",
            "alt_text"      => "",
            "caption"       => "",
            "post"          => $wordPressSubmissionId,
            "meta"          => ["colheitaAnexoId" => $colheitaAnexoId]
        ];
        $json = json_encode($args);
        $url = "{$this->wpConfig->base}/wp-json/wp/v2/media/{$wordPressAnexoId}";
        $header = [
            "Authorization: Bearer {$this->token}"
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $outputRaw = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        $output = json_decode($outputRaw);
        if ($output === null) {
            throw new Exception("Failed to send file '" . $colheitaAnexoId . "' to WP! Error: " . $outputRaw);
        }
// print_r($output); exit;
    }

    private function getExistingSubmissionAnexo($colheitaAnexoId): string
    {
        if (!$this->existingSubmission) {
            return "";
        }
        foreach ($this->existingSubmission["attachments"] as $attachment) {
            if (
                property_exists($attachment->meta, "colheitaAnexoId")
                && $attachment->meta->colheitaAnexoId[0] == $colheitaAnexoId
            ) {
                return (string) $attachment->id;
            }
        }
        return "";
    }

    private function uploadSubmissionFiles(string $wordPressSubmissionId)
    {
        // print_r('uploadSubmissionFiles');
        foreach ($this->submission as $key => $vals) {
            // print_r([$key=>$vals]);
            if (is_array($vals) && is_object($vals[0]) && property_exists($vals[0], "filesize")) {
                $newVals = [];
                foreach ($vals as $val) {
                    if ($val->arq) {
                        $colheitaAnexoId = $val->arq;
                        $path = $this->colheitaAnexosPath . $val->arq;
                        $info = pathinfo($path);
                        // print_r(['path'=>$path]);
                        if (file_exists($path)) {
                            $wordPressAnexoId = $this->getExistingSubmissionAnexo($colheitaAnexoId);
                            $title = (property_exists($val, "question"))
                                ? $val->question
                                : basename($path, ".{$info['extension']}");

                            if ($wordPressAnexoId == "") {
                                try {
                                    $wordPressAnexoId = $this->sendFileToWordPress($path);
                                } catch (Exception $e) {
                                    $this->Utils->pE($e->getMessage());
                                }
                            }
                            if ($wordPressAnexoId != "") {
                                $this->attachWordPressFileToSubmission($wordPressSubmissionId, $wordPressAnexoId, $colheitaAnexoId, $title);
                                $newVals[] = (object) ["attachment_id" => $wordPressAnexoId];
                                $this->attachmentsIds[] = $wordPressAnexoId;
                            }
                        }
                    }
                }
                $this->submission[$key] = $newVals;
            }
        }
        $this->removeOrphanedAttachments();
    }

    private function removeOrphanedAttachments()
    {
        if (!$this->existingSubmission) {
            return;
        }
        foreach ($this->existingSubmission["attachments"] as $attachment) {
            if (!in_array($attachment->id, $this->attachmentsIds)) {
                $this->removePosts([$attachment->id], "media");
            }
        }
    }

    /**
     * Set the field being worked on current submission
     * @method setTargetField
     * @param  string         $colheitaTargetField the field in Colheita to be translated to the field in WordPress
     */
    public function setTargetField(string $targetField)
    {
        if (!$targetField) {
            throw new Exception('Error! colheitaTargetField is not set!');
        }

        $this->targetField = $targetField;
        $this->targetValue = $this->submission[$targetField];
    }

    /**
     * Sets the value of the field being working on current submission
     * @method setTargetValue
     * @param           $newVal The value to be set
     */
    public function setTargetValue($newVal)
    {
        if (!$this->targetField) {
            throw new Exception('Error! targetField is not set!');
        }

        $this->targetValue = $newVal;
        $this->submission[$this->targetField] = $this->targetValue;
    }

    public function addTargetValues($newVals, $relForm)
    {
        if (!$this->targetField) {
            throw new Exception('Error!');
        }
    // print_r([
    //  'targetValue' => $this->targetValue,
    //  'newVals' => $newVals
    // ]);
        if ($relForm != "" && array_key_exists($relForm, $this->submissions)) {
            $newNewVals = [];
            foreach ($newVals as $i => $newVal) {
                foreach ($this->submissions[$relForm] as $relSubmission) {
                    if ($relSubmission->title->rendered == $newVal) {
                        $newNewVals[] = $relSubmission->id;
                        break;
                    }
                }
            }
            $newVals = $newNewVals;
        }
        if ($this->targetValue && is_array($this->targetValue)) {
            $newVals = array_merge($this->targetValue, $newVals);
        }
    // $this->Utils->pT([
    //  'newVals' => $newVals
    // ]);
        $this->setTargetValue($newVals);
    }

    public function addGeorref(string $georrefType)
    {
        if (!array_key_exists($this->targetField, $this->georrefs)) {
            $this->georrefs[$this->targetField] = $georrefType;
        }
    }

    public function convertStateToId()
    {
        if (!$this->targetValue) {
            return;
        }
        // TODO: should I do something?
    }

    public function convertCountryToId()
    {
        if (!$this->targetValue) {
            return;
        }
        // TODO: should I do something?
    }

    public function prepareCity(bool $foundLocations)
    {
        if (!$this->targetValue || !is_array($this->targetValue) || count($this->targetValue) < 2) {
            throw new Exception("No targetValue set or in wrong format! targetValue = " . json_encode($this->targetValue));
        }
        $newVals = [];
        for ($i = 0; $i < count($this->targetValue); $i+=2) {
            $municipio = $this->Utils->resgataDadosMunicipio($this->targetValue[$i+1], $this->targetValue[$i]);
            if ($municipio) {
                if ($this->targetField == $this->cmpo->localizacao) {
                    $newVals[] = (object) [
                    "UF" => $municipio->UF,
                    "municipio" => $municipio->nome
                    ];
                    if ($municipio->lat && !$foundLocations) {
                        $this->setLatLng((float) $municipio->lat, (float) $municipio->lng);
                        $foundLocations = true;
                    }
                } else {
                    $newVals[] = (object) [
                    "UF_abrangencia" => $municipio->UF,
                    "municipio_abrangencia" => $municipio->nome
                    ];
                }
            }
        }
        $this->setTargetValue($newVals);
        return (bool) $foundLocations;
    }

    private function targetValueToArray()
    {
    // return explode('|', $this->targetValue);
        $decoded = json_decode($this->targetValue);
        if (!is_array($decoded)) {
            $decoded = [$decoded];
        }
        return $decoded;
    }

    public function setLatLng(float $lat, float $lng)
    {
        $this->submission[$this->cmpo->localizacao_lat] = $lat;
        $this->submission[$this->cmpo->localizacao_lng] = $lng;
    }

    public function tabelaApoioResgataIdPorNome($tabelaApoio, $textoBusca, $campoId = 'id', $campoValor = 'nome')
    {
    }

    public function tabelaApoioInsereValores($tabelaApoio, $campos, $valores, $noAutoincrement)
    {
    }
}
