<?php
function extract_submissions($form_id) {
	global $colheita;
	if ($pegaEstrutura) {
		$extraSQLCampos = "
			parent_answers.parent_id as grandparent_id,
			parent_questionings.group_name_translations as parent_name,
			parent_questionings.ancestry as parent_ancestry,
			grandparent_questionings.group_name_translations as grandparent_name,
			grandparent_questionings.ancestry as grandparent_ancestry,
		";
		$extraSQLJoins = "
			INNER JOIN form_items parent_questionings
				ON parent_answers.questioning_id = parent_questionings.id
			INNER JOIN answers grandparent_answers
				ON parent_answers.parent_id = grandparent_answers.id
			INNER JOIN form_items grandparent_questionings
				ON grandparent_answers.questioning_id = grandparent_questionings.id
		";
	} else {
		$extraSQLJoins = $extraSQLCampos = '';
	}

	$sql = "
	SELECT
		responses.id AS submission_id,
		responses.created_at,
		responses.updated_at,
		responses.reviewed AS is_reviewed,
		questions.code AS code,
		questions.canonical_name AS question,
		questions.qtype_name AS question_type,
		users.name AS submitter_name,
		answers.id AS id,
		answers.new_rank AS ord,
		answers.value AS value,
		answers.datetime_value AS datetime_value,
		answers.parent_id as parent_id,
		questionings.ancestry as ancestry,
		questionings.ancestry_depth as ancestry_depth,
		{$extraSQLCampos}
		media_objects.id as item_id,
		media_objects.item_content_type,
		media_objects.item_file_name,
		media_objects.item_file_size,
		media_objects.type as item_type,
		COALESCE(ao.canonical_name, co.canonical_name) AS choice_name,
		COALESCE(ao.value, co.value) AS choice_value,
		option_sets.name AS option_set,
		option_sets.form_id AS option_set_related_form
	FROM
		responses
		LEFT JOIN users users
			ON responses.user_id = users.id
		INNER JOIN forms forms
			ON responses.form_id = forms.id
		LEFT JOIN answers answers
			ON answers.response_id = responses.id AND answers.type = 'Answer'
		INNER JOIN form_items questionings
			ON answers.questioning_id = questionings.id
		INNER JOIN questions questions
			ON questionings.question_id = questions.id
		INNER JOIN answers parent_answers
			ON answers.parent_id = parent_answers.id
		{$extraSQLJoins}
		LEFT JOIN option_sets option_sets
			ON questions.option_set_id = option_sets.id
		LEFT JOIN options ao
			ON answers.option_id = ao.id
		LEFT JOIN option_nodes ans_opt_nodes
			ON ans_opt_nodes.option_id = ao.id AND ans_opt_nodes.option_set_id = option_sets.id
		LEFT JOIN choices choices
			ON choices.answer_id = answers.id
		LEFT JOIN options co
			ON choices.option_id = co.id
		LEFT JOIN option_nodes ch_opt_nodes
			ON ch_opt_nodes.option_id = co.id AND ch_opt_nodes.option_set_id = option_sets.id
		LEFT JOIN media_objects media_objects
			ON media_objects.answer_id = answers.id
	WHERE
		forms.id = '{$form_id}'
	ORDER BY responses.created_at DESC, answers.parent_id, ord";
	$result = pg_query($colheita->PG, $sql);
	$dados = pg_fetch_all($result);
	// print_r($dados);exit;

	$r = new stdClass();
	$submissions = [];
	$questions = [];
	foreach ($dados as $dd) {
		$submission_id = $dd['submission_id'];
		$code = $dd['code'];
		$question = $dd['question'];
		$parent_id = $dd['parent_id'];
		$parent_question = $dd['parent_name'];
		$grandparent_id = $dd['grandparent_id'];
		$grandparent_question = $dd['grandparent_name'];
		$ord = intval($dd['ord']);
		if (!isset($questions[$code])) {
			$questions[$code] = $question;
		}
		if (!isset($submissions[$submission_id])) {
			$t = new stdClass();
			$t->answers = [];
			$t->updated_at = $dd['updated_at'];
			$t->created_at = $dd['created_at'];
			$t->submitter_name = $dd['submitter_name'];
			$t->is_reviewed = $dd['is_reviewd'];
			$submissions[$submission_id] = $t;
		}
		if (!isset($submissions[$submission_id]->answers[$parent_id])) {
			$submissions[$submission_id]->answers[$parent_id] = [];
		}

		$isSelect = in_array($dd['question_type'], ['select_one', 'select_multiple']);

		$value = $dd['value'];
		if ($isSelect) {
			$value = $dd['choice_value']
				? $dd['choice_value']
				: $dd['choice_name'];
		} else if ($dd['datetime_value']) {
			$value = $dd['datetime_value'];
		} else if ($dd['item_file_name']) {
			$t = new stdClass();
			$t->fileid = $dd['item_id'];
			$t->filesize = $dd['item_file_size'];
			$t->mimetype = $dd['item_content_type'];
			$t->itemtype = str_replace('::', '/', strtolower($dd['item_type'])).'s';
			$value = $t;
		}

		$t = new stdClass();
		$t->id = $dd['id'];
		$t->code = $dd['code'];
		$t->ord = $ord;
		$t->value = $value;
		$t->slug = ($isSelect)
			? slugify($value, '_')
			: '';
		$t->question_type = $dd['question_type'];
		$t->parent_id = $parent_id;
		$t->ancestry = $dd['ancestry'];
		$t->ancestry_depth = $dd['ancestry_depth'];
		if ($pegaEstrutura) {
			$t->parent_question = $parent_question;
			$t->parent_ancestry = $dd['parent_ancestry'];
			$t->grandparent_id = $grandparent_id;
			$t->grandparent_question = $grandparent_question;
			$t->grandparent_ancestry = $dd['grandparent_ancestry'];
		}
		// $t->datetime_value = $dd['datetime_value'];
		// $t->choice_name = $dd['choice_name'];
		$t->option_set = $dd['option_set'];
		// $t->option_set_related_form = $dd['option_set_related_form'];

		if ($value) {
			if ($submissions[$submission_id]->answers[$parent_id][$ord]) {
				if ($submissions[$submission_id]->answers[$parent_id][$ord]->id != $t->id || $t->question_type=='select_multiple') {
					$submissions[$submission_id]->answers[$parent_id][$ord]->value .= "|{$value}";
					$submissions[$submission_id]->answers[$parent_id][$ord]->slug .= "|{$t->slug}";
				}
			} else {
				$submissions[$submission_id]->answers[$parent_id][$ord] = $t;
			}
		}
	}

	foreach ($submissions as $submission_id=>$submission) {
		foreach ($submission->answers as $parent_id=>$answers) {
			if (count($answers) == 0) {
				unset($submissions[$submission_id]->answers[$parent_id]);
			} else {

				ksort($submissions[$submission_id]->answers[$parent_id]);

				foreach ($submissions[$submission_id]->answers[$parent_id] as $n=>$answer) {
					if ($answer->value) {
						$t = new stdClass();
						$t->code = $answer->code;
						$t->value = $answer->value;
						$submissions[$submission_id]->answers[$parent_id][$n]->codeValue = $t;
					}
				}
			}
		}
	}

	return [$submissions, $questions];
}

function formataTodosOsDadosEmBlocos($submission, $questions, $form_id, $camposAIgnorar) {
	global $colheita;
	$sql = "select id, ancestry, ancestry_depth, group_name_translations from form_items where type='QingGroup' and ancestry_depth > 0 and form_id = '{$form_id}' order by ancestry_depth";
	$result = pg_query($colheita->PG, $sql);
	$blocos = pg_fetch_all($result);
	// pT($blocos);exit;

	$s = [];
	$raiz = '';
	foreach ($blocos as $b) {
		$id = $b['id'];
		$sups = explode('/', $b['ancestry']);
		$t = new stdClass();
		$t->title = colheitaTraduz($b['group_name_translations']);
		$t->subs = [];
		$t->answers = [];
		switch ($b['ancestry_depth']) {
			case 1:
				// pT($s);pT($sups);pT($b['ancestry']);exit;
				if (!$s[$id]) {
					$s[$id] = $t;
				}
				break;
			case 2:
				if (!$s[$sups[1]]->subs[$id]) {
					$s[$sups[1]]->subs[$id] = $t;
				}
				break;
			case 3:
				if (!$s[$sups[1]]->subs[$sups[2]]->subs[$id]) {
					$s[$sups[1]]->subs[$sups[2]]->subs[$id] = $t;
				}
				break;
		}
	}





	$dadosEmBlocos = [];

	foreach ($submission->answers as $parent_id=>$answers) {
		$first = array_values($answers)[0];
		$sups = explode('/', $first->ancestry);
		if (!$sups[1]) {
			// pT($answers);exit;
			continue; // TODO: tratar casos em que a pergunta está na raiz.
		}
		$c = count($sups);
		if (!$dadosEmBlocos[$sups[1]]) {
			$dadosEmBlocos[$sups[1]] = $s[$sups[1]];
		}
		if ($sups[2] && !$dadosEmBlocos[$sups[1]]->subs[$sups[2]]) {
			$dadosEmBlocos[$sups[1]]->subs[$sups[2]] = $s[$sups[1]]->subs[$sups[2]];
		}
		if ($sups[3] && !$dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]]) {
			$dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]] = $s[$sups[1]]->subs[$sups[2]]->subs[$sups[3]];
		}

		$smallerAnswers = [];
		foreach ($answers as $a) {
			if (!in_array($a->code, $camposAIgnorar)) {
				if (is_string($a->value)) {
					$aVals = explode('|', $a->value);
					$newAVals = [];
					foreach ($aVals as $ii=>$aVal) {
						$decoded = json_decode($aVal);
						$newAVals[] = ($decoded) ? $decoded : $aVal;
					}
					if (count($newAVals) == 1) {
						$newAVals = $newAVals[0];
					}
				} else {
					$newAVals = $a->value;
				}
				$t = new stdClass();
				$t->id = $a->id;
				$t->code = $a->code;
				$t->question = $questions[$a->code];
				$t->value = $newAVals;
				$smallerAnswers[] = $t;
			}
		}

		if (count($smallerAnswers)) {
			if ($c==2) {
				$dadosEmBlocos[$sups[1]]->answers[$parent_id] = $smallerAnswers;
			} else if ($c==3) {
				$dadosEmBlocos[$sups[1]]->subs[$sups[2]]->answers[$parent_id] = $smallerAnswers;
			} else if ($c > 3) {
				$dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]]->answers[$parent_id] = $smallerAnswers;
			}
		}
	}

	foreach ($dadosEmBlocos as $idUm=>$x) {
		$temAnswers = true;
		$temSubs = true;
		if (!$x->answers || !count($x->answers)) {
			unset($dadosEmBlocos[$idUm]->answers);
			$temAnswers = false;
		}
		$temAlgumSSubs = false;
		$temAlgumSAnswers = false;
		if (!$x->subs || !count($x->subs)) {
			unset($dadosEmBlocos[$idUm]->subs);
			$temSubs = false;
		} else {
			$temSSubs = [];
			$temSAnswers = [];
			foreach ($x->subs as $idDois=>$xx) {
				$temSSubs[$idDois] = true;
				$temSAnswers[$idDois] = true;
				if (!$xx->answers || !count($xx->answers)) {
					unset($dadosEmBlocos[$idUm]->subs[$idDois]->answers);
					$temSAnswers[$idDois] = false;
				}
				if (!$xx->subs || !count($xx->subs)) {
					unset ($dadosEmBlocos[$idUm]->subs[$idDois]->subs);
					$temSSubs[$idDois] = false;
				}
				if (!$temSAnswers[$idDois] && !$temSSubs[$idDois]) {
					unset($dadosEmBlocos[$idUm]->subs[$idDois]);
				}
			}
			foreach ($temSSubs as $idDois=>$temSSub) {
				$temSAnswer = $temSAnswers[$idDois];
				if ($temSSub) {
					$temAlgumSSubs = true;
				}
				if ($temSAnswer) {
					$temAlgumSAnswers = true;
				}
			}
		}
		if (!$temAnswers && (!$temSubs || !$temAlgumSAnswers)) {
			unset($dadosEmBlocos[$idUm]);
		} else if (!$temAlgumSSubs && !$temAlgumSAnswers) {
			unset($dadosEmBlocos[$idUm]->subs);
		}
	}
	// pT($dadosEmBlocos);exit;

	return $dadosEmBlocos;
}
?>
