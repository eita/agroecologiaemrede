<?php
require_once "config.php";
require_once "api_legacy.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
$db = conecta($bd);
$sql = "SELECT id FROM lista_areas_tematicas";
$areas_tematicas = faz_query($sql, '', 'array')[0];
$sql = "SELECT * FROM frm_exp_base_comum";
$dadosAtuaisRaw = faz_query($sql, '', 'object');

$d = $dadosAtuaisRaw[0];
if (!property_exists($dadosAtuaisRaw[0], 'ex_areas_tematicas_bckp')) {
	$sql = "ALTER TABLE `frm_exp_base_comum` ADD `ex_areas_tematicas_bckp` TEXT AFTER `ex_areas_tematicas`";
	$res = faz_query($sql);
	$sql = "UPDATE frm_exp_base_comum SET ex_areas_tematicas_bckp=ex_areas_tematicas";
	$res = faz_query($sql);
}

$dadosAtuais = [];
foreach ($dadosAtuaisRaw as $d) {
	$dadosAtuais[$d->ex_id] = $d;
}

$arq = $dir['base'].'frm_exp_base_comum.csv';
if (($handle = fopen($arq, "r")) !== FALSE) {
	$titulos = fgetcsv($handle, 1000, ",");
    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$t = new stdClass();
        for ($i = 0; $i < count($row); $i++) {
			$titulo = $titulos[$i];
			$valor = trim($row[$i]);
			if ($titulo == 'ex_areas_tematicas_nova') {
				$valores = [];
				$dAreasTematicas = explode('|', $valor);
				foreach ($dAreasTematicas as $dAreaTematica) {
					$valores[] = slugify($dAreaTematica, '_');
					if ($valor && !in_array(slugify($dAreaTematica, '_'), $areas_tematicas)) {
						echo 'FERROU';
						pR($valor);
						pR($dAreaTematica);
						pR($areas_tematicas);
						exit;
					}
				}
				$valor = implode("|", $valores);
			}
			$t->$titulo = $valor;
        }
		if (!$t->ex_id) {
			$naoExiste = true;
			foreach ($dadosAtuais as $ex_id=>$dadoAtual) {
				$slugAtual = slugify(trim($t->ex_descricao));
				$slug = slugify(trim($dadoAtual->ex_descricao));
				if ($slugAtual == $slug && strval(trim($t->dt_criacao)) == strval(trim($dadoAtual->dt_criacao)) && strval(trim($t->ref_lat)) == strval(trim($dadoAtual->ref_lat))) {
					$naoExiste = false;
					$t->ex_id = $ex_id;
					break;
				}
			}
			if ($naoExiste) {
				pR('T');
				pR($t);
				echo 'FERROU';
				exit;
			}
		}
		if ($t->vale_a_pena == 'N') {
			$t->ex_liberacao = 'R';
		}
		// $dados[] = $t;

		// Migração à base de dados. Momentos de tensão...
		$set = array();
		$dadoAtual = $dadosAtuais[$t->ex_id];
		$dadoAtual->ex_areas_tematicas = $t->ex_areas_tematicas_nova;
		$set[] = "ex_areas_tematicas='{$dadoAtual->ex_areas_tematicas}'";
		if ($t->ex_liberacao == 'R') {
			$dadoAtual->ex_liberacao = $t->ex_liberacao;
			$set[] = "ex_liberacao='{$dadoAtual->ex_liberacao}'";
		}
		$sql = "UPDATE frm_exp_base_comum SET " . implode(', ', $set) . " WHERE ex_id='{$dadoAtual->ex_id}'";
		faz_query($sql);
		echo "<H1>{$dadoAtual->ex_descricao}</H1>";
		echo "<p>$sql</p>".chr(10);
    }
    fclose($handle);
}

$db->close();
?>
