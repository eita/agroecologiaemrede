<?php
require_once "mapeo/config.php";
require_once "mapeo/api_legacy.php";
require_once "mapeo/api_colheita.php";
mb_internal_encoding("UTF-8");
require_once "{$dir['apoio']}funcoes_comuns.php";
require_once "mapeo/script_SQL_da_colheita.php";
$db = conecta($bd);
pT("ETL colheita => AeR","h1");
$u =faz_login_admin($colheita->AER_LOGIN,$colheita->AER_SENHA);
if (!$colheita || !$u) {
	pT("Acesso não autorizado");
	exit;
}
$extraHeaders = array("Authorization: Token token=".$colheita->API_TOKEN);

# Comandos:
# isItPublic => o valor é a resposta que autoriza a publicização. Ex: "sim"
# fonteEhLatLng => o valor é um par lat/lng, dividido pelo caractere ' ' (espaço)
# slugify => o valor da resposta tem que ser slugified
# georref => o valor da resposta é um município, estado ou país - deve-se passar o ID da cidade
# estado => (só vale para georref) o valor da resposta é um estado - deve-se passar o ID
# pais => (só vale para georref) o valor da resposta é um país - deve-se passar o ID
# macrorregial => (só vale para georref) o valor da resposta é uma macrorregião - TODO!!
# associaCode => transforma o valor em um json com nome do código da questão e o valor
# associaValue => transforma o valor em um json com nome do valor de uma outra questão e o valor desta questão
# limite => valor máximo de caracteres do texto (se for mais, truncar)
# stripTags => remove tudo que é html, com exceção de <a> e transforma <p> em <br>
foreach($colheita->ETL as $form_id=>$ETL) {
	$camposAIgnorar = $colheita->camposAIgnorar[$form_id];
	foreach ($ETL as $es) {
		if (in_array('de', array_keys($es))) {
			$parts = explode(':', $es['de']);
			if (count($parts) == 1 && is_array($camposAIgnorar) && !in_array($es['de'], $camposAIgnorar)) {
				$camposAIgnorar[] = $es['de'];
			}
		} else if (in_array('cria_apoio', array_keys($es))) {
			foreach ($es['cria_apoio']['ETL'] as $ees) {
				$parts = explode(':', $ees['de']);
				if (count($parts) == 1 && is_array($camposAIgnorar) && !in_array($ees['de'], $camposAIgnorar)) {
					$camposAIgnorar[] = $ees['de'];
				}
			}
		}
	}
	$fonte = $colheita->FONTES[$form_id];
	$tipofrm = $fonte->tipo;
	$frm = ($tipofrm != 'experiencia')
		? "frm_{$tipofrm}"
		: "frm_exp_base_comum";
	$cmpo = $campo[$frm];
	$missao = $fonte->missao;
	pT("Missão $missao, Formulário $form_id", "h1");
	if (in_array($fonte->flow, ['skip', 'questionsOnly'])) {
		echo "Formulário ignorado pela configuração.".chr(10);
		continue;
	}
	$rows = [];

	list($submissions, $questions) = extract_submissions($form_id);

	$nAddedSubmissions = 0;
	$nUpdatedSubmissions = 0;
	$nRemovedSubmissions = 0;
	$nDownloadedFiles = 0;
	$georrefs = [];
	foreach ($submissions as $submission_id=>$submission) {
		$foundLocations = false;
		$rowAllowed = true;
		pT("Submissão {$submission_id}... ", "h2");

		if ($fonte->downloadFiles) {
			// BAIXA ARQUIVOS DA SUBMISSÃO:
			pT(chr(10).'Baixando arquivos de mídia do servidor', 'h3');
			foreach ($submission->answers as $parent_id=>$answers) {
				foreach ($answers as $ord=>$a) {
					if (is_object($a->value) && $a->value->fileid) {
						$val = $a->value;
						$fileUrl = "{$colheita->BASE_URL}/pt/m/{$missao}/{$val->itemtype}/{$val->fileid}";
						list($file, $fileHeaders) = pega_xml_arquivo($fileUrl, 'raw', $extraHeaders, true);
						list($fileCmd, $filename) = explode("=", explode(";", $fileHeaders["content-disposition"][0])[1]);
						$filename = substr($filename,1,strlen($filename)-2);
						$filename = str_replace(["elmo", ".jpeg"], ["frm_$tipofrm", ".jpg"], strtolower($filename));
						$arq = $dir['upload'].$filename;
						if (!file_exists($arq) || filesize($arq) == 0) {
							list($file, $fileHeaders) = pega_xml_arquivo($fileUrl, 'raw', $extraHeaders);
							$h = fopen($arq, "w+");
							fputs($h, $file);
							fclose($h);
							pT("Arquivo {$filename} baixado com sucesso.");
							$nDownloadedFiles++;
						} else {
							pT("Arquivo {$filename} já disponível no nosso servidor.");
						}
						unset($val->fileid, $val->filetype);
						$val->arq = $filename;
						// $val->url = $dir['upload_URL'].$filename;
						$submissions[$submission_id]->answers[$parent_id][$ord]->value = $val;
					}
				}
			}
			pT('Encerrada a busca de arquivos no servidor.');
		} else {
			pT(chr(10).'Arquivos de mídia não baixados por conta da configuração.', 'h3');
		}


		$row = [
			$cmpo->id=>$submission_id,
			$cmpo->mapeamento=>$missao,
			"dt_criacao"=>$submission->created_at,
			"dt_atualizacao"=>$submission->updated_at,
		];
		foreach ($ETL as $e) {
			$criaApoio = $e["cria_apoio"];
			$pra = $e["pra"];

			// Comandos:
			$cmds = ($e["cmd"]) ? $e["cmd"] : [];
			$fonteEhLatLng = in_array('fonteEhLatLng',$cmds);
			$slugify = in_array('slugify',$cmds);
			$georref = in_array('georref',$cmds);
			$estado = in_array('estado',$cmds);
			$pais = in_array('pais',$cmds);
			$associaCode = in_array('associaCode',$cmds);
			$stripTags = in_array('stripTags',$cmds);
			$associaValue = (isset($cmds['associaValue'])) ? $cmds['associaValue'] : false;
			$limite = (isset($cmds['limite'])) ? $cmds['limite'] : false;

			if (isset($cmds['isItPublic']) && $cmds['isItPublic']) {
				foreach ($submission->answers as $parent_id=>$answers) {
					if (!$rowAllowed) {
						pT("A/o usuária/o não autoriza a disponibilização pública deste formulário.");
						$sql = "SELECT {$cmpo->id} FROM {$frm} WHERE {$cmpo->id}='{$submission_id}'";
						$res = faz_query($sql, '', 'object');
						if (count($res) > 0) {
							$sql = "DELETE FROM {$frm} WHERE {$cmpo->id}='{$submission_id}'";
							faz_query($sql);
							if ($frm == 'frm_exp_base_comum') {
								$sql = "DELETE FROM `frm_exp_geral` WHERE `{$campo['frm_exp_geral']->id}` = '{$submission_id}'";
								faz_query($sql);
							}
							pT("Ela estava de fato pública. Agora foi apagada com sucesso.");
							$nRemovedSubmissions++;
						}
						break;
					}
					foreach ($answers as $answer) {
						if ($e["de"] == $answer->code) {
							$rowAllowed = ($slugify)
								? ($answer->slug == slugify($cmds['isItPublic'], '_'))
								: ($answer->value == $cmds['isItPublic']);
							break;
						}
					}
				}
			}
			if (!$rowAllowed) {
				break;
			}

			if ($georref && !$georrefs[$pra]) {
				if ($pais) {
					$georrefs[$pra] = 'pais';
				} else if ($estado) {
					$georrefs[$pra] = 'estado';
				} else {
					$georrefs[$pra] = 'cidade';
				}
			}

			if ($criaApoio) {
				$apoioTab = $criaApoio["apoio_tab"];
				$unique = $criaApoio["unique"];
				$idRef = $criaApoio["id_ref"];
				$noAutoincrement = $criaApoio["noAutoincrement"];

				$constList = [];
				$apoioValores = [];
				$parentIds = [];
				$pras = [];
				foreach ($criaApoio["ETL"] as $ae) {
					$apoioDe = $ae["de"];
					$apoioPra = $ae["pra"];
					$apoioCmds = ($ae["cmd"]) ? $ae["cmd"] : [];
					$apoioSlugify = in_array('slugify',$apoioCmds);
					$apoioStripTags = in_array('stripTags',$apoioCmds);
					$apoioLimite = (isset($apoioCmds['limite'])) ? $apoioCmds['limite'] : false;
					$pras[] = $apoioPra;
					list($type, $const) = explode(":", $apoioDe);
					if (!$const) {
						foreach ($submissions[$submission_id]->answers as $parent_id=>$answers) {
							foreach ($answers as $ord=>$answer) {
								if ($answer->code == $apoioDe) {
									if (!isset($apoioValores[$parent_id])) {
										$apoioValores[$parent_id] = [];
									}
									$val = $answer->value;
									if (is_object($val)) {
										$val = trim(mysqli_real_escape_string($db, json_encode($val)));
									} else {
										$val = trim(mysqli_real_escape_string($db, $val));
									}
									if ($apoioStripTags) {
										$val = trim(mysqli_real_escape_string($db, stripTags($val)));
									}
									if ($apoioLimite) {
										$val = truncate(trim(mysqli_real_escape_string($db, $answer->value)), $apoioLimite);
									}

									$t = new stdClass();
									$t->val = $val;
									$t->apoioDe = $apoioDe;
									$t->pra = $apoioPra;
									$apoioValores[$parent_id][] = $t;
								}
							}
						}
					} else {
						$constList[$apoioPra] = $const;
					}
				}
				foreach ($apoioValores as $parent_id=>$apoioRow) {
					$codes = array_keys($constList);
					foreach ($apoioRow as $a) {
						$codes[] = $a->pra;
					}
					foreach ($pras as $p) {
						if (!in_array($p, $codes)) {
							unset($apoioValores[$parent_id]);
						}
					}
				}
				//DEBUG print_r($apoioValores);
				if (count($apoioValores)) {
					$newVals = [];
					foreach ($apoioValores as $apoioRow) {
						$campos = array_keys($constList);
						$apoioValues = array_values($constList);
						foreach ($apoioRow as $a) {
							$campos[] = $a->pra;
							$apoioValues[] = $a->val;
						}
						$temValor = false;
						$valido = false;
						$idApoio = null;
						foreach ($apoioRow as $a) {
							$vt = $a->val;
							$ct = $a->pra;
							if ($vt && $vt != $const) {
								$temValor = true;
							}
							if ($ct == $unique) {
								if (trim($vt) != "") {
									$valido = true;
									$sql = "SELECT $idRef FROM $apoioTab WHERE $ct like '$vt'";
									$ret = faz_query($sql, $db, 'object');
									$idApoio = $ret[0]->{$idRef};
								}
							}
						}
						if ($temValor && $valido) {
							if ($idApoio) {
								$newVals[] = $idApoio;
							} else {
								$apoioCamposRow = implode("`, `", $campos);
								$apoioValoresRow = implode("', '", $apoioValues);
								$idCriado = null;
								if ($noAutoincrement) {
									$idCriado = $pra . '-' . substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 6) . '-' . strval(round(microtime(true)*1000));
									$apoioCamposRow = "{$idRef}`, `{$apoioCamposRow}";
									$apoioValoresRow = "{$idCriado}', '{$apoioValoresRow}";
								}
								$sql = "INSERT INTO $apoioTab (`$apoioCamposRow`) VALUES ('$apoioValoresRow')";
								// echo chr(10).chr(10).$sql.chr(10);
								$idAuto = faz_query($sql, $db, 'id');
								$newVals[] = ($idCriado) ? $idCriado : $idAuto;
							}
						}
					}
					$row[$pra] .= (count($newVals) > 0 && $row[$pra]) ? '|' : '';
					$row[$pra] .= implode("|", $newVals);
				} else {
					$row[$pra] .= '';
				}
			} else {
				$de = $e["de"];
				$exploded = (explode(':', $de));
				if (count($exploded) == 2 && in_array($exploded[0], ['string', 'number'])) {
					// Este é o caso em que o "de" é uma string direta (constante):
					switch ($exploded[0]) {
						case 'string':
							$row[$pra] = $exploded[1];
							break;
						case 'number':
							$row[$pra] = intval($exploded[1]);
							break;
					}
				} else {
					// E aqui vão os outros casos, em que o 'de' é realmente um campo válido:
					foreach ($submissions[$submission_id]->answers as $parent_id=>$answers) {
						foreach ($answers as $ord=>$a) {
							if ($a->code == $de) {
								$vals = (is_object($a->value))
									? [$a->value]
									: explode("|", $a->value);
								$slugs = explode("|", $a->slug);
								$type = $a->question_type;
								$code = $a->code;
								$newVals = [];
								foreach ($vals as $j=>$val) {
									if (is_string($val)) {
										if ($stripTags) {
											$val = stripTags($val);
										}
										$val = trim(mysqli_real_escape_string($db, $val));
									}
									if (is_string($val) && $limite) {
										$val = truncate(trim(mysqli_real_escape_string($db, $val)), $limite);
									}

									// Treat depending on type and commands:
									if ($slugify) {
										if ($slugs[$j]) {
											$val = $slugs[$j];
											$newVals[] = $val;
										}
									} else if ($e["apoio"]) {
										if (!is_string($val)) {
											$val = json_encode($val);
										}
										$sql = "SELECT id FROM ".$e['apoio']." WHERE nome like '$val'";
										$res = faz_query($sql, $db, 'object');
										if (count($res) > 0) {
											$newVals[] = $res[0]->id;
										}
										// print_r([$e, $sql, $res]);
									} else if ($type == 'location') {
										if (!is_string($val)) {
											$val = json_encode($val);
										}
										$latLng = explode(' ',$val);
										if (count($latLng) == 2) {
											$lat = floatval($latLng[0]);
											$lng = floatval($latLng[1]);
											// A longitude 150 é quando a pessoa não escolhe nada. Problema do colheita. TODO: dar valor NULO para lat/lng quando a pessoa não escolhe nada!
											if ($lng < 150) {
												$row[$cmpo->localizacao_lat] = $lat;
												$row[$cmpo->localizacao_lng] = $lng;
												$foundLocations = true;
											}
										}
									} else if ($associaCode) {
										if ($val) {
											$t = new stdClass();
											$t->question = trim(mysqli_real_escape_string($db, $questions[$a->code]));
											$t->value = (is_string($a->value))
												? trim(mysqli_real_escape_string($db, $a->value))
												: $a->value;
											$newVals[] = json_encode($t);
										}
									} else if ($associaValue) {
										if ($val) {
											if (is_object($val)) {
												$t = $val;
											} else {
												$t = new stdClass();
												$t->value = $val;
											}
											$constValue = explode(':',$associaValue);
											if ($constValue[1]) {
												$t->nome = $constValue[0];
											} else {
												if (is_array($camposAIgnorar)) {
													$camposAIgnorar[] = $associaValue;
												}
												foreach ($answers as $aa) {
													if ($aa->$associaValue) {
														$t->nome = $aa->$associaValue;
														break;
													}
												}
											}
											if ($t->nome || is_object($val)) {
												$newVals[] = json_encode($t);
											}
										}
									} else {
										if ($val) {
											$newVals[] = (is_object($val))
												? json_encode($val)
												: trim(mysqli_real_escape_string($db, $val));
										}
									}
								}
								if ($pra && $type != 'location') {
									$row[$pra] .= ($row[$pra] && count($newVals)) ? '|' : '';
									$row[$pra] .= implode("|", $newVals);
								}
							}
						}
					}
				}
			}
		} // fim do loop sobre o ETL
		if (!$rowAllowed) {
			continue;
		}

		// Convert georrefs to codes:
		foreach ($row as $pra=>$r) {
			if ($r && $georrefs[$pra]) {
				// print_r([$pra, $r]);exit;
				$vals = explode('|', $r);
				if ($georrefs[$pra] == 'estado') {
					//Estados:
					$newVals = [];
					foreach ($vals as $val) {
						$uf = estadoParaUF($val);
						$sql = "SELECT id FROM _BR_maes WHERE UF='$uf'";
						$res = faz_query($sql, $db, 'object');
						if (count($res) > 0) {
							$newVals[] = $res[0]->id;
						}
					}
					$row[$pra] = implode('|', $newVals);
					// pR([$pra, 'Estado', $vals, $row[$pra]]);
				} else if ($georrefs[$pra] == 'pais') {
					//Países:
					$newVals = [];
					foreach ($vals as $val) {
						$sql = "SELECT id FROM __paises WHERE nome like '%:$val|%'";
						$res = faz_query($sql, $db, 'object');
						if (count($res)>0) {
							$newVals[] = $res[0]->id;
						}
					}
					$row[$pra] = implode('|', $newVals);
					// pR([$pra, 'País', $vals, $row[$pra]]);
				} else {
					// Municípios:
					$c = count($vals);
					// pT($vals);exit;
					$newVals = [];
					// $m = intval($c/2);
					for ($i = 0; $i < $c; $i+=2) {
						$uf = estadoParaUF($vals[$i]);
						$cidade = mysqli_real_escape_string($db, $vals[$i+1]);
						if ($cidade && $uf) {
							$sql = "SELECT id, lat, lng FROM _BR WHERE UF='$uf' AND nome like '$cidade'";
							$res = faz_query($sql, $db, 'object');
							// echo chr(10).$sql;
							// print_r($res);
							if ($res[0]->id) {
								$newVals[] = $res[0]->id;
								if ($pra == $cmpo->localizacao && $res[0]->lat && !$foundLocations) {
									$foundLocations = true;
									$row[$cmpo->localizacao_lat] = $res[0]->lat;
									$row[$cmpo->localizacao_lng] = $res[0]->lng;
								}
							}
						}
					}
					$row[$pra] = implode('|', $newVals);
					// pR([$pra, 'Municipio', $vals, $row[$pra]]);
				}
			} // fim do if georref
		}

		if (!$foundLocations) {
			$row[$cmpo->localizacao_lat] = 0;
			$row[$cmpo->localizacao_lng] = 0;
		}

		// Remove duplicates:
		foreach ($row as $pra=>$r) {
			$vals = explode("|", $r);
			$singleVals = [];
			foreach ($vals as $val) {
				if (!in_array($val, $singleVals)) {
					$singleVals[] = $val;
				}
			}
			$row[$pra] = implode("|", $singleVals);
		}

		$umDia = 24*3600;

		// Add everything:
		// $dadosEmBlocos = null;
		if (is_array($camposAIgnorar)) {
			$dadosEmBlocos = formataTodosOsDadosEmBlocos($submission, $questions, $form_id, $camposAIgnorar);
			$row[$cmpo->tudo] = mysqli_real_escape_string($db, json_encode($dadosEmBlocos));
			// pR($row[$cmpo->tudo]);exit;
		} else {
			$row[$cmpo->tudo] = json_encode(new stdClass());
		}

		// Check if organization name exists with different ID! If so, merge both:
		$updateOnDuplicate = true;
		if ($frm == 'frm_instituicao') {
			$nome = $row[$cmpo->nome];
			$sql = "
				SELECT *
				FROM {$frm}
				WHERE
					`{$cmpo->nome}` like '{$nome}' AND
					`{$cmpo->id}`!='{$row[$cmpo->id]}'
			";
			$res = faz_query($sql, '', 'object');
			if (count($res)) {
				echo chr(10) . "Nome repetido '{$row[$cmpo->nome]}'! " . chr(10) ."Estou escolhendo o mais recente." . chr(10);
				$updateOnDuplicate = (
					!$res[0]->{$cmpo->atualizacao} ||
					(strtotime($row[$cmpo->atualizacao]) > (strtotime($res[0]->{$cmpo->atualizacao}) + $umDia))
				);
				$mergedIntoId = $res[0]->{$cmpo->id};
				// $sql = "UPDATE colheita_submissions SET merged_into='{$mergedIntoId}' WHERE id='{$row[$cmpo->id]}'";
				// faz_query($sql); // Não existe mais o controle colheita_submissions
				$row[$cmpo->id] = $mergedIntoId;
			}
			// /*DEBUG*/ print_r($row);
		}

		if ($updateOnDuplicate) {
			$sql = "
				SELECT
					{$cmpo->id},
					{$cmpo->atualizacao}
				FROM {$frm}
				WHERE `{$cmpo->id}`='{$row[$cmpo->id]}'
			";
			$res = faz_query($sql, $db, 'object');
			if (count($res) > 0) {
				// Exists! Check if existing row is older than this actual data
				$existingDataLastUpdatedAt = strtotime($res[0]->{$cmpo->atualizacao}) + $umDia;
				$colheitaDataLastUpdatedAt = strtotime($row[$cmpo->atualizacao]);
				$hasChanges = ($existingDataLastUpdatedAt < $colheitaDataLastUpdatedAt);

				if ($hasChanges || $fonte->flow == 'forceUpdate') {
					$updates = [];
					foreach ($row as $k=>$v) {
						if ($k != $cmpo->id) {
							$updates[] = "`{$k}`='{$v}'";
						}
					}
					$updates = implode(", ", $updates);
					$sql = "
						UPDATE {$frm}
						SET {$updates}
						WHERE `{$cmpo->id}`='{$row[$cmpo->id]}'
					";
					//DEBUG echo chr(10).$sql;exit;
					echo "Já existe. Atualizando... ";
					$nUpdatedSubmissions ++;
				} else {
					echo "Já existe e não precisa atualizações.";
				}
			} else {
				// Doesn't exist!
				$fields = implode("`, `", array_keys($row));
				$values = implode("', '", array_values($row));
				echo "Criando novo... ";
				$sql = "INSERT INTO $frm (`$fields`) VALUES ('$values')";
				$nAddedSubmissions ++;
			}
			// pT('------------','h2');pT($row);pT($sql);exit;
			faz_query($sql);
			if ($frm == 'frm_exp_base_comum') {
				$sql = "REPLACE INTO `frm_exp_geral` (`{$campo['frm_exp_geral']->id}`) VALUES ('{$row[$cmpo->id]}')";
				faz_query($sql);
			}
		}
		pT(" Feito!");
	} // fim do loop por submission do form

	// Agora retiro itens que porventura não estejam mais no colheita:
	$allSubmissionIds = implode("', '", array_keys($submissions));
	$sql = "DELETE FROM $frm WHERE `{$campo[$frm]->mapeamento}`='{$missao}' AND `{$campo[$frm]->id}` NOT IN ('{$allSubmissionIds}')";
	$res = faz_query($sql, $db, 'affected_rows');
	$nRemovedSubmissions += $res;
	if ($frm == 'frm_exp_base_comum') {
		$sql = "DELETE FROM `frm_exp_geral` WHERE `{$campo[$frm]->mapeamento}`='{$missao}' AND `{$campo['frm_exp_geral']->id}` NOT IN ('{$allSubmissionIds}')";
		faz_query($sql);
	}

	pT("$nAddedSubmissions submissões ADICIONADAS à tabela $frm");
	pT("$nUpdatedSubmissions submissões ATUALIZADAS na tabela $frm");
	pT("$nRemovedSubmissions submissões REMOVIDAS da tabela $frm");
	pT("$nDownloadedFiles arquivos BAIXADOS ao servidor local");

} // fim do loop por form_id

pT("FIM","h1");

$db->close();
?>
