<?php
require_once "config.php";
require_once "api_legacy.php";
require_once "api_colheita.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
$db = conecta($bd);
echo "<h1>Eliminação de experiências da base de dados</h1>";
$u =faz_login_admin($colheita->AER_LOGIN,$colheita->AER_SENHA);
if (!$colheita || !$u) {
	pR("Acesso não autorizado");
	exit;
}

$pass = $_REQUEST['pass'];
$id = $_REQUEST['id'];
if ($pass != $chave_scripts) {
	pR("Acesso não autorizado");
	exit;
}

$sqls = array(
	"delete from frm_exp_base_comum where ex_id = $id",
	"delete from frm_exp_cca where id = $id",
	"delete from frm_exp_geral where ex_id = $id",
	"delete from experiencia_arquivo WHERE ea_id_experiencia = $id",
	"delete from experiencia_autor WHERE exa_id_experiencia = $id",
	"delete from experiencia_chave WHERE ec_id_experiencia = $id",
	"delete from experiencia_relator WHERE exr_id_experiencia = $id",
);

foreach ($sqls as $sql) {
	$res = faz_query($sql);
	pR($sql.' - OK!');
}

?>
