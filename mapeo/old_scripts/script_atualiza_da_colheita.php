<?php
require_once "config.php";
require_once "{$dir['base']}api_legacy.php";
require_once "{$dir['base']}mapeo/api_colheita.php";
mb_internal_encoding("UTF-8");
require_once "{$dir['apoio']}funcoes_comuns.php";

$bdcon = pg_connect("dbname=colheita");

// $sql = "select * from answers where response_id='e6cb4702-d45f-4dc8-a118-08e18bb74a66'";
// $result = pg_query($bdcon, $sql);
// $dados = pg_fetch_all($result);
// print_r($dados);
// exit;


$db = conecta($bd);
pT("Atualização de dados do site AeR vindos da colheita","h1");
$u =faz_login_admin($colheita->AER_LOGIN,$colheita->AER_SENHA);
if (!$colheita || !$u) {
	pT("Acesso não autorizado");
	exit;
}

$extraHeaders = array("Authorization: Token token=".$colheita->API_TOKEN);
$baseUrl = $colheita->BASE_URL . $colheita->API_URL;
foreach ($colheita->FONTES as $form_id=>$fonte) {
	$missao = $fonte->missao;
	$tipofrm = $fonte->tipo;
	pT("Missão $missao, formulário $form_id:","h1");
	if ($fonte->flow == "skip") {
		echo "Formulário ignorado.".chr(10);
		continue;
	} else if ($fonte->flow == "questionsOnly") {
		echo "Serão colhidos apenas os nomes das questões do formulário.".chr(10);
	}
	$url = "{$baseUrl}{$missao}/responses?per_page={$colheita->PER_PAGE}&form_id={$form_id}";
	$questions = pega_dados_colheita($url, $extraHeaders, $missao, $form_id, $tipofrm, $fonte->flow);

	if ($fonte->flow == "questionsOnly") {
		foreach ($questions as $code=>$question) {
			$sql = "REPLACE INTO colheita_questions VALUES ('{$form_id}', '{$code}', '{$question}')";
			faz_query($sql);
		}
	}
}
pT("FIM.", "h1");

function pega_dados_colheita($url, $extraHeaders, $missao, $form_id, $tipofrm, $flow = "", $n = 0, $questions = []) {
	global $db, $colheita, $dir;
	$sql = "SELECT id, updated_at FROM colheita_submissions WHERE form_id='{$form_id}'";
	$dadosJaBaixados = faz_query($sql, '', 'object_assoc', 'id');

	$forceUpdate = ($flow == "forceUpdate");
	$questionsOnly = ($flow == "questionsOnly");

	echo "Baixando novo lote de submissões da API da colheita...";
	list($dados, $headers) = pega_xml_arquivo($url, 'json', $extraHeaders);
	pT(" baixado!", "p");
	#print_r($dados);exit;
	$next = "";
	$links  = explode(', ', $headers["link"][0]);
	foreach ($links as $linkRaw) {
		list($link, $rel) = explode('; ', $linkRaw);
		if ($rel == 'rel="next"') {
			$next = substr($link,1,strlen($link)-2);
		}
	}
	foreach ($dados as $d) {
		$n++;
		$timeUpdatedAt = strtotime($d->updated_at);
		if (
			!$forceUpdate &&
			!$questionsOnly &&
			$dadosJaBaixados[$d->id] &&
			$timeUpdatedAt == strtotime($dadosJaBaixados[$d->id]->updated_at)
		) {
			continue;
		}
		$id = $d->id;
		$submitter = $d->submitter;
		$created_at = date("Y-m-d H:i:s", strtotime($d->created_at));
		$updated_at = date("Y-m-d H:i:s", $timeUpdatedAt);
		/*if ($id == 'e89f2a5c-3a1f-455c-888c-c68603e2c960' || $id == 'c844d380-d58b-4b41-8955-d83af1b925d6' || $id == '847f3ba0-db1f-4023-ab80-5395726d40f4' || $id == '821b60eb-6ed8-45ae-a2c6-9019f84ad170' || $id == '23f9c524-dc02-4b96-bbf0-eb7558758fb7') {
			pT("submissão $id");
			foreach ($d->answers as $a) {
				if (substr(strtolower($a->code),0,5) == 'abran') {
					pT($a);
				}
			}
		}*/
		if (!$questionsOnly) {
			$sql = "REPLACE INTO colheita_submissions VALUES ('$id', '', '$submitter', '$created_at', '$updated_at', '$missao', '$form_id', '$n')";
			$res = faz_query($sql);
			echo "Submissão $n/".$headers["total"][0].": ";
		}
		if ($id=="e6cb4702-d45f-4dc8-a118-08e18bb74a66") {
			print_r($d->answers);exit;
		}
		foreach($d->answers as $aord=>$a) {
			#if ($a->value) {
				$aid = $a->id;
				$acode = $a->code;
				if (!$questions[$a->code]) {
					$questions[$a->code] = $a->question;
				}
				if ($questionsOnly) {
					continue;
				}
				// $aquestion = $a->question;
				$avalue = mysqli_real_escape_string($db, $a->value);
				if (strpos($avalue, '/media/') > 0) {
					print_r($avalue);
					print_r($colheita->BASE_URL.$avalue);exit;
					list($file, $fileHeaders) = pega_xml_arquivo($colheita->BASE_URL.$avalue, 'raw', $extraHeaders, true);
					print_r($fileHeaders["content-disposition"][0]);exit;
					list($fileCmd, $filename) = explode("=", explode(";", $fileHeaders["content-disposition"][0])[1]);
					$filename = substr($filename,1,strlen($filename)-2);
					$filename = str_replace(["elmo", ".jpeg"], ["frm_$tipofrm", ".jpg"], strtolower($filename));
					$arq = $dir['upload'].$filename;
					if (!file_exists($arq)) {
						list($file, $fileHeaders) = pega_xml_arquivo($colheita->BASE_URL.$avalue, 'raw', $extraHeaders);
						$h = fopen($arq, "w+");
						fputs($h, $file);
						fclose($h);
						echo "S";
					} else {
						echo "N";
					}
					$avalue = $filename;
				}
				$sql = "REPLACE INTO colheita_answers VALUES ('$form_id', '$id', '$aid', '$acode', '$avalue', '$aord', '$created_at', '$updated_at')";
				$res = faz_query($sql);
				echo ".";
			#}
		}
		if (!$questionsOnly) {
			echo PHP_EOL;
		}
	}
	if ($next) {
		$questions = pega_dados_colheita($next, $extraHeaders, $missao, $form_id, $tipofrm, $flow, $n, $questions);
	} else {
		pT("fim","h1");
	}
	return $questions;
}

$db->close();
?>
