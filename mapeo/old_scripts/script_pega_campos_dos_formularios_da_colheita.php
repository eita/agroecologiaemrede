<?php
require_once "mapeo/config.php";
require_once "mapeo/api_legacy.php";
require_once "mapeo/api_colheita.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
$db = conecta($bd);
pT("Atualização de dados do site AeR vindos da colheita","h1");
$u =faz_login_admin($colheita->AER_LOGIN,$colheita->AER_SENHA);
if (!$colheita || !$u) {
	pT("Acesso não autorizado");
	exit;
}

// $extraHeaders = array("Authorization: Token token=".$colheita->API_TOKEN);
$extraHeaders = array("Authorization: Token token=7561500742de932c50f5bd3c0e789907");
foreach ($colheita->FONTES as $form_id=>$fonte) {
	$missao = $fonte->missao;
	$tipofrm = $fonte->tipo;
	$arq = "ETL/" . $tipofrm . "_" . $missao . "_" . $form_id . ".csv";
	pT("Missão $missao, formulário $form_id:","h1");
	$url = $colheita->BASE_URL.$colheita->API_URL."$missao/responses?per_page=1&form_id=$form_id";
	list($dados, $headers) = pega_xml_arquivo($url, 'json', $extraHeaders);
	if (!is_array($dados)) {
		pT("Não consegui acesso ao formulário: ele é privado?");
		continue;
	}
	$fp = fopen($arq, 'w');
	fputcsv($fp, ["id", "code", "question"]);
	foreach ($dados[0]->answers as $d) {
		fputcsv($fp, [$d->id, $d->code, $d->question]);
	}
	fclose($fp);
	pT("Feito.");
}
pT("FIM", "h1");
$db->close();
?>
