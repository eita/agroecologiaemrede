<?php
require_once "config.php";
require_once "api_legacy.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
$db = conecta($bd);
$tipofrms = [];
$tipofrms_nome = [];
$lingua = $padrao_lingua;

$tipofrms = (isset($_REQUEST['tipofrm']))
	? explode('|', $_REQUEST['tipofrm'])
	: ['experiencia'];
$sql = "SELECT DISTINCT tipo, tipo_nome FROM mapeo_formularios WHERE tipo IN ('" . implode("', '", $tipofrms) . "')";
$res = faz_query($sql,'','object');
foreach ($res as $r) {
	$tipofrms[] = $r->tipo;
	$tipofrms_nome[$r->tipo] = traduz($r->tipo_nome, $lingua);
}

$filtroGenerico = resgata_filtros($tipofrms[0], true);
$geos = explode('|',$_REQUEST[$campo[$filtroGenerico->tab]->localizacao]);

$sql = "SELECT * FROM mapeo_formularios WHERE tipo IN ('" . implode("', '", $tipofrms) . "')";
if ($filtroGenerico->sels['nomefrm']) {
	$sql .= " AND nomefrm IN (";
	foreach ($filtroGenerico->sels['nomefrm'] as $o)
		$sql .= "'".$o."',";
	$sql = substr($sql,0,-1).")";
}
$frms = faz_query($sql,'','object');
unset($filtroGenerico->sels['nomefrm'],$filtroGenerico->mapa['nomefrm']);

$csv = [];

$dados = [];
$filtros = [];
foreach ($frms as $frm) {
	$leftjoin = [];
	$filtros[$frm->tipo] = resgata_filtros($frm->tipo);
	$filtroAtual = $filtros[$frm->tipo];
	$cmpo = $campo[$frm->nomefrm];
	// $sql = "SELECT '". $frm->tipo ."' AS 'tipo', '".$frm->nomefrm."' as 'nomefrm', ".$cmpo->descricao." AS 'descricao', ".$cmpo->criacao." AS 'criacao'";
	$whereCampos = [];
	$tabsApoio = [];
	foreach ($filtroAtual->mapa as $gcampo=>$g) {
		$cmdo = $filtroAtual->cmd[$gcampo];
		$campoGenerico = extraiCampoGenerico($frm->nomefrm,$gcampo);
		$whereCampos[] = ($gcampo != $campoGenerico)
			? "a.{$gcampo} AS '{$campoGenerico}'"
			: "a.{$gcampo}";
	}
	$sql = "SELECT '{$frm->nomefrm}' AS 'frm', '{$frm->tipo}' AS 'tipo', " . implode(', ', $whereCampos);

	// FROM:
	$from = ' FROM `lista_mapeamentos` as LM, ';
	$from .= ($frm->tab_base_comum)
		? "`{$frm->nomefrm}` as b, `{$frm->tab_base_comum}` as a"
		: "`{$frm->nomefrm}` as a";

	// WHERE:
	$where = " WHERE `{$cmpo->nome}` != '' AND {$campo[$frm->nomefrm]->mapeamento}=LM.id AND LM.publica=1";
	if ($frm->tab_base_comum){
		$where .= " AND b.".$cmpo->id."=a.".$campo[$frm->tab_base_comum]->id;
	}

	if ($include_images && $frm->tipo=='experiencia') {
		$leftjoin[] = "LEFT JOIN experiencia_arquivo d ON a.ex_id=d.ea_id_experiencia";
		$sql .= ", GROUP_CONCAT(d.ea_arquivo SEPARATOR ',') AS imgs";
		$groupby = " GROUP BY a.ex_id";
	} else {
		$groupby = "";
	}

	$sql.=$from.implode(' ', $leftjoin).$where.$groupby;
	// echo $sql.chr(10).chr(10);exit;

	$res = faz_query($sql,'','object');
	if ($res){
		$dados = array_merge($dados,$res);
	}
} // fim do loop nos formulários do tipo $tipofrm
// pR($dados,1);exit;

if (count($frms) > 1) {
	if ($orderby == 'dt_criacao') {
		$orderby = 'criacao';
	}
	$dados = sortAssociativeArrayByKey($dados, $orderby, trim($order));
}

$paises = $cidades = $abrangencias = $latlngs = array();

foreach ($dados as $i=>$d) {
	$filtroAtual = $filtros[$d->tipo];
	foreach ($d as $gcampo=>$valores) {
		$cmdo = ($campo[$d->frm]->{$gcampo})
			? $filtroAtual->cmd[$campo[$d->frm]->{$gcampo}]
			: $filtroAtual->cmd[$gcampo];
		if ($cmdo->apoio && $valores && $gcampo != 'mapeamento') {
			$apoio = $cmdo->apoio;
			if (!$tabsApoio[$apoio]) {
				if ($apoio == 'lista_areas_tematicas' && $d->mapeamento == 'agrorede') {
					$tabela = 'area_tematica';
					$apoioNome = 'at_descricao';
					$apoioId = 'at_id';
				} else {
					$tabela = $apoio;
					$apoioNome = ($cmdo->campo_nome)
						? $cmdo->campo_nome
						: 'nome';
					$apoioId = ($cmdo->campo_id)
						? $cmdo->campo_id
						: 'id';
				}
				$s = "SELECT {$apoioId} AS id, {$apoioNome} AS nome FROM {$tabela}";
				$tabsApoio[$apoio] = faz_query($s, '', 'object_assoc', 'id');
			}
			$valores = explode('|', $valores);
			$newValores = [];
			foreach($valores as $valor) {
				$newValores[] = $tabsApoio[$apoio][$valor]->nome;
			}
			$dados[$i]->{$gcampo} = implode('|', $newValores);
		}
	}
	$t = $d->localizacao;
	if (($t &&
		(substr($t,2,1) == '.' && substr($t,5,1) == '.' && strlen($t) == 11) ||
		(substr($t,2,1) == '.' && strlen($t) == 5) ||
		(strlen($t) == 2)
	)) {
		if (!in_array(substr($t,0,2),$paises)) {
			$paises[]=substr($t,0,2);
		}
		if (!in_array($t,$cidades)) {
			$cidades[]=$t;
		}
	}
} // fim do loop nos dados
// Agora resgato os dados de latitude e longitude das cidades que são abrangência e/ou das cidades de tabelas que não tenham lat/lng definidas e apenas cidade:
if (count($paises)) {
	foreach ($paises as $pais) {
		$sql = "
			SELECT a.id, a.nome as cidade, b.nome as estado, c.nome as pais, a.lat, a.lng
			FROM _".$pais." as a
				LEFT JOIN _".$pais."_maes as b ON a.id_mae=b.id
				LEFT JOIN __paises as c ON c.id='".$pais."'
			WHERE a.id IN (";
		foreach ($cidades as $c) {
			if (substr($c,0,2)==$pais)
				$sql.="'".$c."', ";
		}
		$sql = substr($sql,0,-2).")";
		$res = faz_query($sql,'','object');
		if ($res) {
			$latlngs = array_merge($latlngs,$res);
		}
	}
	// Definição das lat/lng se não estão na tabela:
	foreach ($dados as $n=>$dado) {
		foreach ($latlngs as $latlng) {
			if ($latlng->id == $dado->localizacao) {
				$dados[$n]->cidadeNome = $latlng->cidade;
				$dados[$n]->estadoNome = $latlng->estado;
				$dados[$n]->paisNome = traduz($latlng->pais,$lingua);
			}
		}
		if (!$dados[$n]->paisNome) {
			$dados[$n]->cidadeNome = '';
			$dados[$n]->estadoNome = '';
			$dados[$n]->paisNome = '';
		}
	}
	// // Definição das abrangências:
	// foreach ($latlngs as $latlng) {
	// 	if ($abrangencias[$latlng->id]) {
	// 		foreach ($abrangencias[$latlng->id] as $a) {
	// 			$n++;
	// 			$t = new stdClass();
	// 			$t->id_mae=$a;
	// 			//$t->cidade=$latlng->id;
	// 			$t->localizacao_lat=$latlng->lat;
	// 			$t->localizacao_lng=$latlng->lng;
	// 			$t->icone_tipo = 'Pontogde';
	// 			$t->icone_cor = 'Verde';
	// 			$dados[$n] = $t;
	//
	// 			// para ficar hachurado, veremos!
	// 			if ($mostra_hachurado)
	// 			foreach ($dados as $di=>$da)
	// 			if ($da->id == $a) {
	// 				$dados[$di]->localizacao_lat .= ','.$latlng->lat;
	// 				$dados[$di]->localizacao_lng .= ','.$latlng->lng;
	// 			}
	// 		}
	// 	}
	// }
}

// pR($dados);exit;

$fp = fopen("../acervo/{$tipofrms[0]}.csv", 'w');
$temTitulos = false;
foreach ($dados as $d) {
	$dd = [];
	foreach ($d as $k=>$v) {
		if (!$temTitulos) {
			$titulos[] = $k;
		}
		$dd[] = $v;
	}
	if (!$temTitulos) {
		$temTitulos = true;
		fputcsv($fp, $titulos);
	}
    fputcsv($fp, $dd);
}

fclose($fp);

?>
