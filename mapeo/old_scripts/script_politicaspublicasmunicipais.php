<?php
require_once "config.php";
require_once "mapeo/api_legacy.php";
require_once "mapeo/api_colheita.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
$db = conecta($bd);
$u =faz_login_admin($colheita->AER_LOGIN,$colheita->AER_SENHA);
if (!$colheita || !$u) {
	pT("Acesso não autorizado");
	exit;
}

$extraHeaders = array("Authorization: Token token=".$colheita->API_TOKEN);
$missao = "politicaspublicasmunicipais";
$form_id = "e3c91db8-f363-4b39-8bd6-b7e51e0a6d46";
#pT("Missão $missao, formulário $form_id:","h1");
$url = $colheita->BASE_URL.$colheita->API_URL."$missao/responses?per_page=1000&form_id=$form_id";
pega_dados_colheita($url, $extraHeaders, $missao, $form_id, $tipofrm);

function pega_dados_colheita($url, $extraHeaders, $missao, $form_id, $n = 0) {
	global $db, $colheita, $dir;
	#echo "Baixando novo lote de submissões da API da colheita...";
	list($dados, $headers) = pega_xml_arquivo($url, 'json', $extraHeaders);
	#pT(" baixado!", "p");
	#print_r($dados);exit;
	#$next = "";
	#$links  = explode(', ', $headers["link"][0]);
	#foreach ($links as $linkRaw) {
	#	list($link, $rel) = explode('; ', $linkRaw);
	#	if ($rel == 'rel="next"') {
	#		$next = substr($link,1,strlen($link)-2);
	#	}
	#}
	$geojson = new stdClass();
	$geojson->type = "FeatureCollection";
	$features = [];
	$georrefCode = "identMunicipio";
	$nameCode = "identNome";
	$temasCode = "tema";
	$temasPPPCode = "temasProjPolitPubl";
	$markers = [];
	$cidades = [];
	$codes = [];
	$orderCodes = [
	  'identNome' => 'titulo',
      'identMunicipio' => 'texto',
      'identExecutor' => 'texto',
      'orgAbrangencia' => 'texto',
      'orgAbrLocal' => 'texto',
      'orgEstAbr' => 'texto',
      'orgInterEstAbr' => 'texto',
      'identFinanciador' => 'texto',
      'identTipo' => 'texto',
      'tema' => 'array',
      'temasProjPolitPubl' => 'array',
      'identAtividade' => 'texto',
      'identPopulacao' => 'numeroNaoNulo',
      'identPopRural' => 'numeroNaoNulo',
      'identPopUrbana' => 'numeroNaoNulo',
      'identPublBenef' => 'numeroNaoNulo',
      'identPeriodoExecucao' => 'texto',
      'identOrcamento' => 'moeda',
      'identDescricao' => 'texto',
      'identDestaque' => 'texto',
      'identLink' => 'link'
	];
	$ignoredCodes = [
      'orgMunAbrUnico',
      'orgMunAbr',
      'identRegiao',
      'identBioma'
	];
	foreach ($dados as $d) {
	    $marker = new stdClass();
	    $geometry = new stdClass();
	    $properties = new stdClass();
	    $geometry->type = "Point";
	    $geometry->coordinates = [$lat, $lng];
	    $t = new stdClass();
	    $t->type = "Feature";
		$id = $d->id;
		$submitter = $d->submitter;
		$created_at = date("Y-m-d H:i:s", strtotime($d->created_at));
		$updated_at = date("Y-m-d H:i:s", strtotime($d->updated_at));
		$vals = [];
		$labels = [];
		$ehArray = [];
		foreach ($d->answers as $aord=>$a) {
			if ($a->value) {
				$aid = $a->id;
				$acode = $a->code;
				$aquestion = $a->question;
				$avalue = $a->value;
				if (!in_array($acode, $codes)) {
				    $codes[] = $acode;
				}
				if (in_array($acode, array_keys($vals))) {
				    $vals[$acode] .= "|$avalue";
				    $ehArray[$acode] = true;
				} else {
				    $vals[$acode] = $avalue;
				    $labels[$acode] = $aquestion;
				    $ehArray[$acode] = false;
				}
			}
		}
		$propertiesObject = new stdClass();
		$popupContent = [];
		#foreach ($vals as $code=>$valRaw) {
		foreach ($orderCodes as $code => $tipo) {
		    if ($vals[$code]) {
		        $valRaw = $vals[$code];
                $val = ($ehArray[$code])
                    ? explode('|', $valRaw)
                    : $valRaw;
		        if ($code == $georrefCode) {
		            $uf = estadoParaUF($val[0]);
		            $cidade = mysqli_real_escape_string($db, $val[1]);
		            if ($uf == "RJ") {
		                $cidade = str_replace('Paraty', 'Parati', $cidade);
		            }
		            if ($uf == "TO") {
		                $cidade = str_replace('Couto Magalhães', 'Couto de Magalhães', $cidade);
		            }
		            $sql = "SELECT lat, lng FROM _BR WHERE UF='$uf' AND nome like '$cidade'";
		            $res = faz_query($sql, $db, 'object');
		            if ($res[0]->lat) {
		                $geometry->coordinates = [$res[0]->lat, $res[0]->lng];
		                $t->geometry = $geometry;
		                $popupContent[] = "<p><strong>" . $labels[$code] . ":</strong>$cidade / $uf</p>";
		                $marker->lon = $res[0]->lng;
		                $marker->lat = $res[0]->lat;
						if (in_array($cidade . '-' . $uf, $cidades)) {
							$marker->lon+=rand(-500,0)*(0.03/500);
							$marker->lat+=rand(-500,0)*(0.03/500);
						}
						$marker->cidade = $cidade;
						$marker->uf = $uf;
						$marker->estado = $val[0];
    	                $propertiesObject->{$labels[$code]} = "$cidade / $uf";
		            } else {
		                echo "não encontrei a cidade e UF $cidade - $uf";
		                exit;
		            }
		        } else if ($code == $nameCode) {
		            $nome = $val;
		            $popupContent[] = "<h5>$nome</h5>";
		            $propertiesObject->{$labels[$code]} = $nome;
		        } else {
		            switch ($tipo) {
		                case 'texto':
		                    $valStr = ($ehArray[$code])
		                        ? implode(", ", $val)
		                        : $val;
		                    break;
		                case 'numero':
		                    $valStr = number_format(floatval($val), 0, ',', '.');
		                    break;
						case 'numeroNaoNulo':
		                    $valStr = number_format(floatval($val), 0, ',', '.');
							if ($valStr == '0') {
								$valStr = '<i>Não informado</i>';
							}
		                    break;
		                case 'moeda':
							$valStrRaw = number_format(floatval($val), 2, ',', '.');
		                    $valStr = ($valStrRaw == '0,00') ? '<i>Não Informado</i>' : "R$ $valStrRaw";
		                    break;
		                case 'link':
		                    if ($ehArray[$code]) {
                                foreach ($val as $i=>$v) {
                                   $val[$i] = "<li><a target='_blank' href='" . $v . "'>" . $v . "</a></li>";
                                }
                                $valStr = "<ul>" . implode(", ", $val) . "</ul>";
		                    } else {
		                        $valStr = "<a target='_blank' href='" . $val . "'>" . $val . "</a>";
		                    }
		                    break;
						case 'array':
							if (is_array($val)) {
								// print_r($val);print_r($acode);print_r($id);exit;
								$val = $val[0];
							}
							$valStr = "<ul><li>" . implode("</li><li>", explode(';', $val)) . "</li></ul>";
							break;
		            }
		            if (substr($labels[$code], -1) == '.') {
		                $labels[$code] = substr($labels[$code], 0, -1);
		            }
		            $popupContent[] = "<p><strong>" . $labels[$code] . ":</strong> " . $valStr . "</p>";
		            $propertiesObject->{$labels[$code]} = $valStr;
		        }
				if ($code == $temasCode) {
					$marker->temas = explode(';', $val);
				} else if ($code == $temasPPPCode) {
					$marker->temasPPP = explode(';', $val);
				}
		    }
		}
		$popupContentStr = implode('', $popupContent);
		$properties->name = $nome;
		$properties->popupContent = "<strong>$nome</strong><br>" . $popupContentStr;
		#$t->properties = $properties;
		$t->properties = $propertiesObject;
		$marker->c = $popupContentStr;
		$features[] = $t;
		$markers[] = $marker;
		$cidades[] = $marker->cidade . '-' . $marker->uf;
	}
	$geojson->features = $features;
	header('Content-Type: text/json; charset=utf-8');
	echo json_encode($markers);
}

$db->close();
?>
