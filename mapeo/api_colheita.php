<?php
/**
 * Configurações para a Colheita. Atributos:
 * BASE_URL:	URL onde está o colheita
 * API_URL:		Rota para a API do colheita
 * PER_PAGE:	Resultados por página (caso seja usado o API)
 * API_TOKEN:	Token para acesso ao API do colheita
 * AER_LOGIN:	Login para acesso à base de dados legacy
 * AER_SENHA:	Senha para acesso à base de dados legacy
 * PG:			Conexão direta à base de dados do colheita no postgres
 * FONTES:		Mapeamentos e formulários do colheita que serão importados
 * ETL:			Configurações ETL de cada formulário de cada mapeamento
 * camposAIgnorar:	Campos de cada formulário de cada mapeamento que serão ignorados por privacidade (bypassed)
 * taxonomias:	Taxonomias do Colheita
 * @var object
 */
$colheita = (object) [
	"BASE_URL" => "https://colheita.agroecologiaemrede.org.br",
	"API_URL" => "/api/v1/m/",
	"PER_PAGE" => 50,
	"API_TOKEN" => "",
	"AER_LOGIN" => "",
	"AER_SENHA" => "",
	"PG" => "",
	"FONTES" => [],
	"ETL" => [],
	"camposAIgnorar" => [],
	"taxonomias" => new stdClass()
];

/**
 * Import access settings
 */
include 'config_colheita.php';

/**
 * Loop over colheita_etl directory to populate all ETL settings for each form in each mapeamento
 */
foreach (new DirectoryIterator($dir['colheita_etl']) as $fileInfo) {
    if($fileInfo->isDot()) continue;
	$data = file_get_contents($dir['colheita_etl'] . $fileInfo->getFilename());
	$fonte = json_decode($data);
	if (!$fonte) {
		echo "----------------" . chr(10);
		echo "JSON INVÁLIDO! Arquivo: {$fileInfo->getFilename()}" . chr(10);
		echo "----------------" . chr(10);
		exit;
	}
	$fonte->arquivo = $fileInfo->getFilename();
	$colheita->FONTES[$fonte->formId] = $fonte;
}

// Recupera os itens da taxonomia:
// select b.*, a.* from option_nodes a, options b where a.option_id=b.id and a.option_set_id='6d1b47e0-aba9-47e5-84de-70ad4b3b8411';


$colheita->taxonomias = (object) [
	"areas_tematicas" => (object) [
		"optionSetId" => "6d1b47e0-aba9-47e5-84de-70ad4b3b8411",
		"APIWordPress" => (object) [
			"name" => "Temas",
			"singular_name" => "Tema",
			"slug" => "tema",
		],
		"APILegacy" => (object) [
			"campoId" => "areas_tematicas",
			"apoioTab" => "lista_areas_tematicas",
			"apoioTabCampoId" => "id",
			"apoioTabCampoNome" => "nome"
		]
	],
	"lista_abrangencias" => (object) [
		"optionSetId" => "98b56b20-bc62-4644-89ac-9eb45d4db601",
		"APIWordPress" => (object) [
			"name" => "Abrangências",
			"singular_name" => "Abrangência",
			"slug" => "abrangencia",
		],
		"APILegacy" => (object) [
			"campoId" => "lista_abrangencias",
			"apoioTab" => "lista_abrangencias",
			"apoioTabCampoId" => "id",
			"apoioTabCampoNome" => "nome"
		]
	],
	"lista_mapeamentos" => (object) [
		"dados" => (object) [
			"agrorede" => (object) [
				"id" => "agrorede",
				"nome" => "Antigo agroecologia em rede",
				"publica" => true
			],
			"agrorede" => (object) [
				"id" => "agrorede",
				"nome" => "Antigo agroecologia em rede",
				"publica" => true
			]
		],
		"APIWordPress" => (object) [
			"name" => "Mapeamentos",
			"singular_name" => "Mapeamento",
			"slug" => "mapeamento"
		],
		"APILegacy" => (object) [
			"campoId" => "lista_mapeamentos",
			"apoioTab" => "lista_mapeamentos",
			"apoioTabCampoId" => "id",
			"apoioTabCampoNome" => "nome"
		]
	]
]

?>
