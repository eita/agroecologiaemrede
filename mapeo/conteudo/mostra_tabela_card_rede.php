<?php if (isset($pt->imgs) && $pt->imgs) { ?>
	<?php $imgsRaw = explode(',',$pt->imgs); ?>
	<?php foreach ($imgsRaw as $img) { ?>
		<?php if (file_exists($dir['upload'].$img) && is_array(getimagesize($dir['upload'].$img))) { ?>
			<?php $imgs[] = $img; ?>
		<?php } ?>
	<?php } ?>
<?php } ?>
<?php $ellipses = ' <a href="'.str_replace('{id}',$pt->id,$dir['mostra_'.$tipo.'_URL']).'">(...)</a>'; ?>
<?php if (count($imgs)>0) { ?>
	<?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
		<img class="card-img-top" src="<?=$dir['upload_URL'].$imgs[array_rand($imgs)]?>" alt="Imagem da experiência">
	<?php /* </a> */ ?>
<?php } ?>
<div class="card-body">
  <h3 class="card-title">
	  <?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
	  	<?=mb_strtolower($pt->nome)?>
	  <?php /* </a> */ ?>
  </h3>
  <?php if ($pt->descricao) { ?>
	  <p class="card-text card-descricao">
    		<?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
      			<?=truncate(strip_tags($pt->descricao),220,'...')?>
    		<?php /* </a> */ ?>
  	  </p>
  <?php } ?>
  <?php if ($pt->cidade) { ?>
		<p class="card-localizacao"><label class='card-localizacao-label'>Localização:</label> <?=$pt->cidade.'  ('.estadoParaUF($pt->estado).')'?></p>
  <?php } ?>
  <?php if (is_array($pt->areas_tematicas) && count($pt->areas_tematicas)) { ?>
	  <p class="card-temas">
		  <label class='card-temas-label'>Áreas temáticas:</label>
		  <?php foreach ($pt->areas_tematicas as $slug) { ?>
			  <a href="./?areas_tematicas=<?=$slug?>">
				  <img class='img-tema-<?= $slug ?>' src='<?= $dir['mostra_temas_URL'].$slug ?>.svg' alt="<?= $areas_tematicas[$slug]->nome ?>" title="<?= $areas_tematicas[$slug]->nome ?>">
			  </a>
		  <?php } ?>
	  </p>
  <?php } ?>
  <p class="card-tipo"><span class="badge badge-pill badge-lg badge-light"><?= $tipofrms_nome[$tipo] ?></span></p>
  <p class="card-data">Cadastrado na base em <?=date('d/m/Y',strtotime($pt->criacao))?></p>
</div>
<?php /*<div class="card-footer text-center">
	<a href="<?=str_replace('{id}',$pt->id,$dir['mostra_'.$tipo.'_URL'])?>" class="btn btn-aer-card"><?= $txt['ver_ficha_'.$tipo] ?></a>
</div>*/?>
