<?php if ($pt->anexos) { ?>
	<?php if (!is_array($pt->anexos)) { $pt->anexos = [$pt->anexos]; } ?>
	<?php foreach ($pt->anexos as $img) { ?>
		<?php if (file_exists($dir['upload'].$img->arq) && is_array(getimagesize($dir['upload'].$img->arq))) { ?>
			<?php $imgs[] = $img; ?>
		<?php } ?>
	<?php } ?>
<?php } ?>

<?php if (!$pt->foto && count($imgs) > 0) { ?>
	<a href="../<?=$tipo?>?id=<?=$pt->id?>">
		<img class="card-img-top" src="<?=$dir['upload_URL'].$imgs[array_rand($imgs)]->card?>" alt="Imagem da experiência">
	</a>
<?php } else if ($pt->foto) { ?>
	<?php $foto = is_array($pt->foto) ? $pt->foto[0] : $pt->foto; ?>
	<a href="../<?=$tipo?>?id=<?=$pt->id?>">
		<img class="card-img-top" src="<?=($foto->card) ? $foto->card : $dir['upload_URL'].$foto->arq?>" alt="Imagem da experiência">
	</a>
<?php }?>
<div class="card-body">
	<?php if ($pt->logo) { ?>
    	<div class="row">
    		<div class="col-lg-6 col-md-4 col-sm-3">
    			<a href="../<?=$tipo?>?id=<?=$pt->id?>">
					<img class="w-100" src="<?=$dir['upload_URL'].$pt->logo->arq?>" />
				</a>
    		</div>
    		<div class="col-lg-6 col-md-8 col-sm-9">
    			<h3 class="card-title title-with-image">
					<a href="../<?=$tipo?>?id=<?=$pt->id?>">
						<?=truncate(mb_strtolower($pt->nome),70, '...')?>
					</a>
				</h3>
    		</div>
    	</div>
    <?php } else { ?>
		<h3 class="card-title">
  		  <a href="../<?=$tipo?>?id=<?=$pt->id?>">
  		  	<?=mb_strtolower($pt->nome)?>
  		  </a>
  	  </h3>
    <?php } ?>
	<?php if ($pt->descricao) { ?>
		<p class="card-text card-descricao">
	  		<a href="../<?=$tipo?>?id=<?=$pt->id?>">
	    		<?=truncate(strip_tags(str_replace('\\r\\n', ' ', $pt->descricao)),220,'...')?>
	  		</a>
		</p>
	<?php } ?>
	<?php if ((string)$pt->cidade) { ?>
	  <p class="card-localizacao"><label class='card-localizacao-label'>Localização:</label>  <?=$pt->cidade.'  ('.estadoParaUF($pt->estado).')'?></p>
	<?php } ?>
	<?php if (is_array($pt->areas_tematicas) && count($pt->areas_tematicas)) { ?>
		<p class="card-temas">
			<label class='card-temas-label'>Áreas temáticas:</label>
			<?php foreach ($pt->areas_tematicas as $slug) { ?>
				<a href="./?areas_tematicas=<?=$slug?>">
					<img class='img-tema-<?= $slug ?>' src='<?= $dir['mostra_temas_URL'].$slug ?>.svg' alt="<?= $areas_tematicas[$slug]->nome ?>" title="<?= $areas_tematicas[$slug]->nome ?>">
				</a>
			<?php } ?>
		</p>
	<?php } ?>


	<?php /*foreach ($filtros[$tipo]->geraXML as $i_campo=>$i_titulo) { ?>
	  <?php if ($pt->$i_campo != "" && !$campo["frm_" . $tipo]->{$i_titulo} && !$filtros[$tipo]->cmd_mapa[$i_campo]->esconde) { ?>
		  <p class="card-text">
			  <strong><?= $i_titulo ?>:</strong> <?= pegaValorHTML($pt->$i_campo, $filtros[$tipo]->cmd[$i_campo]); ?>
		  </p>
	  <?php } ?>
	<?php } */ ?>


  <p class="card-tipo"><span class="badge badge-pill badge-lg badge-light"><?= $tipofrms_nome[$tipo] ?></span></p>
  <p class="card-data">Cadastrado na base em <?=date('d/m/Y',strtotime($pt->criacao))?></p>
</div>
<div class="card-footer text-center">
	<a href="../experiencia?id=<?= $pt->id ?>" class="btn btn-aer-card"><?= $txt['ver_ficha_'.$tipo] ?></a>
</div>
