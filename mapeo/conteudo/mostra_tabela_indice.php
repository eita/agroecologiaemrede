<?php $href = str_replace('&pgLista='.$pgAtual,'',$_SERVER['QUERY_STRING']); ?>
<?php $numptsAtual = ($pgAtual==$numpgs) ? $numpts - (($numpgs-1)*$padrao_itens_por_pagina) : $padrao_itens_por_pagina; ?>
<?php $ellipsisI = $ellipsisF = false; ?>
<?php //$p = ($numpgs<=11) ? '' : ' p-1'; ?>
<?php if ($numpgs>1) { ?>
	<ul class="pagination pagination-sm" style="flex-wrap: wrap;">
		<li class="page-item<?=($pgAtual>1) ? '' : ' disabled'?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$pgAtual-1?>"><i class="fa fa-chevron-left"></i></a></li>
		<?php for ($i=1;$i<=$numpgs;$i++) { ?>
			<?php if ($numpgs<=11 || ($i<3 || $i>$numpgs-2 || ($i>$pgAtual-2 && $i<$pgAtual+2) ) ) { ?>
				<li class="page-item<?=($i==$pgAtual) ? ' active' : ''?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$i?>"><?=$i?></a></li>
			<?php } else if (($i<$pgAtual && !$ellipsisI) || ($i>$pgAtual && !$ellipsisF)) { ?>
				<li class="page-item disabled"><a class="page-link<?=$p?>" href="#"><i class="fa fa-ellipsis-h"></i></a></li>
				<?php if ($i<$pgAtual) { ?>
					<?php $ellipsisI = true; ?>
				<?php } else { ?>
					<?php $ellipsisF = true; ?>
				<?php } ?>
			<?php } ?>
		<?php } ?>
		<li class="page-item<?=($pgAtual<$numpgs) ? '' : ' disabled'?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$pgAtual+1?>"><i class="fa fa-chevron-right"></i></a></li>
	</ul>
<?php } ?>
