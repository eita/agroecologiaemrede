<?php $n=0; ?>

<div class="row<?= (EMBEDADO) ? ' d-none' : ''; ?>" id="selecaoAtual">
	<?php mostraSelecaoAtual($numpts, $filtroGenerico); ?>
</div>
<?php if (!$numpts) { ?>
	<div class="row">
		<div class="col-md-12">
			<div id="semResultados">
				<div class="alert alert-danger">
					Oooops: Não há resultados para a sua busca e/ou filtros aplicados.
				</div>
			</div>
		</div>
	</div>
	<?php return; ?>
<?php } else { ?>
	<div class="card-columns" data-numpts="<?=$numpts?>">
		<?php foreach ($pts as $pt) { ?>
			<?php $n++; $imgs = array(); $tipo = $pt->tipo; ?>
				<div class="card card-<?= $tipo ?> mb-4">
					<?php include("mostra_tabela_card_{$tipo}.php"); ?>
			 	</div>
		<?php } ?>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div id="indiceRodape" class="indiceMostraTabela">
				<?php include("mostra_tabela_indice.php"); ?>
				<?php /* faz_indice_tabela($numpts, $numpgs, $pgAtual, 'indiceRodape')*/ ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php
