<?php $ellipses = ' <a href="'.str_replace('{id}',$pt->id,$dir['mostra_'.$tipo.'_URL']).'">(&hellip;)</a>'; ?>
<?php if (isset($pt->disp_anexos) && $pt->disp_anexos) { ?>
	<?php $anexos = pegaImagens($pt->disp_anexos, $filtros[$tipo]->cmd['disp_anexos']); ?>
	<?php if ($pt->foto) { ?>
		<?php /* <a href="<?=str_replace('{id}',$pt->id,$dir["mostra_{$tipo}_URL"])?>"> */ ?>
			<img class="card-img-top" src="<?=$dir['upload_URL'].$pt->foto->arq?>" alt="Imagem do dispositivo">
		<?php /* </a> */ ?>
	<?php } ?>
<?php } ?>
<?php if (count($imgs)>0) { ?>
	<?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
		<img class="card-img-top" src="<?=$dir['upload_URL'].$imgs[array_rand($imgs)]?>" alt="Imagens">
	<?php /* </a> */ ?>
<?php } ?>
<div class="card-body">
	<?php if ($pt->logo) { ?>
	 <div class="row">
		 <div class="col-lg-6 col-md-4 col-sm-3">
			 <?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
				 <img class="w-100" src="<?=$dir['upload_URL']?><?=(is_string($pt->logo)) ? $pt->logo : $pt->logo->arq?>" />
			 <?php /* </a> */ ?>
		 </div>
		 <div class="col-lg-6 col-md-8 col-sm-9">
			 <h3 class="card-title">
		   	  <?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
		   	  	<?=mb_strtolower($pt->nome)?>
		   	  <?php /* </a> */ ?>
		     </h3>
		 </div>
	 </div>
	<?php } else { ?>
		<h3 class="card-title">
 	 	  <?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
 	 	  	<?=mb_strtolower($pt->nome)?>
 	 	  <?php /* </a> */ ?>
 	   </h3>
	<?php } ?>
	<?php if ($pt->descricao) { ?>
		<p class="card-text card-descricao">
			<?php /*<a href="../<?=$tipo?>?id=<?=$pt->id?>"> */ ?>
				<?=truncate(strip_tags($pt->descricao),220,'...')?>
			<?php /* </a> */ ?>
		</p>
	<?php } ?>
	<?php if ($pt->cidade) { ?>
		<p class="card-localizacao"><label class='card-localizacao-label'>Localização:</label>  <?=$pt->cidade.'  ('.estadoParaUF($pt->estado).')'?></p>
	<?php } ?>
	<?php foreach ($filtros[$tipo]->geraXML as $i_campo=>$i_titulo) { ?>
	  <?php if ($pt->$i_campo != "" && !$campo["frm_" . $tipo]->{$i_titulo} && !$filtros[$tipo]->cmd_mapa[$i_campo]->esconde) { ?>
		  <p class="card-text">
			  <strong><?= $i_titulo ?>:</strong> <?= pegaValorHTML($pt->$i_campo, $filtros[$tipo]->cmd[$i_campo]); ?>
		  </p>
	  <?php } ?>
	<?php } ?>
	<?php if (is_array($pt->areas_tematicas) && count($pt->areas_tematicas)) { ?>
		<p class="card-temas">
			<label class='card-temas-label'>Áreas temáticas:</label>
			<?php foreach ($pt->areas_tematicas as $slug) { ?>
				<a href="./?areas_tematicas=<?=$slug?>">
					<img class='img-tema-<?= $slug ?>' src='<?= $dir['mostra_temas_URL'].$slug ?>.svg' alt="<?= $areas_tematicas[$slug]->nome ?>" title="<?= $areas_tematicas[$slug]->nome ?>">
				</a>
			<?php } ?>
		</p>
	<?php } ?>
	<p class="card-tipo"><span class="badge badge-pill badge-lg badge-light"><?= $tipofrms_nome[$tipo] ?></span></p>
	<p class="card-data">Cadastrado na base em <?=date('d/m/Y',strtotime($pt->criacao))?></p>
</div>
<?php /* <div class="card-footer text-center">
	<a href="../<?=$tipo?>?id=<?= $pt->id ?>" class="btn btn-aer-card"><?= $txt["ver_ficha_$tipo"] ?></a>
</div> */ ?>
