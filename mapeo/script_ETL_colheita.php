<?php
include "mapeo/config.php";
include "mapeo/api_legacy.php";
include "mapeo/config_colheita.php";
include "mapeo/config_wordpress.php";

use \EITA\Utils;
use \EITA\APIColheita;
use \EITA\APILegacy;
use \EITA\APIWordPress;

/**
 * All useful methods
 * @var Utils
 */
$Utils = new Utils($bd);

$Utils->pT("ETL colheita => AeR","h1");

/**
 * Handles colheita submissions (read-only!)
 * @var APIColheita
 */
try {
	$APIColheita = new \EITA\APIColheita([
		"Utils" => $Utils,
		"dirColheitaETL" => $dir['colheita_etl'],
		"uploadPath" => $dir['upload'],
		"configColheita" => $configColheita
	]);
} catch(Exception $e) {
	echo "----------------" . chr(10);
	$Utils->pE($e->getMessage());
	echo "----------------" . chr(10);
	exit;
}
/**
 * Handles legacy submissions (read and write!)
 * @var APILegacy
 */
$APILegacy = new \EITA\APILegacy([
	'Utils' => $Utils,
	'campo' => $campo
]);

/**
 * Handles wordPress submissions (read and write!)
 * @var APIWordPress
 */
$APIWordPress = new \EITA\APIWordPress([
	'wpConfig' => $wpConfig,
	'Utils' => $Utils,
	'campo' => $campo,
	'colheitaTaxonomias' => $APIColheita->taxonomias,
    'colheitaAnexosPath' => $APIColheita->uploadPath
]);

/**
 * $fonte is the json ETL file and has the following structure:
 * $fonte = stdClass => (
 *     formId               string     The id of the Form in Colheita
 *     dependsOnFormId      string     This form "formId" must be processed after "dependsOnFormId"
 *     missao               string     The mapeamento
 *     tipo                 string     The form type. Possible options:
 *                                     "dispositivo"
 *                                     "rede"
 *                                     "organizacao"
 *                                     "experiencia"
 *     config => stdClass => (
 *         downloadFiles    boolean    True if should download files (submission attachments) from Colheita
 *         updateLegacy     boolean    True if should update Legacy API server
 *         updateWordPress  boolean    True if should update WordPress DB
 *         flow             string     The flow configurations. Possible options:
 *                                     ""            default: updates if Colheita was updated in the meantime
 *                                     "reset"       removes all submissions in target DB
 *                                     "forceUpdate" updates target DB even when there is no updates in Colheita
 *                                     "skip"        this form won't be updated at all
 *     )
 *     camposAIgnorar       array      Fields from Colheita to be ignored in update. If null,
 *                                     ignores all fields except those defined in ETC
 *     ETL                  array      The ETL commands, field per field. Possible options:
 *                                     "de"          (obligatory) Colheita's field
 *                                     "pra"         (optional) Target DB field
 *                                     "apoio"       (optional) transforms text into ID from another table
 *                                     "cria_apoio"  (optional) Array that configures ETL to related form
 *                                                   that should have submission created
 *                                                   or updated with data from Colheita's "de".
 *                                                   The options are similar to ETL.
 *                                     "cmd"         (optional) Array of commands to be applied, described below
 * )
 * List of ETL commands "cmd":
 * isItPublicValue     o valor é a resposta que autoriza a publicização. Ex: "sim"
 * fonteEhLatLng       o valor é um par lat/lng, dividido pelo caractere ' ' (espaço)
 * slugify             o valor da resposta tem que ser slugified
 * georref             o valor da resposta é um município, estado ou país - deve-se passar o ID da cidade
 * estado              (só vale para georref) o valor da resposta é um estado - deve-se passar o ID
 * pais                (só vale para georref) o valor da resposta é um país - deve-se passar o ID
 * macrorregial        (só vale para georref) o valor da resposta é uma macrorregião - TODO!!
 * associaCode         transforma o valor em um json com nome do código da questão e o valor
 * associaValue        transforma o valor em um json com nome do valor de uma outra questão e o valor desta questão
 * limite              valor máximo de caracteres do texto (se for mais, truncar)
 * stripTags           remove tudo que é html, com exceção de <a> e transforma <p> em <br>
 * isLink              o valor é uma url. Virará um objeto {"url": val}
 * @var object
 */
foreach($APIColheita->FONTES as $formId=>$fonte) {
	$Utils->pT("Missão $fonte->missao, Formulário $formId", "h1");
	$Utils->pT("Arquivo sendo processado: $fonte->arquivo", "p");

	if (in_array($fonte->config->flow, ['skip', 'questionsOnly'])) {
		$Utils->pT("Formulário ignorado pela configuração.", "h2");
		continue;
	}

	echo "Verificando autorização de acesso aos dados... ";
	try {
		$APIColheita->setForm($formId);
		echo "Ok!" . chr(10);
	} catch(Exception $e) {
		$Utils->pE("Falhou! Erro: {$e->getMessage()}");
		continue;
	}

	$APILegacy->setForm([
		'colheitaFormId' => $formId,
		'mapeamento' => $fonte->missao,
		'tipoFrm' => $fonte->tipo
	]);

	$APIWordPress->setForm([
		'colheitaFormId' => $formId,
        'colheitaFormNome' => $APIColheita->formNome,
		'mapeamento' => $fonte->missao,
		'tipoFrm' => (property_exists($fonte, "tipoWp")) ? $fonte->tipoWp : $fonte->tipo,
		'colheitaFormQuestions' => $APIColheita->questions,
		'cmpo' => $APILegacy->cmpo,
	]);

    /**
     * Stats summarizing legacy update
     * @var object
     */
	$legacyCounter = (object) [
		'nAddedSubmissions' => 0,
		'nUpdatedSubmissions' => 0,
		'nRemovedSubmissions' => 0,
		'nErrors' => 0,
		'nDownloadedFiles' => 0
	];

    /**
     * Stats summarizing WordPress update
     * @var object
     */
	$wordPressCounter = (object) [
		'nAddedSubmissions' => 0,
		'nUpdatedSubmissions' => 0,
		'nRemovedSubmissions' => 0,
		'nErrors' => 0,
		'nDownloadedFiles' => 0
	];

    // APAGA TODOS OS SUBMISSIONS, COMEÇANDO DO ZERO:
    if ($fonte->config->flow == 'reset') {
		if ($fonte->config->updateWordPress) {
            $removed = $APIWordPress->removeAllCurrentSubmissions();
			$wordPressCounter->nRemovedSubmissions += $removed;
			$Utils->pT('Por configuração deste formulário, TODAS as submissões foram apagadas!', 'h2');
		}
	}

	$georrefs = [];
	foreach ($APIColheita->submissions as $submission_id=>$submission) {
		$APIColheita->setSubmission($submission);

		$foundLocations = false;
		$foundLocationsWP = false;
		$isSubmissionAllowed = true;
		if ($fonte->config->updateLegacy) {
			$APILegacy->setSubmission(
				$submission_id,
				$submission->created_at,
				$submission->updated_at
			);
		}
		if ($fonte->config->updateWordPress) {
			$APIWordPress->setSubmission(
				$submission_id,
				$submission->created_at,
				$submission->updated_at,
				$APIColheita->dadosEspecificos()
			);
		}
		$Utils->pT("Submissão {$submission_id}... ", "h2");

		/**
		 * Downloads files from current submission in Colheita
		 */
		if ($fonte->config->downloadFiles) {
			$Utils->pT('Baixando arquivos de mídia do servidor', 'h3');
			$result = $APIColheita->downloadSubmissionFiles();
			foreach ($result->log as $msg) {
				$Utils->pT($msg);
			}
			$legacyCounter->nDownloadedFiles = $result->nDownloadedFiles;
			$Utils->pT('Encerrada a busca de arquivos no servidor.');
		} else {
			$Utils->pT('Arquivos de mídia não baixados por conta da configuração.' . chr(10), 'h3');
		}


		foreach ($fonte->ETL as $e) {
			/**
			 * Target field (legacy or wordpress)
			 * @var string
			 */
			$pra = (property_exists($e, 'pra'))
				? $e->pra
				: "";

			/**
			 * Related taxonomy or table (read and write)
			 * @var object
			 */
			$criaApoio = (property_exists($e, 'cria_apoio'))
				? $e->cria_apoio
				: null;

			/**
			 * Related taxonomy or table (read-only)
			 * @var string
			 */
			$tabelaDeApoio = (property_exists($e, 'apoio'))
				? $e->apoio
				: '';

			/**
			 * Origin field (colheita)
			 * @var string
			 */
			$de = (property_exists($e, 'de'))
				? $e->de
				: '';

			/**
			 * Commands (operations) to be applied on this field
			 * @var object
			 */
			$cmds = (property_exists($e, 'cmd'))
				? $e->cmd
				: new stdClass();

			if ($pra) {
				$APIWordPress->setTargetField($pra);
			}

			// Commands
			$fonteEhLatLng = (property_exists($cmds, 'fonteEhLatLng'));
			$slugify = (property_exists($cmds, 'slugify'));
			$georref = (property_exists($cmds, 'georref'));
			$estado = (property_exists($cmds, 'estado'));
			$pais = (property_exists($cmds, 'pais'));
			$associaCode = (property_exists($cmds, 'associaCode'));
			$stripTags = (property_exists($cmds, 'stripTags'));
            $isLink = (property_exists($cmds, 'isLink'));
			$associaValue = (property_exists($cmds, 'associaValue'))
				? $cmds->associaValue
				: "";
            $relForm = (property_exists($cmds, 'relForm'))
				? $cmds->relForm
				: "";
			$limite = (property_exists($cmds, 'limite'))
				? $cmds->limite
				: -1;
			$isItPublicValue = property_exists($cmds, 'isItPublicValue')
				? $cmds->isItPublicValue
				: '';

			if ($isItPublicValue != '') {
				foreach ($submission->answers as $parent_id=>$answers) {
					if (!$isSubmissionAllowed) {
						$Utils->pT("A/o usuária/o não autoriza a disponibilização pública deste formulário.");

						if ($fonte->config->updateLegacy) {
							$removed = $APILegacy->removeSubmission();
							if ($removed) {
								$Utils->pT("LEGACY: Ela estava de fato pública. Agora foi apagada com sucesso.");
								$legacyCounter->nRemovedSubmissions++;
							}
						}

						if ($fonte->config->updateWordPress) {
							$removed = $APIWordPress->removeCurrentSubmission();
							if ($removed) {
								$Utils->pT("WordPress: Ela estava de fato pública. Agora foi apagada com sucesso.");
								$wordPressCounter->nRemovedSubmissions++;
							}
						}

						break;
					}
					foreach ($answers as $answer) {
						if ($e->de == $answer->code) {
							$isSubmissionAllowed = ($slugify)
								? ($answer->slug == $Utils->slugify($isItPublicValue, '_'))
								: ($answer->value == $isItPublicValue);
							break;
						}
					}
				}
				// Go to next "pra".
				continue;
			}
			if (!$isSubmissionAllowed) {
				break;
			}

			if ($georref) {
				if ($fonte->config->updateLegacy && !$georrefs[$pra]) {
					if ($pais) {
						$georrefs[$pra] = 'pais';
					} else if ($estado) {
						$georrefs[$pra] = 'estado';
					} else {
						$georrefs[$pra] = 'cidade';
					}
				}
				if ($fonte->config->updateWordPress) {
					$georrefType = 'cidade';
					if ($pais) {
						$georrefType = 'pais';
					} else if ($estado) {
						$georrefType = 'estado';
					}
					$APIWordPress->addGeorref($georrefType);
				}
			}

			if ($criaApoio) {
				$apoioTab = (property_exists($criaApoio, 'apoio_tab'))
					? $criaApoio->apoio_tab
					: '';

				$unique = (property_exists($criaApoio, 'unique'))
					? $criaApoio->unique
					: '';

				$idRef = (property_exists($criaApoio, 'id_ref'))
					? $criaApoio->id_ref
					: '';

				$noAutoincrement = (property_exists($criaApoio, 'noAutoincrement'));

				$constList = [];
				$apoioValores = [];
				$parentIds = [];
				$pras = [];
				foreach ($criaApoio->ETL as $ae) {
					$apoioDe = $ae->de;
					$apoioPra = $ae->pra;
					$apoioCmds = (property_exists($ae, "cmd"))
						? $ae->cmd
						: new stdClass();
					$apoioSlugify = property_exists($apoioCmds, 'slugify');
					$apoioStripTags = property_exists($apoioCmds, 'stripTags');
					$apoioLimite = (property_exists($apoioCmds, 'limite'))
						? $apoioCmds->limite
						: -1;

					$pras[] = $apoioPra;
					list($type, $const) = explode(":", $apoioDe);
					if (!$const) {
						foreach ($APIColheita->submissions[$submission_id]->answers as $parent_id=>$answers) {
							foreach ($answers as $ord=>$answer) {
								if ($answer->code == $apoioDe) {
									if (!isset($apoioValores[$parent_id])) {
										$apoioValores[$parent_id] = [];
									}
									$val = $answer->value;
                                    $valWp = $answer->value;
									if (is_object($val)) {
										$val = $Utils->prepareString(json_encode($val));
									} else {
										$val = $Utils->prepareString($val);
									}
									if ($apoioStripTags) {
										$val = $Utils->prepareString($Utils->stripTags($val));
                                        $valWp = is_string($valWp)
                                            ? $Utils->stripTags($valWp)
                                            : $valWp;
									}
									if ($apoioLimite > 0) {
										$val = $Utils->truncate($Utils->prepareString($answer->value), $apoioLimite);
                                        $valWp = is_string($valWp)
                                            ? $Utils->truncate($valWp, $apoioLimite)
                                            : $valWp;
									}

									$apoioValores[$parent_id][] = (object) [
										"val" => $val,
										"apoioDe" => $apoioDe,
										"pra" => $apoioPra
									];
								}
							}
						}
					} else {
						$constList[$apoioPra] = $const;
					}
				}
				foreach ($apoioValores as $parent_id=>$apoioRow) {
					$codes = array_keys($constList);
					foreach ($apoioRow as $a) {
						$codes[] = $a->pra;
					}
					foreach ($pras as $p) {
						if (!in_array($p, $codes)) {
							unset($apoioValores[$parent_id]);
						}
					}
				}
				//DEBUG print_r($apoioValores);
				if (count($apoioValores)) {
					$newVals = [];
					foreach ($apoioValores as $apoioRow) {
						$campos = array_keys($constList);
						$apoioValues = array_values($constList);
						foreach ($apoioRow as $a) {
							$campos[] = $a->pra;
							$apoioValues[] = $a->val;
						}
						$temValor = false;
						$valido = false;
						$idApoio = '';
						foreach ($apoioRow as $a) {
							$vt = $a->val;
							$ct = $a->pra;
							if ($vt && $vt != $const) {
								$temValor = true;
							}
							if ($ct == $unique) {
								if (trim($vt) != "") {
									$valido = true;
									$idApoio = $APILegacy->tabelaApoioResgataIdPorNome($apoioTab, $vt, $idRef, $ct);
								}
							}
						}
						if ($temValor && $valido) {
							if ($idApoio != '') {
								$newVals[] = $idApoio;
							} else {
                                try {
                                    $id = $APILegacy->tabelaApoioInsereValores($apoioTab, $campos, $apoioValues, $noAutoincrement, $idRef);
                                    $newVals[] = $id;
                                } catch (Exception $e) {
                                    $Utils->pT("Erro inserindo dados na tabela de apoio " . $apoioTab . ". Mensagem: " . $e->getMessage());
                                }
							}
						}
					}
					$APILegacy->submission[$pra] .= (count($newVals) > 0 && $APILegacy->submission[$pra])
						? '|'
						: '';
					$APILegacy->submission[$pra] .= implode("|", $newVals);
				} else {
					$APILegacy->submission[$pra] .= '';
				}
			} else {
				$exploded = (explode(':', $de));
				if (count($exploded) == 2 && in_array($exploded[0], ['string', 'number'])) {
					// Este é o caso em que o "de" é uma string direta (constante):
					$newVal = null;
					switch ($exploded[0]) {
						case 'string':
							$newVal = $exploded[1];
							break;
						case 'number':
							$newVal = intval($exploded[1]);
							break;
					}
					if ($fonte->config->updateLegacy) {
						$APILegacy->submission[$pra] = $newVal;
					}
					if ($fonte->config->updateWordPress) {
						$APIWordPress->setTargetValue($newVal);
					}
				} else {
					// E aqui vão os outros casos, em que o 'de' é realmente um campo válido:
					foreach ($APIColheita->submissions[$submission_id]->answers as $parent_id=>$answers) {
						foreach ($answers as $ord => $a) {
							if ($a->code == $de) {
								$vals = (is_object($a->value))
									? [$a->value]
									: explode("|", $a->value);
                                $choiceNames = ($a->choice_name)
                                    ? explode("|", $a->choice_name)
                                    : [];
								$slugs = explode("|", $a->slug);
								$type = $a->question_type;
								$code = $a->code;
								$newVals = [];
								$newValsWP = [];
								foreach ($vals as $j => $val) {
                                    $valWp = $val;
									if (is_string($val)) {
										if ($stripTags) {
											$val = $Utils->stripTags($val);
                                            $valWp = $Utils->stripTags($valWp);
										}
										$val = $Utils->prepareString($val);
										if ($limite > 0) {
											$val =  $Utils->truncate($val, $limite);
                                            $valWp = $Utils->truncate($valWp, $limite);
										}
									}

									// Treat depending on type and commands:
									if ($slugify) {
										if ($slugs[$j]) {
											$newVals[] = $slugs[$j];
											// $val = $slugs[$j];
										}
                                        // WordPress ignores command "slugify":
										$newValsWP[] = $valWp;
									} else if ($tabelaDeApoio) {
										if (!is_string($val)) {
											$val = json_encode($val);
										}
										$idApoio = $APILegacy->tabelaApoioResgataIdPorNome($tabelaDeApoio, $val);
										if ($idApoio != '') {
											$newVals[] = $idApoio;
										}
										$newValsWP[] = $valWp;
										// print_r([$e, $sql, $res]);
									} else if ($type == 'location') {
										if (!is_string($val)) {
											$val = json_encode($val);
										}
										$latLng = explode(' ',$val);
										if (count($latLng) == 2) {
											$lat = floatval($latLng[0]);
											$lng = floatval($latLng[1]);
											// A longitude 150 é quando a pessoa não escolhe nada. Problema do colheita. TODO: dar valor NULO para lat/lng quando a pessoa não escolhe nada!
											if ($lng < 150) {
												if ($fonte->config->updateLegacy) {
													$APILegacy->submission[$APILegacy->cmpo->localizacao_lat] = $lat;
													$APILegacy->submission[$APILegacy->cmpo->localizacao_lng] = $lng;
													$foundLocations = true;
												}

												if ($fonte->config->updateWordPress) {
                                                    $APIWordPress->setLatLng((float) $lat, (float) $lng);
													$foundLocationsWP = true;
												}
											}
										}
									} else if ($associaCode) {
										if ($val) {
											$t = (object) [
                                                "question" => $Utils->prepareString($APIColheita->questions[$a->code]),
                                                "value" => (is_string($a->value))
                                                    ? $Utils->prepareString($a->value)
                                                    : $a->value
                                            ];
                                            if ($isLink) {
                                                $t->is_link = true;
                                            }
											$newVals[] = json_encode($t);

											$t = (object) [
                                                "question" => $APIColheita->questions[$a->code],
                                                "value" => $a->value
                                            ];
                                            if ($isLink) {
                                                $t->is_link = true;
                                            }
                                            $newValsWP[] = $t;
										}
									} else if ($associaValue != '') {
										if ($val) {
											if (is_object($val)) {
												$t = $val;
											} else {
												$t = (object) [
													"value" => $val
												];
											}
											$constValue = explode(':',$associaValue);
											if ($constValue[1]) {
												$t->question = $constValue[1];
											} else {
												$APIColheita->addCampoAIgnorar($associaValue);
												foreach ($answers as $aa) {
													if ($aa->code == $associaValue) {
														$t->question = $aa->value;
														break;
													}
												}
											}
                                            if ($isLink) {
                                                $t->is_link = true;
                                            }
											if ($t->question || is_object($val)) {
												$newVals[] = json_encode($t);
												$newValsWP[] = $t;
											}
										} else {
                                            $APIColheita->addCampoAIgnorar($associaValue);
                                        }
                                        // print_r([$t, $val, $associaValue, $APIColheita->camposAIgnorar, $APIWordPress->submission[$associaValue]]);
									} else {
										if ($val) {
											$newVals[] = (is_object($val))
												? json_encode($val)
												: $Utils->prepareString($val);

                                            if ($choiceNames[$j]) {
                                                $newValsWP[] = $choiceNames[$j];
                                            } else {
                                                if ($isLink) {
                                                    if (is_object($valWp)) {
                                                        $valWp->is_link = true;
                                                    } else {
                                                        $valWp = (object) [
                                                            "value" => $valWp,
                                                            "is_link" => true
                                                        ];
                                                    }
                                                }
                                                $newValsWP[] = $valWp;
                                            }
										}
									}
								}
								if ($pra && $type != 'location') {
									if ($fonte->config->updateLegacy) {
										$APILegacy->submission[$pra] .= ($APILegacy->submission[$pra] && count($newVals))
											? '|'
											: '';
										$APILegacy->submission[$pra] .= implode("|", $newVals);
									}

									if ($fonte->config->updateWordPress) {
										// print_r(['pra' => $pra, 'newVals' => $newValsWP]);
										$APIWordPress->addTargetValues($newValsWP, $relForm);
									}
								}
							}
						}
					}
				}
			}
		} // fim do loop sobre o ETL
		if (!$isSubmissionAllowed) {
			continue;
		}

		// Convert georrefs to codes:
		if ($fonte->config->updateLegacy) {
			foreach ($APILegacy->submission as $pra=>$r) {
				if ($r && $georrefs[$pra]) {
					// print_r([$pra, $r]);exit;
					$vals = explode('|', $r);
					if ($georrefs[$pra] == 'estado') {
						//Estados:
						$APILegacy->convertStateToId($pra);
					} else if ($georrefs[$pra] == 'pais') {
						$APILegacy->convertCountryToId($pra);
					} else {
						// Municípios:
						$foundLocations = $APILegacy->convertCityToId($pra, $foundLocations);
					}
				}
			}

			if (!$foundLocations) {
				$APILegacy->submission[$APILegacy->cmpo->localizacao_lat] = 0;
				$APILegacy->submission[$APILegacy->cmpo->localizacao_lng] = 0;
			}
		}

		// Convert georrefs to codes:
		if ($fonte->config->updateWordPress) {
			foreach ($APIWordPress->submission as $pra=>$r) {
				if ($r && $APIWordPress->georrefs[$pra]) {
					if ($APIWordPress->georrefs[$pra] == 'estado') {
						//Estados:
						// $APIWordPress->convertStateToId();
					} else if ($APIWordPress->georrefs[$pra] == 'pais') {
						// $APIWordPress->convertCountryToId();
					} else {
						// Municípios:
						$APIWordPress->setTargetField($pra, "");
						try {
							$foundLocationsWP = $APIWordPress->prepareCity($foundLocationsWP);
						} catch (Exception $e) {
							$Utils->pE("Falha ao tentar identificar a cidade! Erro: " . $e->getMessage());
						}
					}
				}
			}

			if (!$foundLocationsWP) {
                $APIWordPress->setLatLng(0.0, 0.0);
			}
		}


		// Remove duplicates:
		if ($fonte->config->updateLegacy) {
			$APILegacy->removeDuplicateValuesInRow();
		}

		if ($fonte->config->updateWordPress) {
			// TODO: Should I do something here?
		}

		if ($fonte->config->updateLegacy) {
			// Add everything:
			// $dadosEmBlocos = null;
			if (is_array($APIColheita->camposAIgnorar)) {
				$dadosEmBlocos = $APIColheita->formataTodosOsDadosEmBlocos();
				$APILegacy->submission[$APILegacy->cmpo->tudo] = $Utils->prepareString(json_encode($dadosEmBlocos));
				// pR($APILegacy->submission[$APILegacy->cmpo->tudo]);exit;
			} else {
				$APILegacy->submission[$APILegacy->cmpo->tudo] = json_encode(new stdClass());
			}
		}

		if ($fonte->config->updateWordPress) {
			$APIWordPress->setTargetField($APILegacy->cmpo->tudo, "");
			if (is_array($APIColheita->camposAIgnorar)) {
				$dadosEmBlocos = $APIColheita->formataTodosOsDadosEmBlocos(false);
				$APIWordPress->setTargetValue($dadosEmBlocos);
			} else {
				$APIWordPress->setTargetValue("");
			}
			// TODO: Should I do something here?
		}

		if ($fonte->config->updateLegacy) {
			$updateOnDuplicate = true;
			// Check if organization name exists with different ID! If so, merge both:
			if ($APILegacy->frm == 'frm_instituicao') {
				$instituicaoRepetida = $APILegacy->resgataSubmissionRepetidaPorNome();
				if ($instituicaoRepetida) {
					echo chr(10) . "Nome repetido '{$APILegacy->submission[$APILegacy->campoNome]}'! " . chr(10) ."Estou escolhendo o mais recente." . chr(10);
					$updateOnDuplicate = (
						!$instituicaoRepetida->{$APILegacy->campoAtualizacao} ||
						(strtotime($APILegacy->submission[$APILegacy->campoAtualizacao]) > (strtotime($instituicaoRepetida->{$APILegacy->campoAtualizacao}) + $Utils->umDia))
					);
					$mergedIntoId = $instituicaoRepetida->{$APILegacy->campoId};
					// $sql = "UPDATE colheita_submissions SET merged_into='{$mergedIntoId}' WHERE id='{$APILegacy->submission[$APILegacy->campoId]}'";
					// $Utils->query($sql); // Não existe mais o controle colheita_submissions
					$APILegacy->submission[$APILegacy->campoId] = $mergedIntoId;
				}
				// /*DEBUG*/ print_r($APILegacy->submission);
			}

			if ($updateOnDuplicate) {
				// $Utils->pT($APILegacy->submission);
				$status = $APILegacy->createOrUpdateSubmission($fonte->config->flow == 'forceUpdate');
				$erro = false;
				switch ($status) {
					case 'created':
						echo "LEGACY: Criando novo... ";
						$legacyCounter->nAddedSubmissions ++;
						break;
					case 'updated':
						echo "LEGACY: Já existe. Atualizando... ";
						$legacyCounter->nUpdatedSubmissions ++;
						break;
					case 'noUpdated':
						echo "LEGACY: Já existe e não precisa atualizações.";
						break;
					default:
						echo $status;
						$erro = true;
						$legacyCounter->nErrors ++;
						break;
				}
				if (!$erro) {
					$Utils->pT(" Feito!");
				}
			} else {
				$Utils->pT(" Feito!");
			}
		}

		if ($fonte->config->updateWordPress) {
			// $instituicaoRepetida = $APIWordPress->resgataSubmissionRepetidaPorNome();
			// // TODO!!!
			// if ($instituicaoRepetida) {
			// 	echo chr(10) . "Nome repetido '{$APIWordPress->submission[$APILegacy->campoNome]}'! " . chr(10) . "Estou escolhendo o mais recente." . chr(10);
			// 	$updateOnDuplicate = (
			// 		!$instituicaoRepetida->{$APILegacy->campoAtualizacao} ||
			// 		(strtotime($APIWordPress->submission[$APILegacy->campoAtualizacao]) > (strtotime($instituicaoRepetida->{$APILegacy->campoAtualizacao}) + $Utils->umDia))
			// 	);
			// 	$mergedIntoId = $instituicaoRepetida->{$APILegacy->campoId};
			// 	$APIWordPress->submission[$APILegacy->campoId] = $mergedIntoId;
			// }

			$status = $APIWordPress->createOrUpdateSubmission(
                $APIColheita->dadosEspecificos(),
                $fonte->config->flow == 'forceUpdate'
            );

			$erro = false;
			switch ($status) {
				case 'created':
					echo "WORDPRESS: Criando novo... ";
					$wordPressCounter->nAddedSubmissions ++;
					break;
				case 'updated':
					echo "WORDPRESS: Já existe. Atualizando... ";
					$wordPressCounter->nUpdatedSubmissions ++;
					break;
				case 'notUpdated':
					echo "WORDPRESS: Já existe e não precisa atualizações.";
					break;
				default:
					$erro = true;
					$wordPressCounter->nErrors ++;
					break;
			}
			if (!$erro) {
				$Utils->pT(" Feito!");
			}
            // print_r($APIWordPress->submission);exit;
		}

	} // fim do loop por submission do form

	// Agora retiro itens que porventura não estejam mais no colheita:
	if ($fonte->config->updateLegacy) {
		$affectedSubmissions = $APILegacy->removeOrphanedSubmissions(array_keys($APIColheita->submissions));
		$legacyCounter->nRemovedSubmissions += $affectedSubmissions;
	}
	if ($fonte->config->updateWordPress) {
		$affectedSubmissions = $APIWordPress->removeOrphanedSubmissions(array_keys($APIColheita->submissions));
		$wordPressCounter->nRemovedSubmissions += $affectedSubmissions;
	}

	// RESUMO DO PROCESSO:
	if ($fonte->config->updateLegacy) {
		$Utils->pT("RESULTADOS NO APILegacy (tabela {$APILegacy->frm}):", "h2");
		$Utils->pT("{$legacyCounter->nAddedSubmissions} submissões ADICIONADAS");
		$Utils->pT("{$legacyCounter->nUpdatedSubmissions} submissões ATUALIZADAS");
		$Utils->pT("{$legacyCounter->nRemovedSubmissions} submissões REMOVIDAS");
		$Utils->pT("{$legacyCounter->nErrors} ERROS");
		$Utils->pT("{$legacyCounter->nDownloadedFiles} arquivos BAIXADOS");
	}
	if ($fonte->config->updateWordPress) {
		$Utils->pT("RESULTADOS NO APIWordPress (Tipo de cadastro: {$fonte->tipo}):", "h2");
		$Utils->pT("{$wordPressCounter->nAddedSubmissions} submissões ADICIONADAS");
		$Utils->pT("{$wordPressCounter->nUpdatedSubmissions} submissões ATUALIZADAS");
		$Utils->pT("{$wordPressCounter->nRemovedSubmissions} submissões REMOVIDAS");
		$Utils->pT("{$wordPressCounter->nErrors} ERROS");
		$Utils->pT("{$wordPressCounter->nDownloadedFiles} arquivos BAIXADOS ao servidor local");
	}

} // fim do loop por form_id

$Utils->pT("FIM","h1");

?>
