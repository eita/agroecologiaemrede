<?php
declare(strict_types=1);

namespace EITA;
use \Exception;

/**
 * Manage and update Legacy API Database
 */
class APILegacy
{
    // Public objects
    public $cmpo;

    // Public vars
    public $frm = "";
    public $tipoFrm = "";
    public $submission = [];
    public $submissionId = "";
    public $campoNome = "";
    public $campoDescricao = "";
    public $campoId = "";
    public $campoCriacao = "";
    public $campoAtualizacao = "";
    public $campoLocalizacao = "";
    public $campoMapeamento = "";

    // Private objects
    private $Utils;

    // Private vars
    private $campo = [];
    private $mapeamento = "";

    /**
     * Os parâmetros são um array:
     * $params['campo']
     * $params['Utils']
     * $params['tipoFrm']
     * $params['mapeamento']
     * @method __construct
     * @param  array      $params descrição acima
     */
    public function __construct(array $params)
    {
        $this->campo = $params['campo'];
        $this->Utils = $params['Utils'];
    }

    public function setForm($params)
    {
        $this->colheitaFormId = $params['colheitaFormId'];
        $this->tipoFrm = $params['tipoFrm'];
        $this->mapeamento = $params['mapeamento'];

        $this->frm = ($this->tipoFrm != 'experiencia')
            ? "frm_{$this->tipoFrm}"
            : "frm_exp_base_comum";
        $this->cmpo = $this->campo[$this->frm];

        $this->campoNome = $this->cmpo->nome;
        $this->campoDescricao = $this->cmpo->descricao;
        $this->campoId = $this->cmpo->id;
        $this->campoAtualizacao = $this->cmpo->atualizacao;
        $this->campoLocalizacao = $this->cmpo->localizacao;
        $this->campoCriacao = $this->cmpo->criacao;
        $this->campoMapeamento = $this->cmpo->mapeamento;
    }

    /**
     * Sets the active submission being worked on
     * @method setSubmission
     * @param  string        $submissionId
     * @param  object        $submission
     */
    public function setSubmission(string $submissionId, $submissionCreatedAt, $submissionUpdatedAt)
    {
        $this->submission = [
            $this->campoId => $submissionId,
            $this->campoMapeamento => $this->mapeamento,
            "dt_criacao" => $submissionCreatedAt,
            "dt_atualizacao" => $submissionUpdatedAt,
        ];
        $this->submissionId = $submissionId;
    }

    /**
     * Remove current submission from database
     * @method removeSubmission
     * @return bool          True if succesfully removed
     */
    public function removeSubmission()
    {
        $sql = "SELECT {$this->campoId} FROM {$this->frm} WHERE {$this->campoId} = '{$this->submissionId}'";
        $res = $this->query($sql, 'object');
        if (count($res) > 0) {
            $sql = "
                DELETE
                    FROM {$this->frm}
                    WHERE {$this->campoId} = '{$this->submissionId}'
            ";
            $this->query($sql);
            if ($this->frm == 'frm_exp_base_comum') {
                $sql = "
                    DELETE
                        FROM `frm_exp_geral`
                        WHERE `{$this->campo['frm_exp_geral']->id}` = '{$this->submissionId}'
                ";
                $this->query($sql);
            }
        }
        return (count($res) == 1);
    }

    /**
     * Removes submissions that don't exist anymore in Colheita
     * @method removeOrphanedSubmissions
     * @param  array                     $submissionIds List of submission IDs that should NOT be removed
     * @return int                                      Number of submissions removed
     */
    public function removeOrphanedSubmissions(array $submissionIds): int
    {
        $submissionIdsTxt = implode("', '", $submissionIds);
        $sql = "DELETE FROM {$this->frm} WHERE `{$this->campoMapeamento}` = '{$this->mapeamento}' AND `{$this->campoId}` NOT IN ('{$submissionIdsTxt}')";
        $affectedRows = $this->query($sql, 'affected_rows');

        if ($this->frm == 'frm_exp_base_comum') {
            $sql = "DELETE FROM `frm_exp_geral` WHERE `{$this->campoMapeamento}` = '{$this->mapeamento}' AND `{$this->campo['frm_exp_geral']->id}` NOT IN ('{$submissionIdsTxt}')";
            $this->query($sql);
        }

        return $affectedRows;
    }

    /**
     * Check if another submission in database has the same name as the active Colheita's one. If yes, return it
     * @method resgataSubmissionRepetidaPorNome
     * @return \stdClass                           Submission which has the same name as the active one
     */
    public function resgataSubmissionRepetidaPorNome(): \stdClass
    {
        $nome = $this->submission[$this->campoNome];
        $id = $this->submission[$this->campoId];
        $sql = "
            SELECT *
            FROM {$this->frm}
            WHERE
                `{$this->campoNome}` like '{$nome}' AND
                `{$this->campoId}` != '{$id}'
        ";
        $ret = $this->query($sql, 'object');
        return (count($ret) > 0) ? $ret[0] : (object) [];
    }

    /**
     * Checks if database has submission with same id as the Colheita's Active Submission
     * @method resgataSubmissionRepetidaPorId
     * @return \stdClass                         The submission in database, if exists
     */
    public function resgataSubmissionRepetidaPorId(): \stdClass
    {
        $id = $this->submission[$this->campoId];
        $sql = "
            SELECT *
            FROM {$this->frm}
            WHERE `{$this->campoId}` = '{$id}'
        ";
        $ret = $this->query($sql, 'object');
        return (count($ret) > 0) ? $ret[0] : (object) [];
    }

    /**
     * Recursively finds a submission with non repeated name
     * @method renomeiaSubmissionRepetidaPorNome
     * @param  int                            $n Suffix
     * @return bool                              True when finished recursion
     */
    private function renomeiaSubmissionRepetidaPorNome($n = 1) : bool
    {
        $submissionRepetidaPorNome = $this->resgataSubmissionRepetidaPorNome();
        if ((array)$submissionRepetidaPorNome) {
            $lastN = $n - 1;
            $newName = ($n == 1)
                ? "{$this->submission[$this->campoNome]} (1)"
                : str_replace(" ({$lastN})", " ({$n})", $this->submission[$this->campoNome]);
            $this->submission[$this->campoNome] = $newName;
            $this->Utils->pT("Repetido: mudando o nome para {$newName}");
            $submissionRepetidaPorNomeDeNovo = $this->resgataSubmissionRepetidaPorNome();
            if ($submissionRepetidaPorNomeDeNovo) {
                return $this->renomeiaSubmissionRepetidaPorNome($n + 1);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * Creates or updates new submission in database from Colheita's Active Submission
     * @method createOrUpdateSubmission
     * @param  bool                     $forceUpdate If should update anyway
     * @return string                                Status: 'updated', 'created', 'noUpdated'
     */
    public function createOrUpdateSubmission($forceUpdate) : string
    {
        if (!$this->submission[$this->campoId]) {
            throw new Exception('Cannot update submission if it\'s not set!');
        }
        $id = $this->submission[$this->campoId];
        $submissionRepetidaPorId = $this->resgataSubmissionRepetidaPorId();
        $status = '';
        if (property_exists($submissionRepetidaPorId, $this->campoAtualizacao)) {
            // Exists! Check if existing submission is older than this actual data
            $existingDataLastUpdatedAt = strtotime($submissionRepetidaPorId->{$this->campoAtualizacao}) + $this->Utils->umDia;
            $colheitaDataLastUpdatedAt = strtotime($this->submission[$this->campoAtualizacao]);
            $hasChanges = ($existingDataLastUpdatedAt < $colheitaDataLastUpdatedAt);

            if ($hasChanges || $forceUpdate) {
                $updates = [];
                foreach ($this->submission as $k => $v) {
                    if ($k != $this->campoId) {
                        $updates[] = "`{$k}`='{$v}'";
                    }
                }
                $updates = implode(", ", $updates);
                $sql = "
                    UPDATE {$this->frm}
                    SET {$updates}
                    WHERE `{$this->campoId}` = '{$id}'
                ";
                //DEBUG echo chr(10).$sql;exit;
                $status = 'updated';
            } else {
                $sql = null;
                $status = 'noUpdated';
            }
        } else {
            // Doesn't exist!
            $this->renomeiaSubmissionRepetidaPorNome();
            $fields = implode("`, `", array_keys($this->submission));
            $values = implode("', '", array_values($this->submission));
            $sql = "INSERT INTO {$this->frm} (`{$fields}`) VALUES ('{$values}')";
            $status = 'created';
        }
        // $this->Utils->pT('------------','h2');$this->Utils->pT($this->submission);$this->Utils->pT($sql);exit;
        if ($sql) {
            try {
                $this->query($sql);
                if ($this->frm == 'frm_exp_base_comum') {
                    $sql = "REPLACE INTO `frm_exp_geral` (`{$this->campo['frm_exp_geral']->id}`) VALUES ('{$this->submission[$this->campoId]}')";
                    $this->query($sql);
                }
            } catch (Exception $e) {
                $status = $e->getMessage();
            }
        }
        return $status;
    }

    // TABELAS DE APOIO:


    /**
     * Gets Id from Taxonomy Table if item name exists
     * @method tabelaApoioResgataIdPorNome
     * @param  string                      $tabelaApoio Name of the Taxonomy Table
     * @param  string                      $textoBusca  Name of the item to be checked
     * @param  string                      $campoId     Name of the ID field of the Taxonomy Table
     * @param  string                      $campoValor  Name of the VALUE field of the Taxonomy Table
     * @return string                                   Id of the item, if it exists
     */
    public function tabelaApoioResgataIdPorNome(
        string $tabelaApoio,
        string $textoBusca,
        string $campoId = 'id',
        string $campoValor = 'nome'
    ) : string {
        $sql = "SELECT {$campoId} FROM {$tabelaApoio} WHERE `{$campoValor}` like '{$textoBusca}'";
        $ret = $this->query($sql, 'object');
        return (count($ret) > 0) ? $ret[0]->{$campoId} : '';
    }

    /**
     * Insert items to Taxonomy Table
     * @method tabelaApoioInsereValores
     * @param  string                   $tabelaApoio     Name of the Taxonomy Table
     * @param  array                    $campos          Fields where values will be inserted
     * @param  array                    $valores         Values to be inserted
     * @param  bool                     $noAutoincrement If the id is autoincremented or should be created ad hoc
     * @param  string                   $idRef           Reference Id for creating id
     * @return string                                    The id of the inserted item
     */
    public function tabelaApoioInsereValores(
        string $tabelaApoio,
        array $campos,
        array $valores,
        bool $noAutoincrement,
        string $idRef
    ) : string {
        $apoioCamposSubmission = implode("`, `", $campos);
        $apoioValoresSubmission = implode("', '", $valores);
        $idCriado = null;
        if ($noAutoincrement) {
            $idCriado = $pra . '-' . substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'), 1, 6) . '-' . strval(round(microtime(true)*1000));
            $apoioCamposSubmission = "{$idRef}`, `{$apoioCamposSubmission}";
            $apoioValoresSubmission = "{$idCriado}', '{$apoioValoresSubmission}";
        }
        $sql = "INSERT INTO $tabelaApoio (`$apoioCamposSubmission`) VALUES ('$apoioValoresSubmission')";
        // echo chr(10).chr(10).$sql.chr(10);
        $idAuto = $this->query($sql, 'id');
        return  ($idCriado) ? $idCriado : $idAuto;
    }

    /**
     * Gets state name and convert it to legacy's DB code
     * @param  string $campo The field in submission which is a state
     */
    public function convertStateToId(string $campo)
    {
        if (!$this->colheitaFormId || !$this->submissionId || !$this->submission[$campo]) {
            return;
        }
        $vals = explode('|', $this->submission[$campo]);
        $newVals = [];
        foreach ($vals as $val) {
            $estadoId = $this->Utils->resgataEstadoId($val);
            if ($estadoId) {
                $newVals[] = $estadoId;
            }
        }
        $this->submission[$campo] = implode('|', $newVals);
    }

    /**
     * Gets country name and convert it to legacy's DB code
     * @param  string $campo The field in submission which is a country
     */
    public function convertCountryToId(string $campo)
    {
        if (!$this->colheitaFormId || !$this->submissionId || !$this->submission[$campo]) {
            return;
        }
        $vals = explode('|', $this->submission[$campo]);
        $newVals = [];
        foreach ($vals as $val) {
            $paisId = $this->Utils->resgataPaisId($val);
            if ($paisId) {
                $newVals[] = $paisId;
            }
        }
        $this->submission[$campo] = implode('|', $newVals);
    }

    /**
     * Convert city to Legacy's database ID
     * @param  string $campo          The field of the submission which is one or more cities
     * @param  bool   $foundLocations If should update localizacao_lat and localizacao_lng
     * @return bool                   If a location was found
     */
    public function convertCityToId(string $campo, bool $foundLocations)
    {
        if (!$this->colheitaFormId || !$this->submissionId || !$this->submission[$campo]) {
            throw new Exception('Failed to convert!');
        }
        $vals = explode('|', $this->submission[$campo]);
        $newVals = [];
        $c = count($vals);
        for ($i = 0; $i < $c; $i+=2) {
            $municipio = $this->Utils->resgataDadosMunicipio($vals[$i+1], $vals[$i]);
            if ($municipio) {
                $newVals[] = $municipio->id;
                if ($campo == $this->campoLocalizacao && $municipio->lat && !$foundLocations) {
                    $foundLocations = true;
                    $this->submission[$this->cmpo->localizacao_lat] = $municipio->lat;
                    $this->submission[$this->cmpo->localizacao_lng] = $municipio->lng;
                }
            }
        }
        $this->submission[$campo] = implode('|', $newVals);
        return $foundLocations;
    }

    /**
     * Removes duplicate values in a multivalues' row in this submission
     */
    public function removeDuplicateValuesInRow()
    {
        if (!$this->colheitaFormId || !$this->submissionId) {
            return;
        }

        foreach ($this->submission as $pra => $r) {
            $vals = explode("|", (string) $r);
            $singleVals = [];
            foreach ($vals as $val) {
                if (!in_array($val, $singleVals)) {
                    $singleVals[] = $val;
                }
            }
            $this->submission[$pra] = implode("|", $singleVals);
        }
    }

    /**
     * @method query
     * @param  string $sql   The SQL Query
     * @param  string $tipo  (optional) Type of query
     * @param  string $assoc (optional) Field to associate in the result
     * @return string                   Query result in different types
     */
    private function query($sql, $tipo = '', $assoc = '')
    {
        return $this->Utils->query($sql, $tipo, $assoc);
    }
}
