<?php
declare(strict_types=1);

namespace EITA;
use \Exception;
use \DirectoryIterator;

/**
 * Class to manage Colheita forms and submissions database
 */
class APIColheita
{
    private $Utils;
    private $BASE_URL = "https://colheita.agroecologiaemrede.org.br";
    private $API_URL = "/api/v1/m/";
    private $PER_PAGE = 50;
    private $API_TOKEN = "";
    private $AER_LOGIN = "";
    private $AER_SENHA = "";
    private $PG = "";
    private $extraHeaders = [];

    private $submission;

    public $uploadPath = "";
    public $submissions = [];
    public $questions = [];
    public $formId = "";
    public $formName = "";
    public $camposAIgnorar = [];
    public $FONTES = [];
    public $blocos = [];

    public $taxonomias = [
        "areas_tematicas" => [
            "optionSetId" => "6d1b47e0-aba9-47e5-84de-70ad4b3b8411",
            "APIWordPress" => [
                "name" => "Temas",
                "singular_name" => "Tema",
                "slug" => "areas_tematicas",
            ]
        ],
        "abrangencia" => [
            "optionSetId" => "98b56b20-bc62-4644-89ac-9eb45d4db601",
            "APIWordPress" => [
                "name" => "Abrangências",
                "singular_name" => "Abrangência",
                "slug" => "abrangencia",
            ]
        ],
        "mapeamento" => [
            "APIWordPress" => [
                "name" => "Mapeamentos",
                "singular_name" => "Mapeamento",
                "slug" => "mapeamento"
            ]
        ],
        "uf" => [
            "APIWordPress" => [
                "name" => "Estados",
                "singular_name" => "Estado",
                "slug" => "uf",
            ]
        ],
        "municipio" => [
            "APIWordPress" => [
                "name" => "Municípios",
                "singular_name" => "Município",
                "slug" => "municipio",
            ]
        ],
        "uf_abrangencia" => [
            "APIWordPress" => [
                "name" => "Estados de abrangência",
                "singular_name" => "Estado de abrangência",
                "slug" => "uf_abrangencia",
            ]
        ],
        "municipio_abrangencia" => [
            "APIWordPress" => [
                "name" => "Municípios de abrangência",
                "singular_name" => "Município de abrangência",
                "slug" => "municipio_abrangencia",
            ]
        ],
    ];

    public function __construct($params)
    {
        $this->Utils = $params['Utils'];
        $this->uploadPath = $params['uploadPath'];

        $this->API_TOKEN = $params['configColheita']->API_TOKEN;
        $this->AER_LOGIN = $params['configColheita']->AER_LOGIN;
        $this->AER_SENHA = $params['configColheita']->AER_SENHA;
        $this->PG = $params['configColheita']->PG;
        $this->extraHeaders = array("Authorization: Token token=".$this->API_TOKEN);

        $waitForDepedency = null;
        /**
         * Loop over colheita_etl directory to populate all ETL settings for each form in each mapeamento
         */
        foreach (new DirectoryIterator($params['dirColheitaETL']) as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }
            $data = file_get_contents($params['dirColheitaETL'] . $fileInfo->getFilename());
            $fonte = json_decode($data);
            if (!$fonte) {
                throw new Exception("JSON INVÁLIDO! Arquivo: {$fileInfo->getFilename()}");
            }
            $fonte->arquivo = $fileInfo->getFilename();

            if (property_exists($fonte, "dependsOnFormId") && !array_key_exists($fonte->dependsOnFormId, $this->FONTES)) {
                $waitForDepedency = $fonte;
            } else {
                $this->FONTES[$fonte->formId] = $fonte;
            }

            if ($waitForDepedency && $waitForDepedency->dependsOnFormId == $fonte->formId) {
                $this->FONTES[$waitForDepedency->formId] = $waitForDepedency;
                $waitForDepedency = null;
            }
            // $this->FONTES[$fonte->formId] = $fonte;
        }
    }

    /**
     * Set current Colheita Form being handled, if allowed.
     * Loads all submissions and populates camposAIgnorar
     * @method setForm
     * @param  string  $formId The form id
     */
    public function setForm(string $formId)
    {
        if (!$this->isAccessAllowed($formId)) {
            throw new Exception("The configured user is not allowed to access form '{$formId}' from mission '{$this->FONTES[$formId]->missao}' in Colheita!");
        }

        $this->formId = $formId;
        $this->getFormNome();
        $this->resgataBlocos();
        $this->extractSubmissions();
        $this->populateCamposAIgnorar();
    }

    /**
     * Check if user has access to this form in Colheita
     * @method isAccessAllowed
     * @param  string          $formId The form id in Colheita
     * @return boolean                 True if access is allowed
     */
    private function isAccessAllowed(string $formId)
    {
        $missao = $this->FONTES[$formId]->missao;
        $url = "{$this->BASE_URL}{$this->API_URL}{$missao}/responses?per_page=1&form_id={$formId}";
        list($dados, $headers) = $this->Utils->pega_xml_arquivo($url, 'json', $this->extraHeaders);
        return !is_array($dados->errors);
    }

    /**
     * Sets current Colheita Submission being handled
     * @method setSubmission
     * @param  object        $submission the submission
     */
    public function setSubmission(object $submission)
    {
        $this->submission = $submission;
    }

    /**
     * Checks for fields to be ignored in ETL and populates camposAIgnorar
     * @method populateCamposAIgnorar
     */
    private function populateCamposAIgnorar()
    {
        $fonte = $this->FONTES[$this->formId];
        $camposAIgnorar = $fonte->camposAIgnorar;
        foreach ($fonte->ETL as $es) {
            if (property_exists($es, 'de')) {
                $parts = explode(':', $es->de);
                if (count($parts) == 1 && is_array($camposAIgnorar) && !in_array($es->de, $camposAIgnorar)) {
                    $camposAIgnorar[] = $es->de;
                }
            } elseif (property_exists($es, 'cria_apoio') && property_exists($es->cria_apoio, 'ETL')) {
                foreach ($es->cria_apoio->ETL as $ees) {
                    $parts = explode(':', $ees->de);
                    if (count($parts) == 1 && is_array($camposAIgnorar) && !in_array($ees->de, $camposAIgnorar)) {
                        $camposAIgnorar[] = $ees->de;
                    }
                }
            }
        }
        $this->camposAIgnorar = $camposAIgnorar;
    }

    /**
     * Adds field to camposAIgnorar
     * @method addCampoAIgnorar
     * @param  string           $campo The field to be added
     */
    public function addCampoAIgnorar(string $campo)
    {
        if (is_array($this->camposAIgnorar)) {
            $this->camposAIgnorar[] = $campo;
        }
    }

    private function getFormNome()
    {
        $sql = "
            SELECT name FROM forms WHERE id = '{$this->formId}'
        ";
        $result = pg_query($this->PG, $sql);
        $dados = pg_fetch_all($result);
        $this->formNome = $dados[0]["name"];
    }

    /**
     * Query Colheita for all submissions from current formId
     * @method extractSubmissions
     * @return null
     */
    private function extractSubmissions()
    {
        $pegaEstrutura = false;
        if ($pegaEstrutura) {
            $extraSQLCampos = "
			parent_answers.parent_id as grandparent_id,
			parent_questionings.group_name_translations as parent_name,
			parent_questionings.ancestry as parent_ancestry,
			grandparent_questionings.group_name_translations as grandparent_name,
			grandparent_questionings.ancestry as grandparent_ancestry,
			";
            $extraSQLJoins = "
			INNER JOIN form_items parent_questionings
			ON parent_answers.questioning_id = parent_questionings.id
			INNER JOIN answers grandparent_answers
			ON parent_answers.parent_id = grandparent_answers.id
			INNER JOIN form_items grandparent_questionings
			ON grandparent_answers.questioning_id = grandparent_questionings.id
			";
        } else {
            $extraSQLJoins = $extraSQLCampos = '';
        }

        $sql = "
			SELECT
				responses.id AS submission_id,
				responses.created_at,
				responses.updated_at,
				responses.reviewed AS is_reviewed,
				questions.code AS code,
				questions.name_translations AS question,
				questions.qtype_name AS question_type,
				users.name AS submitter_name,
				answers.id AS id,
				answers.new_rank AS ord,
				answers.value AS value,
				answers.datetime_value AS datetime_value,
				answers.parent_id as parent_id,
				questionings.ancestry as ancestry,
				questionings.ancestry_depth as ancestry_depth,
				{$extraSQLCampos}
				media_objects.id as item_id,
				media_objects.item_content_type,
				media_objects.item_file_name,
				media_objects.item_file_size,
				media_objects.type as item_type,
				COALESCE(ao.name_translations, co.name_translations) AS choice_name,
				COALESCE(ao.value, co.value) AS choice_value,
				option_sets.name AS option_set,
				option_sets.form_id AS option_set_related_form
			FROM
				responses
			LEFT JOIN users users
				ON responses.user_id = users.id
			INNER JOIN forms forms
				ON responses.form_id = forms.id
			LEFT JOIN answers answers
				ON answers.response_id = responses.id AND answers.type = 'Answer'
			INNER JOIN form_items questionings
				ON answers.questioning_id = questionings.id
			INNER JOIN questions questions
				ON questionings.question_id = questions.id
			INNER JOIN answers parent_answers
				ON answers.parent_id = parent_answers.id
			{$extraSQLJoins}
			LEFT JOIN option_sets option_sets
				ON questions.option_set_id = option_sets.id
			LEFT JOIN options ao
				ON answers.option_id = ao.id
			LEFT JOIN option_nodes ans_opt_nodes
				ON ans_opt_nodes.option_id = ao.id AND ans_opt_nodes.option_set_id = option_sets.id
			LEFT JOIN choices choices
				ON choices.answer_id = answers.id
			LEFT JOIN options co
				ON choices.option_id = co.id
			LEFT JOIN option_nodes ch_opt_nodes
				ON ch_opt_nodes.option_id = co.id AND ch_opt_nodes.option_set_id = option_sets.id
			LEFT JOIN media_objects media_objects
				ON media_objects.answer_id = answers.id
			WHERE
				forms.id = '{$this->formId}'
			ORDER BY
				responses.created_at DESC, answers.parent_id, ord
		";
        $result = pg_query($this->PG, $sql);
        $dados = pg_fetch_all($result);

        $submissions = [];
        $questions = [];
        foreach ($dados as $dd) {
            $submission_id = $dd['submission_id'];
            $code = $dd['code'];
            $question = $this->Utils->colheitaTraduz($dd['question']);
            $parent_id = $dd['parent_id'];
            $parent_question = $dd['parent_name'];
            $grandparent_id = $dd['grandparent_id'];
            $grandparent_question = $dd['grandparent_name'];
            $ord = intval($dd['ord']);
            if (!isset($questions[$code])) {
                $questions[$code] = $question;
            }
            if (!isset($submissions[$submission_id])) {
                $submissions[$submission_id] = (object) [
                    "answers" => [],
                    "updated_at" => $dd['updated_at'],
                    "created_at" => $dd['created_at'],
                    "submitter_name" => $dd['submitter_name'],
                    "is_reviewed" => $dd['is_reviewd']
                ];
            }
            if (!isset($submissions[$submission_id]->answers[$parent_id])) {
                $submissions[$submission_id]->answers[$parent_id] = [];
            }

            $isSelect = in_array($dd['question_type'], ['select_one', 'select_multiple']);

            $value = $dd['value'];
            if ($isSelect) {
                $value = $dd['choice_value']
                    ? $dd['choice_value']
                    : $this->Utils->colheitaTraduz($dd['choice_name']);
            } elseif ($dd['datetime_value']) {
                $value = $dd['datetime_value'];
            } elseif ($dd['item_file_name']) {
                $value = (object) [
                    "fileid" => $dd['item_id'],
                    "filesize" => $dd['item_file_size'],
                    "mimetype" => $dd['item_content_type'],
                    "itemtype" => str_replace('::', '/', strtolower($dd['item_type'])).'s'
                ];
            }

            $t = (object) [];
            $t->id = $dd['id'];
            $t->code = $dd['code'];
            $t->ord = $ord;
            $t->value = $value;
            $t->choice_name = $this->Utils->colheitaTraduz($dd['choice_name']);
            $t->slug = ($isSelect)
                ? $this->Utils->slugify($value, '_')
                : '';
            $t->question_type = $dd['question_type'];
            $t->parent_id = $parent_id;
            $t->ancestry = $dd['ancestry'];
            $t->ancestry_depth = $dd['ancestry_depth'];
            if ($pegaEstrutura) {
                $t->parent_question = $parent_question;
                $t->parent_ancestry = $dd['parent_ancestry'];
                $t->grandparent_id = $grandparent_id;
                $t->grandparent_question = $grandparent_question;
                $t->grandparent_ancestry = $dd['grandparent_ancestry'];
            }
            // $t->datetime_value = $dd['datetime_value'];
            $t->option_set = $dd['option_set'];
            // $t->option_set_related_form = $dd['option_set_related_form'];

            if ($value) {
                if ($submissions[$submission_id]->answers[$parent_id][$ord]) {
                    if ($submissions[$submission_id]->answers[$parent_id][$ord]->id != $t->id || $t->question_type=='select_multiple') {
                        $submissions[$submission_id]->answers[$parent_id][$ord]->choice_name .= "|{$t->choice_name}";
                        $submissions[$submission_id]->answers[$parent_id][$ord]->value .= "|{$value}";
                        $submissions[$submission_id]->answers[$parent_id][$ord]->slug .= "|{$t->slug}";
                    }
                } else {
                    $submissions[$submission_id]->answers[$parent_id][$ord] = $t;
                }
            }
        }

        foreach ($submissions as $submission_id => $submission) {
            foreach ($submission->answers as $parent_id => $answers) {
                if (count($answers) == 0) {
                    unset($submissions[$submission_id]->answers[$parent_id]);
                } else {
                    ksort($submissions[$submission_id]->answers[$parent_id]);

                    foreach ($submissions[$submission_id]->answers[$parent_id] as $n => $answer) {
                        if ($answer->value) {
                            $submissions[$submission_id]->answers[$parent_id][$n]->codeValue = (object) [
                                "code" => $answer->code,
                                "value" => $answer->value
                            ];
                        }
                    }
                }
            }
        }

        // Complementa dados das questions com nomes dos grupos traduzidos:
        foreach ($this->blocos as $bloco) {
            if (!array_key_exists($bloco["id"], $questions)) {
                $questions[$bloco["id"]] = $this->Utils->colheitaTraduz($bloco["group_name_translations"]);
            }
        }

        $this->submissions = $submissions;
        $this->questions = $questions;
    }

    private function resgataBlocos()
    {
        $sql = "select id, ancestry, ancestry_depth, group_name_translations from form_items where type='QingGroup' and ancestry_depth > 0 and form_id = '{$this->formId}' order by ancestry_depth";
        $result = pg_query($this->PG, $sql);
        $this->blocos = pg_fetch_all($result);
    }

    public function formataTodosOsDadosEmBlocos($includeLabels = true)
    {
        $s = [];
        foreach ($this->blocos as $b) {
            $id = $b['id'];
            $title = $this->Utils->colheitaTraduz($b['group_name_translations']);
            $sups = explode('/', $b['ancestry']);
            $t = (object) [];
            if ($includeLabels) {
                $t->title = $title;
            }
            $t->subs = [];
            $t->answers = [];
            switch ($b['ancestry_depth']) {
                case 1:
                    if (!$s[$id]) {
                        $s[$id] = $t;
                    }
                    break;
                case 2:
                    if (!$s[$sups[1]]->subs[$id]) {
                        $s[$sups[1]]->subs[$id] = $t;
                    }
                    break;
                case 3:
                    if (!$s[$sups[1]]->subs[$sups[2]]->subs[$id]) {
                        $s[$sups[1]]->subs[$sups[2]]->subs[$id] = $t;
                    }
                    break;
            }
        }


        $dadosEmBlocos = [];

        foreach ($this->submission->answers as $parent_id => $answers) {
            $first = array_values($answers)[0];
            if (!$first || !property_exists($first, "ancestry")) {
                continue;
            }

            $sups = explode('/', $first->ancestry);
            if (!$sups[1]) {
                continue; // TODO: tratar casos em que a pergunta está na raiz.
            }
            $c = count($sups);
            if (!$dadosEmBlocos[$sups[1]]) {
                $dadosEmBlocos[$sups[1]] = $s[$sups[1]];
            }
            if ($sups[2] && !$dadosEmBlocos[$sups[1]]->subs[$sups[2]]) {
                $dadosEmBlocos[$sups[1]]->subs[$sups[2]] = $s[$sups[1]]->subs[$sups[2]];
            }
            if ($sups[3] && !$dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]]) {
                $dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]] = $s[$sups[1]]->subs[$sups[2]]->subs[$sups[3]];
            }

            $smallerAnswers = [];
            foreach ($answers as $a) {
                if (!in_array($a->code, $this->camposAIgnorar)) {
                    if (is_string($a->value)) {
                        $aVals = explode('|', $a->value);
                        $newAVals = [];
                        foreach ($aVals as $aVal) {
                            $decoded = json_decode($aVal);
                            $newAVals[] = ($decoded) ? $decoded : $aVal;
                        }
                        if (count($newAVals) == 1) {
                            $newAVals = $newAVals[0];
                        }
                    } else {
                        $newAVals = $a->value;
                    }
                    if ($includeLabels) {
                        $smallerAnswers[] = (object) [
                            "id" => $a->id,
                            "code" => $a->code,
                            "question" => $this->questions[$a->code],
                            "value" => $newAVals
                        ];
                    } else {
                        $smallerAnswers[] = [$a->code => $newAVals];
                    }
                }
            }

            if (count($smallerAnswers)) {
                if ($c==2) {
                    $dadosEmBlocos[$sups[1]]->answers[$parent_id] = $smallerAnswers;
                } elseif ($c==3) {
                    $dadosEmBlocos[$sups[1]]->subs[$sups[2]]->answers[$parent_id] = $smallerAnswers;
                } elseif ($c > 3) {
                    $dadosEmBlocos[$sups[1]]->subs[$sups[2]]->subs[$sups[3]]->answers[$parent_id] = $smallerAnswers;
                }
            }
        }

        foreach ($dadosEmBlocos as $idUm => $x) {
            $temAnswers = true;
            $temSubs = true;
            if (!$x->answers || !count($x->answers)) {
                unset($dadosEmBlocos[$idUm]->answers);
                $temAnswers = false;
            }
            $temAlgumSSubs = false;
            $temAlgumSAnswers = false;
            if (!$x->subs || !count($x->subs)) {
                unset($dadosEmBlocos[$idUm]->subs);
                $temSubs = false;
            } else {
                $temSSubs = [];
                $temSAnswers = [];
                foreach ($x->subs as $idDois => $xx) {
                    $temSSubs[$idDois] = true;
                    $temSAnswers[$idDois] = true;
                    if (!$xx->answers || !count($xx->answers)) {
                        unset($dadosEmBlocos[$idUm]->subs[$idDois]->answers);
                        $temSAnswers[$idDois] = false;
                    }
                    if (!$xx->subs || !count($xx->subs)) {
                        unset($dadosEmBlocos[$idUm]->subs[$idDois]->subs);
                        $temSSubs[$idDois] = false;
                    }
                    if (!$temSAnswers[$idDois] && !$temSSubs[$idDois]) {
                        unset($dadosEmBlocos[$idUm]->subs[$idDois]);
                    }
                }
                foreach ($temSSubs as $idDois => $temSSub) {
                    $temSAnswer = $temSAnswers[$idDois];
                    if ($temSSub) {
                        $temAlgumSSubs = true;
                    }
                    if ($temSAnswer) {
                        $temAlgumSAnswers = true;
                    }
                }
            }
            if (!$temAnswers && (!$temSubs || !$temAlgumSAnswers)) {
                unset($dadosEmBlocos[$idUm]);
            } elseif (!$temAlgumSSubs && !$temAlgumSAnswers) {
                unset($dadosEmBlocos[$idUm]->subs);
            }
        }

        return $dadosEmBlocos;
    }

    public function dadosEspecificos()
    {
        $ret = [];
        if ($this->camposAIgnorar == null) {
            return $ret;
        }
        foreach ($this->submission->answers as $answers) {
            foreach ($answers as $a) {
                if (!in_array($a->code, $this->camposAIgnorar)) {
                    if (is_string($a->value)) {
                        $aVals = explode('|', $a->value);
                        $newAVals = [];
                        foreach ($aVals as $aVal) {
                            $decoded = json_decode($aVal);
                            $newAVals[] = ($decoded) ? $decoded : $aVal;
                        }
                        if (count($newAVals) == 1) {
                            $newAVals = $newAVals[0];
                        }
                    } else {
                        $newAVals = $a->value;
                    }
                    $ret[$a->code] = $newAVals;
                }
            }
        }
        return $ret;
    }

    public function downloadSubmissionFiles()
    {
        if (!$this->submission) {
            throw new Exception("Não há submission definido!");
        }

        $log = [];
        $nDownloadedFiles = 0;

        foreach ($this->submission->answers as $parent_id => $answers) {
            foreach ($answers as $ord => $a) {
                if (is_object($a->value) && $a->value->fileid) {
                    $val = $a->value;
                    $fileUrl = "{$this->BASE_URL}/pt/m/{$this->FONTES[$this->formId]->missao}/{$val->itemtype}/{$val->fileid}";
                    list($file, $fileHeaders) = $this->Utils->pega_xml_arquivo($fileUrl, 'raw', $this->extraHeaders, true);
                    if (!$fileHeaders["content-disposition"]) {
                        unset($this->submission->answers[$parent_id][$ord]);
                        $this->Utils->pE("Erro baixando o arquivo da Colheita! Arquivo não encontrado: " . json_encode($fileHeaders));
                        continue;
                    }

                    list($fileCmd, $filename) = explode(
                        "=",
                        explode(";", $fileHeaders["content-disposition"][0])[1]
                    );
                    $filename = substr($filename, 1, strlen($filename)-2);
                    $filename = str_replace(
                        ["elmo", ".jpeg"],
                        ["frm_{$this->FONTES[$this->formId]->tipo}", ".jpg"],
                        strtolower($filename)
                    );

                    $arq = $this->uploadPath . $filename;

                    $currentLog = "Arquivo {$filename} já disponível no nosso servidor.";
                    if (!file_exists($arq) || filesize($arq) == 0) {
                        list($file, $fileHeaders) = $this->Utils->pega_xml_arquivo($fileUrl, 'raw', $this->extraHeaders);
                        $h = fopen($arq, "w+");
                        fputs($h, $file);
                        fclose($h);
                        $currentLog = "Arquivo {$filename} baixado com sucesso.";
                        $nDownloadedFiles++;
                    }
                    $log[] = $currentLog;
                    unset($val->fileid, $val->filetype);
                    $val->arq = $filename;

                    // Updates current submission value for the new attachment filename stored locally
                    $this->submission->answers[$parent_id][$ord]->value = $val;
                    // $this->submissions[$this->submission->id]->answers[$parent_id][$ord]->value = $val;
                }
            }
        }

        return (object) [
            "log" => $log,
            "nDownloadedFiles" => $nDownloadedFiles
        ];
    }
}
