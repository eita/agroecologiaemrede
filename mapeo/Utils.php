<?php

namespace EITA;

use \mysqli;
use \Exception;

/**
 * All methods imported from deprecated functions.php
 */
class Utils
{
    public $db;
    public $umDia = 24*3600;

    function __construct($bd)
    {
        $this->conecta($bd);
    }

    function __destruct()
    {
        $this->db->close();
    }

    public function criptografa($texto, $chave)
    {
        $result = '';
        for ($i=1; $i<=strlen($texto); $i++) {
            if (ord($texto[$i-1])>126) {
                return 0;
            }
            $char = $texto[$i-1];
            $chavechar = $chave[($i % strlen($chave))-1];
            $tmp=ord($char)+ord($chavechar)-126;
            if ($tmp<1) {
                $tmp=$tmp+126;
            }
            //echo ord($char).': '.$char.' -- '.ord($chavechar).': '.$chavechar.' -- fim='.$tmp.' -- '.chr(32).'<br>';
            $char = chr($tmp);
            $result.=$char;
        }
        return $result;
    }

    public function decifra($texto, $chave)
    {
        $result = '';
        for ($i=1; $i<=strlen($texto); $i++) {
            $char = $texto[$i-1];
            $chavechar = $chave[($i % strlen($chave))-1];
            $tmp=ord($char)-ord($chavechar)+126;
            if ($tmp>126) {
                $tmp=$tmp-126;
            }
            //echo ord($char).': '.$char.' -- '.ord($chavechar).': '.$chavechar.' -- fim='.$tmp.' -- '.chr(32).'<br>';
            $char = chr($tmp);
            $result.=$char;
        }
        return $result;
    }




    /**
     * Formata números com o separador de milhar (".") e com o separador de decimais (",").
     * @method yLabelFormat
     */
    public function yLabelFormat($aLabel)
    {
        return number_format($aLabel, 0, ',', '.');
    }

    /**
     * Tira os slashes de matrizes
     * @method stripslashes_array
     */
    public function stripslashes_array($resultado)
    {
        if (is_array($resultado)) {
            $resultado=array_map('stripslashes_array', $resultado);
        } else {
            $resultado=stripslashes($resultado);
        }
        return $resultado;
    }

    /**
     * Faz o query e dispara um erro se der errado
     * @method query
     * @param  string  $sql    [description]
     * @param  string  $tipo   [description]
     * @param  string  $assoc  [description]
     * @param  boolean $encode [description]
     * @return [type]          Results
     */
    public function query(string $sql, $tipo = '', $assoc = '', bool $encode = false)
    {
        $res=$this->db->query($sql);
        if (!$res) {
            $error = "Erro MySQL: {$this->db->error}" . chr(10) . "Query: {$sql}" . chr(10);
            throw new Exception($error);
        }
        $retorno = null;
        switch ($tipo) {
            default:
                $retorno=$res;
                break;
            case 'array':
                $i=0;
                while ($q=$res->fetch_array()) {
                    if (!isset($keys) || !$keys) {
                        $keys=array_keys($q);
                    }
                    foreach ($keys as $k) {
                        $retorno[$k][$i]=$q[$k];
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'array_assoc':
                $i=0;
                while ($q=$res->fetch_array(MYSQLI_ASSOC)) {
                    if (!isset($keys)) {
                        $keys=array_keys($q);
                    }
                    foreach ($keys as $k) {
                        if ($k<>$assoc) {
                            $retorno[$q[$assoc]][$k]=$q[$k];
                        }
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'row':
            case 'rows':
                $i=0;
                while ($q=$res->fetch_row()) {
                    if (!isset($keys)) {
                        $keys=array_keys($q);
                    }
                    foreach ($keys as $k) {
                        $retorno[$k][$i]=$q[$k];
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'object':
                $i=0;
                $retorno = array();
                while ($q=$res->fetch_object()) {
                    $arr=get_object_vars($q);
                    if (!isset($keys) || !$keys) {
                        $keys=array_keys($arr);
                    }
                    $retorno[$i] = (object) [];
                    foreach ($keys as $k) {
                        $retorno[$i]->$k=$q->$k;
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'object_assoc':
                $i=0;
                $retorno = array();
                while ($q=$res->fetch_object()) {
                    $arr=get_object_vars($q);
                    if (!isset($keys)) {
                        $keys=array_keys($arr);
                    }
                    if (!isset($retorno[$q->$assoc])) {
                        $retorno[$q->$assoc] = (object) [];
                    }
                    foreach ($keys as $k) {
                        $retorno[$q->$assoc]->$k=$q->$k;
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'object_assoc_multiple':
                list($mainAssoc, $subAssoc) = explode("|", $assoc);
                $i=0;
                $retorno = array();
                while ($q=$res->fetch_object()) {
                    $arr=get_object_vars($q);
                    if (!isset($keys)) {
                        $keys=array_keys($arr);
                    }
                    $tmp = (object) [];
                    foreach ($keys as $k) {
                        $tmp->$k=$q->$k;
                    }
                    if (!isset($retorno[$q->$mainAssoc])) {
                        $retorno[$q->$mainAssoc] = [];
                    }
                    if ($subAssoc) {
                        if (!isset($retorno[$q->$mainAssoc][$q->$subAssoc])) {
                            $retorno[$q->$mainAssoc][$q->$subAssoc] = [];
                        }
                        $retorno[$q->$mainAssoc][$q->$subAssoc][] = $tmp;
                    } else {
                        $retorno[$q->$mainAssoc][] = $tmp;
                    }
                    $i++;
                }
                if (isset($enconde) && $encode) {
                    $retorno = $this->converte_charset2($retorno);
                }
                break;
            case 'num':
            case 'numrows':
            case 'num_rows':
                $retorno=$res->num_rows;
                break;
            case 'id':
                $retorno=$this->db->insert_id;
                break;
            case 'affected_rows':
                $retorno = $this->db->affected_rows;
                break;
        }
        return $retorno;
    }

    /**
     * Gera o mapa de campos a partir da tabela mapa_campos
     * @method gera_mapa_campos
     * @param  [type]           $ano            [description]
     * @param  [type]           $nomefrm        [description]
     * @param  string           $tab_base_comum [description]
     * @param  [type]           $cmd            [description]
     * @param  string           $cmd_mapa       [description]
     */
    public function gera_mapa_campos($ano, $nomefrm, $tab_base_comum = '', &$cmd, &$cmd_mapa = '')
    {
        global $lingua;
        /*Obtenho os campos e características da tabela $tab armazenados no mapa_campos, e
            crio a variável objeto $mapa. Ela pode dar valores de duas maneiras:
            pelo índice na tabela mapa_campos ($mapa[1]->titulo),
            ou pelo campo específico ($mapa['Q1']->titulo) */
        $sql="select * from mapeo_mapa_campos where ano='".$ano."' AND (frm='".$nomefrm."'";
        if ($tab_base_comum) {
            $sql .= " OR frm='".$tab_base_comum."'";
        }
        $sql .= ") ORDER BY ord";
        $mapa = $this->query($sql, 'object_assoc', 'campo');
        /* A partir da $mapa, obtenho o conjunto completo de comandos para todos os
            campos da tabela. Trata-se de uma variável objeto $cmd, de tipo array ainda por
            cima: $cmd[CAMPO]->FUNCAO=VALOR */
        $campos_lingua = array("titulo", "titulocurto", "bloco", "sub_bloco", "ajuda");
        foreach ($mapa as $i => $m) {
            if ($m->cmd) {
                $tmps=explode('|', $m->cmd);
                foreach ($tmps as $tmp) {
                    $tmp2=explode(':', $tmp);
                    if (!isset($cmd[$m->campo])) {
                        $cmd[$m->campo] = (object) [];
                    }
                    $cmd[$m->campo]->{$tmp2[0]}=$tmp2[1];
                }
                if (isset($cmd[$m->campo]->apoio) && $cmd[$m->campo]->apoio) {
                    if (!isset($cmd[$m->campo]->campo_nome)) {
                        $cmd[$m->campo]->campo_nome='nome';
                    }
                    if (!isset($cmd[$m->campo]->campo_id)) {
                        $cmd[$m->campo]->campo_id='id';
                    }
                }
            }
            if ($m->cmd_mapa) {
                $tmps=explode('|', $m->cmd_mapa);
                foreach ($tmps as $tmp) {
                    $tmp2=explode(':', $tmp);
                    if (!isset($cmd_mapa[$m->campo])) {
                        if (!$cmd_mapa) {
                            $cmd_mapa = array();
                        }
                        $cmd_mapa[$m->campo] = (object) [];
                    }
                    $cmd_mapa[$m->campo]->{$tmp2[0]}=$tmp2[1];
                }
            }
            if ($lingua) {
                foreach ($campos_lingua as $cmpo_lng) {
                    $mapa[$i]->$cmpo_lng = $this->traduz($mapa[$i]->$cmpo_lng, $lingua);
                }
            }
        }
        return $mapa;
    }


    // Pega um texto e dá a sua resposta na língua $lingua.
    // O texto tem que estar no seguinte formato: "lng1:txtlng1|lng2:txtlng2|lng3:txtlng3" etc.
    // Se não estiver neste formato ou não houver a língua $lingua, devolve simplesmente o primeiro txt
    public function traduz($txt, $lingua = '')
    {
        $tmps = explode('|', $txt);
        unset($tit);
        foreach ($tmps as $i => $tmp) {
            $tmp2 = explode(':', $tmp);
            if (strlen($tmp2[0])>5) { // pra evitar considerar dois pontos de texto normal
                $tit[$i]=$tmp;
            } elseif (isset($tmp2[1])) {
                $tit[$tmp2[0]]=str_replace($tmp2[0].':', '', $tmp);
            } elseif (!in_array($tmp2[0], array($lingua,substr($lingua, 0, 2)))) {
                $tit[$i]=$tmp2[0];
            } else {
                $tit[$i]='';
            }
        }
        $keys = array_keys($tit);
        if ($lingua) {
            if (!isset($tit[$lingua])) {
                $txt = (isset($tit[substr($lingua, 0, 2)]))
                    ? $tit[substr($lingua, 0, 2)]
                    : $tit[$keys[0]];
            } else {
                $txt = $tit[$lingua];
            }
        } else {
            $txt=$tit[$keys[0]];
        }
        return $txt;
    }


    //*
    //*
    //* Faz options para determinada variável ou tabela "$fonte"
    //*
    //*
    public function faz_select($fonte, $campo_id, $padrao = '', $campo_nome = '', $order = '', $where = '', $html = true, $limite = 70)
    {
        global $bd, $lingua, $padrao_lingua;
        if (!isset($lingua) || !$lingua) {
            $lingua = $padrao_lingua;
        }
        if (!$order) {
            $order=$campo_nome;
        }
        if (!$campo_nome) {
            $campo_nome=$campo_id;
        }
        if ($where) {
            $where = str_replace('{campo_id}', $campo_id, $where);
            $where = str_replace('{campo_nome}', $campo_nome, $where);
        }
        // Aqui eu faço a varredura numa tabela mysql se a variável $fonte for uma string de texto, pois significa que ela é um nome de uma tabela.
        if (!is_array($fonte)) {
            $sql="SELECT ".$campo_id.", ".$campo_nome.", ".$order." as ord FROM ".$fonte;
            $sql.=" WHERE $campo_nome IS NOT NULL";
            if ($where) {
                $sql .=" AND ($where)";
            }
            $sql.=" ORDER BY ".$order.";";
            $fontetmp = $this->query($sql, 'array');
            // Aqui faço o ordenamento mais complexo, pois pode ser que a tabela tenha traduções, o que faria falhar o "order by" no query do mysql:
            $fonte=array();
            foreach ($fontetmp['ord'] as $i => $txt) {
                $fontetmp['ord'][$i]=$this->traduz($txt, $lingua);
            }
            asort($fontetmp['ord']);
            $j=0;
            foreach ($fontetmp['ord'] as $i => $txt) {
                $fonte[$campo_id][$j]=$this->traduz($fontetmp[$campo_id][$i], $lingua);
                if ($campo_nome!=$campo_id) {
                    $fonte[$campo_nome][$j]=$this->traduz($fontetmp[$campo_nome][$i], $lingua);
                }
                $j++;
            }
        }
        if (!$fonte) {
            return '';
        }
        for ($i=0; $i<count($fonte[$campo_id]); $i++) {
            if ($limite && strlen($this->traduz($fonte[$campo_nome][$i], $lingua))>$limite) {
                $fonte[$campo_nome][$i] = substr($fonte[$campo_nome][$i], 0, $limite).'...';
            }
            if ($fonte[$campo_id][$i]===0) {
                $fonte[$campo_id][$i]='0';
            }
            if ($html) {
                $select .= '<option value="'.$fonte[$campo_id][$i].'"';
                if ($fonte[$campo_id][$i]==$padrao) {
                    $select .= ' selected="selected"';
                }
                $select .= '>'.$this->traduz($fonte[$campo_nome][$i], $lingua).'</option>'.chr(10);
            } else {
                $select[$fonte[$campo_id][$i]]=$this->traduz($fonte[$campo_nome][$i], $lingua);
            }
        }
        //print_r($select);
        return $select;
    }



    //*
    //*
    //* Faz a conexão no banco de dados
    //*
    //*
    public function conecta($bd)
    {
        $db = new \mysqli($bd['servidor'], $bd['usuaria'], $bd['senha'], $bd['agrorede']);
        if ($db->connect_errno) {
            printf("Connect failed: %s\n", $db->connect_error);
            exit();
        }
        // $db->set_charset('utf8');
        $db->set_charset('utf8mb4');
        $this->db = $db;
    }

    // Bobagens, para eu poder fingir que coloco dados, mas sem colocar de verdade...
    public function faz_query2($texto)
    {
        echo $texto.'<br>';
        return true;
    }
    public function mysql_insere_id()
    {
        return 456;
    }


    //*
    //*
    //* Elimina acentos de um texto.
    //*
    //*
    public function tira_acentos($texto)
    {
            $tab['sem_acentos']=array(192=>"A", 193=>"A", 194=>"A", 195=>"A", 196=>"A", 197=>"A", 198=>"A", 199=>"C", 200=>"E", 201=>"E", 202=>"E", 203=>"E", 204=>"I", 205=>"I", 206=>"I", 207=>"I", 208=>"O", 209=>"N", 210=>"O", 211=>"O", 212=>"O", 213=>"O", 214=>"O", 216=>"O", 217=>"U", 218=>"U", 219=>"U", 220=>"U", 221=>"Y", 224=>"a", 225=>"a", 226=>"a", 227=>"a", 228=>"a", 229=>"a", 230=>"a", 231=>"c", 232=>"e", 233=>"e", 234=>"e", 235=>"e", 236=>"i", 237=>"i", 238=>"i", 239=>"i", 241=>"n", 242=>"o", 243=>"o", 244=>"o", 245=>"o", 246=>"o", 248=>"o", 249=>"u", 250=>"u", 251=>"u", 252=>"u", 253=>"y", 255=>"y");
            $tab['com_acentos']=array(chr(192), chr(193), chr(194), chr(195), chr(196), chr(197), chr(198), chr(199), chr(200), chr(201), chr(202), chr(203), chr(204), chr(205), chr(206), chr(207), chr(208), chr(209), chr(210), chr(211), chr(212), chr(213), chr(214), chr(216), chr(217), chr(218), chr(219), chr(220), chr(221), chr(224), chr(225), chr(226), chr(227), chr(228), chr(229), chr(230), chr(231), chr(232), chr(233), chr(234), chr(235), chr(236), chr(237), chr(238), chr(239), chr(241), chr(242), chr(243), chr(244), chr(245), chr(246), chr(248), chr(249), chr(250), chr(251), chr(252), chr(253), chr(255));
            $texto=str_replace($tab['com_acentos'], $tab['sem_acentos'], $texto);
            return($texto);
    }

    public function slugify($str, $delimiter = '-')
    {
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', $this->tira_acentos(utf8_decode($str)))))), $delimiter));
        return $slug;
    }

    public function resgata_dados_usuario()
    {
        $sql = "SELECT * FROM usuario WHERE us_login='".$_COOKIE['cookieUsuario']."'";
        $dados = $this->query($sql, 'object');
        $ret = $dados[0];
        $sql = "SELECT count(log_usuario) as qtAcesso FROM log_acesso WHERE log_usuario='".$_COOKIE['cookieUsuario']."'";
        $dados = $this->query($sql, 'object');
        $ret->cnt = $dados[0]->qtAcesso;
        /*
            Os dados são os seguintes: $ret->us_login, $ret->us_senha, $ret->us_nome, $ret->us_admin, $ret->cnt
        */
        return $ret;
    }

    public function resgata_arvore($tab, $campo_id, $campo_nome, $campo_mae, $id = 0, $nivel = 0, $parentes = '0')
    {
        global $lingua;
        $tab_parentes = explode('|', $parentes);
        if (!in_array($id, $tab_parentes)) {
            $parentes .= '|'.$id;
        }
        $sql = 'SELECT '.$campo_id.', '.$campo_nome.', '.intval($nivel).' as nivel FROM '.$tab.' WHERE '.$campo_mae.'="'.$id.'" ORDER BY '.$campo_nome;
        $dados = $this->query($sql, 'object');
        if ($dados) {
            foreach ($dados as $dado) {
                $dado->parentes = $parentes;
                $dado->$campo_nome = $this->traduz($dado->$campo_nome, $lingua);
                $rets[]=$dado;
                $tmps = $this->resgata_arvore($tab, $campo_id, $campo_nome, $campo_mae, $dado->$campo_id, $nivel+1, $parentes);
                if ($tmps) {
                    foreach ($tmps as $i => $tmp) {
                        $rets[]=$tmp;
                    }
                }
            }
        } else {
            return false;
        }
        return $rets;
    }

    public function pega_xml_arquivo($url, $format = "xml", $extraHeaders = [], $nobody = false)
    {
        $userAgent = ($_SERVER['HTTP_USER_AGENT'])
            ? $_SERVER['HTTP_USER_AGENT']
            : "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13";
        $data = null;
        $headers = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($nobody) {
            curl_setopt($ch, CURLOPT_NOBODY, 1);
        }
        if (count($extraHeaders)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $extraHeaders);
        }
        curl_setopt(
            $ch,
            CURLOPT_HEADERFUNCTION,
            function ($curl, $header) use (&$headers) {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) { // ignore invalid headers
                    return $len;
                }
                $headers[strtolower(trim($header[0]))][] = trim($header[1]);
                return $len;
            }
        );
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data) {
            switch ($format) {
                case 'xml':
                    $data = simplexml_load_string(utf8_encode($data));
                    break;
                case 'json':
                    $data = json_decode($data);
                    break;
            }
        }
        return ($data || $nobody) ? array($data, $headers) : null;
    }

    public function resgata_filtros($tipofrm, $selsCamposGenericos = false)
    {
        global $campo, $txt;
        $sql = "SELECT * FROM mapeo_formularios WHERE tipo='".$tipofrm."'";
        $frms = $this->query($sql, 'object');

        $frm = $frms[0];
        // A variável $filtro->tab contem a tabela mais básica de um determinado tipo de formulário. Cada tipo de formulário tem uma tabela de base que é comum a todos os formulários, o que permite um mapa comum de ser gerado. Isso é para os filtros, mas não necessariamente para os dados do formulário que aparecerão no balão, mas isso é outra história.
        $filtro = (object) [];
        $filtro->tab = (isset($frm->tab_base_comum) && $frm->tab_base_comum)
            ? $frm->tab_base_comum
            : $frm->nomefrm;

        $mapa = $this->gera_mapa_campos($frm->ano, $frm->nomefrm, $frm->tab_base_comum, $cmd, $cmd_mapa);

        // este gera o item para selecionar num determinado tipofrm que tenha mais de uma tabela
        /*if (count($frms)>1) {
            $tmp['nomefrm']->tipo_form = 'checkbox';
            $tmp['nomefrm']->campo = 'nomefrm';
            $tmp['nomefrm']->titulo = $txt['titulo_tipo_'.$tipofrm];
            $tmpc['nomefrm']->apoio = 'mapeo_formularios';
            $tmpc['nomefrm']->campo_id = 'nomefrm';
            $tmpc['nomefrm']->campo_nome = 'titulo';
            $tmpc['nomefrm']->where = 'tipo="'.$tipofrm.'"';
            $tmpcm['nomefrm']->filtro = 1;
            $mapa = $tmp + $mapa;
            $cmd = $tmpc + $cmd;
            $cmd_mapa = $tmpcm + $cmd_mapa;
        }*/

        foreach ($mapa as $m) {
            $cmdo_mapa = isset($cmd_mapa[$m->campo])
                ? $cmd_mapa[$m->campo]
                : (object) [];
            $cmdo = isset($cmd[$m->campo])
                ? $cmd[$m->campo]
                : (object) [];
            $cmdo->nmax_opcoes = 2000;
            // Um eventual where do $cmdo_mapa tem primazia sobre o where do $cmdo:
            if (isset($cmdo_mapa->where) && $cmdo_mapa->where) {
                $cmdo->where = $cmdo_mapa->where;
            }
            // Aqui eu transformo "radios" em "checkboxes" para o filtro permitir interseções:
            if ($mapa[$m->campo]->tipo_form=='radio') {
                $mapa[$m->campo]->tipo_form='checkbox';
            }
            unset($cmdo->admin, $cmdo->ignorar, $mapa[$m->campo]->regras);
            if (isset($cmdo_mapa->geraXML) && $cmdo_mapa->geraXML) {
                foreach ($campo[$filtro->tab] as $obj_nome => $obj_campo) {
                    if ($obj_campo == $m->campo) {
                        $filtro->geraXML[$m->campo] = $obj_nome;
                        break;
                    }
                }
                if (!isset($filtro->geraXML[$m->campo])) {
                    $filtro->geraXML[$m->campo] = ($mapa[$m->campo]->titulocurto)
                        ? $mapa[$m->campo]->titulocurto
                        : $mapa[$m->campo]->titulo;
                }
            }
            if (isset($cmdo_mapa->info) && $cmdo_mapa->info) {
                $filtro->info[$m->campo] = ($m->titulocurto) ? $m->titulocurto : $m->titulo;
            }
            if (isset($cmdo_mapa->filtro) && $cmdo_mapa->filtro) {
                $filtro->filtro[] = $m->campo;
                if (isset($cmdo->apoio) && $cmdo->apoio) {
                    $otmp = isset($cmdo->order) ? $cmdo->order : '';
                    $wtmp = isset($cmdo->where) ? $cmdo->where : '';
                    $filtro->opcoes[$m->campo] = faz_select($cmdo->apoio, $cmdo->campo_id, '', $cmdo->campo_nome, $otmp, $wtmp, false);
                }

                // Este "campoGenerico" serve para permitir filtros inter-conceitos (dispositivos, redes, organizações, experiências, etc), a partir do config.php com a variável "campo"
                $campoGenerico = $m->campo;
                foreach (get_object_vars($campo[$frm->nomefrm]) as $k => $v) {
                    if ($m->campo == $v) {
                        if (isset($_REQUEST[$k]) || isset($_REQUEST[$k.'_0'])) {
                            $campoGenerico = $k;
                        }
                    }
                }

                // dtygel: hack para forçar o nível zero a ser sempre Brasil, por enquanto...
                // TODO: voltar a mostrar os países.
                /*if ($mapa[$m->campo]->tipo_form=='georref' && !(isset($_REQUEST[$campoGenerico.'_0']) && $_REQUEST[$campoGenerico.'_0']) && !(isset($_REQUEST[$campoGenerico]) && $_REQUEST[$campoGenerico])) {
                    $_REQUEST[$campoGenerico.'_0'] = 'BR';
                }*/

                // resgate do que pode ter vindo por REQUEST:
                $key = ($selsCamposGenericos) ? $campoGenerico : $m->campo;
                if (in_array($mapa[$m->campo]->tipo_form, array('georref','select_hierarquia')) && isset($_REQUEST[$campoGenerico.'_0']) && $_REQUEST[$campoGenerico.'_0']) {
                    for ($i=0; $i<10; $i++) {
                        if (isset($_REQUEST[$campoGenerico.'_'.$i]) && $_REQUEST[$campoGenerico.'_'.$i]) {
                            $filtro->sels[$key][$i] = $_REQUEST[$campoGenerico.'_'.$i];
                        } else {
                            if (isset($_REQUEST[$campoGenerico]) && $_REQUEST[$campoGenerico]) {
                                $filtro->sels[$key][$i] = $_REQUEST[$campoGenerico];
                            }
                            break;
                        }
                    }
                } elseif (isset($_REQUEST[$campoGenerico]) && $_REQUEST[$campoGenerico]) {
                    $filtro->sels[$key] = (is_array($_REQUEST[$campoGenerico]))
                        ? $_REQUEST[$campoGenerico]
                        : explode('|', $_REQUEST[$campoGenerico]);
                }
            } elseif ((!isset($cmdo_mapa->info) || !$cmdo_mapa->info) && (!isset($cmdo_mapa->geraXML) || !$cmdo_mapa->geraXML)) {
                unset($mapa[$m->campo], $cmd[$m->campo]);
            }
        }
        $filtro->textoBusca = (isset($_REQUEST['textoBusca']))
            ? $_REQUEST['textoBusca']
            : '';
        $filtro->tipofrmBusca = (isset($_REQUEST['tipofrm']) && $_REQUEST['tipofrm'] != 'todos')
            ? explode('|', $_REQUEST['tipofrm'])
            : '';
        $filtro->mapa=$mapa;
        $filtro->cmd=$cmd;
        $filtro->cmd_mapa=$cmd_mapa;
        return $filtro;
    }

    public function organiza_geos($id = false, $distmax = false, $lingua = 'pt-br')
    {
        if ($id) {
            $geo = (object) [
                "pais" => (object) [
                    "id" => substr($id, 0, 2)
                ]
            ];
            $sql = "SELECT nome FROM __paises WHERE id='{$geo->pais->id}'";
            $res = $this->query($sql, 'rows');
            $geo->pais->nome = $this->traduz($res[0][0], $lingua);
            if (strlen($id)>=5) {
                $geo->estado = (object) [
                    "id" => substr($id, 0, 5),
                    "nome" => ""
                ];
                $sql = "SELECT id, nome FROM _".$geo->pais->id."_maes WHERE id='".$geo->estado->id."'";
                if ($geo->pais->id=='BR') {
                    $sql .= " OR id2='{$geo->estado->id}'";
                }
                $res = $this->query($sql, 'object');
                $geo->estado->nome = $this->traduz($res[0]->nome, $lingua);
                $geo->estado->id = $res[0]->id;
                if ($geo->pais->id=='BR') {
                    $geo->estado->UF = $this->estadoParaUF($geo->estado->nome);
                }
                //echo $res[0][0];exit;
            }
            if (strlen($id)==9) {
                $geo->microrregiao = (object) [
                    "id" => $id
                ];
                $sql = "SELECT nome FROM _{$geo->pais->id}_microrregioes WHERE idmicro='{$geo->microrregiao->id}'";
                $res = $this->query($sql, 'rows');
                $geo->microrregiao->nome = $this->traduz($res[0][0], $lingua);
                $geo->label = "{$geo->microrregiao->nome}, {$geo->estado->nome}";
            } elseif (strlen($id)==11) {
                $sql = "SELECT id, nome, lat, lng FROM _{$geo->pais->id} WHERE id='{$id}'";
                if ($geo->pais->id=='BR') {
                    $sql .= " OR id2='{$id}'";
                }
                $res = $this->query($sql, 'object');
                $geo->cidade = (object) [
                    "id" => $res[0]->id,
                    "nome" => $this->traduz($res[0]->nome, $lingua),
                    "lat" => $res[0]->lat,
                    "lng" => $res[0]->lng
                ];
                $geo->label = "{$geo->cidade->nome}, {$geo->estado->nome}";
                if ($geo->pais->id=='BR') {
                    $geo->labelCurto = "{$geo->cidade->nome} ({$geo->estado->UF})";
                }
            }
            $geo->id = $id;
            $geo->nome = (isset($geo->cidade) && isset($geo->cidade->id))
                ? ($geo->cidade->nome)
                : ($geo->microrregiao->id
                    ? ($geo->microrregiao->nome)
                    : ($geo->estado->id
                        ? ($geo->estado->nome)
                        : $geo->pais->nome
                    )
                );
            $geo->precisao = $geo->cidade->id
                ? 'cidade'
                : ($geo->microrregiao->id
                    ? 'microrregiao'
                    : ($geo->estado->id
                        ? 'estado'
                        : 'pais'
                    )
                );
            if ($geo->cidade->id) {
                $geo->distmax = $distmax;
            }
        }
        return $geo;
    }

    public function truncate($text, $chars = 25, $ellipses = "(...)")
    {
        if (strlen($text) <= $chars) {
            return $text;
        }
        $text = $text." ";
        $text = substr($text, 0, $chars-strlen($ellipses));
        $text = substr($text, 0, strrpos($text, ' '));
        $text = $text.$ellipses;
        return $text;
    }

    public function my_filesize($file)
    {
        if (is_file($file)) {
            $size = filesize($file);
            return readable_filesize($size);
        }
    }

    public function readable_filesize($size)
    {
        $kb = 1024;         // Kilobyte
        $mb = 1024 * $kb;   // Megabyte
        $gb = 1024 * $mb;   // Gigabyte
        $tb = 1024 * $gb;   // Terabyte
        if ($size < $kb) {
            return number_format($size, 2, ",", ".")." B";
        }
        if ($size < $mb) {
            return number_format(round($size/$kb, 2), 2, ",", ".")." KB";
        }
        if ($size < $gb) {
            return number_format(round($size/$mb, 2), 2, ",", ".")." MB";
        }
        if ($size < $tb) {
            return number_format(round($size/$gb, 2), 2, ",", ".")." GB";
        }

        return number_format(round($size/$tb, 2), 2, ",", ".")." TB";
    }

    public function imagecreatefromfile($filename)
    {
        if (!file_exists($filename)) {
            return false;
        }
        switch (strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
            case 'jpeg':
            case 'jpg':
                return array(
                        'img' => imagecreatefromjpeg($filename),
                        'type' => 'jpg'
                    );
            break;
            case 'png':
                return array(
                    'img' => imagecreatefrompng($filename),
                    'type' => 'png'
                );
            break;
            case 'gif':
                return array(
                    'img' => imagecreatefromgif($filename),
                    'type' => 'gif'
                );
            break;
            case 'bmp':
                return array(
                    'img' => imagecreatefrombmp($filename),
                    'type' => 'bmp'
                );
            break;
            default:
                return false;
            break;
        }
    }

    public function getResizedImgURL($arq, $width = 800, $height = 800)
    {
        global $dir;
        $img = pathinfo($arq);
        $img['extension'] = strtolower($img['extension']);
        if (!in_array($img['extension'], array('gif','jpg','jpeg','png','webp'))) {
            return $dir['upload'].$arq;
        }
        $s = $width."x".$height;
        $img['path']=$img['filename'].'-'.$s.'.'.strtolower($img['extension']);
        if (!file_exists($dir['upload'].$img['path'])) {
            $img['url'] = "{$dir['apoio_URL']}get_image.php?s={$s}&img={$arq}";
        } else {
            $img['url'] = $dir['upload_URL'] . $img['path'];
        }
        return $img['url'];
    }

    public function sql_distancia_geo($pos, $lat = 'localizacao_lat', $lng = 'localizacao_lng')
    {
        return "((acos(sin(".$pos->lat."*pi()/180) * sin(".$lat."*pi()/180) + cos(".$pos->lat."*pi()/180) * cos(".$lat."*pi()/180) * cos((".$lng." - ".$pos->lng.")*pi()/180)))*6378.7)";
    }

    public function pR($t, $exit = false)
    {
        echo "<hr><pre>";
        print_r($t);
        echo "</pre>";
        if ($exit) {
            exit;
        }
    }

    public function pE($t)
    {
        $this->pT($t, "error");
    }
    public function pT($t, $style = "p")
    {
        switch ($style) {
            case 'h1':
                $t = (is_string($t))
                    ? PHP_EOL.PHP_EOL.strtoupper($t).PHP_EOL.str_repeat('=', strlen($t)).PHP_EOL
                    : $t;
                break;
            case 'h2':
                $t = (is_string($t))
                    ? PHP_EOL . str_repeat('-', 10) . PHP_EOL . $t
                    : $t;
                break;
            case 'error':
                $t = (is_string($t))
                    ? "\033[31m {$t} \033[0m"
                    : $t;
                break;
            case 'p':
            default:
                break;
        }
        print_r($t);
        echo PHP_EOL;
    }

    ////////////////////////////////
    //THESE SHOULD NEVER BE HERE ;-(
    ////////////////////////////////
    public function mostraTabela($filtro, $xml, $tipofrm, $numpts, $numpgs, $pgAtual)
    {
        global $dir, $txt;
    }

    public function faz_indice_tabela($numpts, $numpgs, $pgAtual, $id)
    {
        global $padrao_itens_por_pagina;
    }

    public function formata_dados_xml_agrorede($xml, $visao)
    {
        global $campo;
        if (!$xml && !is_array($xml)) {
            return null;
        }

        foreach ($xml as $xtmp) {
            /*$numpts+= ($visao=='mapa')
                ? $xtmp->num['num']
                : $xtmp->markers->pt['num'];*/
                $numpts+= (isset($xtmp->markers->pt['num']))
                    ? $xtmp->markers->pt['num']
                    : $xtmp->num['num'];
            // E aqui eu recolho as categorias (para checkboxes dinâmicos), caso existam:
            if ($xtmp->config['cats']) {
                $tmp=explode('||', $xtmp->config['cats']);
                foreach ($tmp as $tm) {
                    $tmp2=explode('|', $tm);
                    if (!isset($ativ)) {
                        $ativ = array();
                    }
                    $ativ[$tmp2[0]]=$tmp2[1];
                }
            }
            $numpgs = $xtmp->num['pgs'];

            $pts = [];
            $i=0;
            foreach ($xtmp->markers->pt as $pt) {
                $t = (object) [];
                foreach ($pt->attributes() as $k => $v) {
                    $decoded = json_decode((string)$v);
                    if (!$decoded) {
                        $vs = explode('|', (string)$v);
                        if (count($vs) == 1) {
                            $vs = $vs[0];
                        }
                        $t->$k = $vs;
                    } else {
                        $t->$k = $decoded;
                    }
                }
                    $pts[$i] = $t;
                    $i++;
            }
        }
        $t = (object) [
            "numpts" => $numpts,
            "numpgs" => $numpgs,
            "ativ" => isset($ativ) ? $ativ : null,
            "xml" => $xml,
            "pts" => $pts
        ];
        return $t;
    }

    /*function mostraSelecaoAtual($filtro) {
        ?>
        <div class="col-md-12">
            <div class="alert alert-light text-muted border border-primary">
                        <div class="row">
                            <div class="col-md-12">
                                <h5>Seleção atual</h5>
                                <?php if((isset($filtro->textoBusca) && $filtro->textoBusca) || $filtro->sels) { ?>
                                <ul class="list-inline">
                                    <?php if (isset($filtro->textoBusca) && $filtro->textoBusca) { ?>
                                        <li class="list-inline-item">
                                            <strong>Busca textual</strong>: <small>"<?= $filtro->textoBusca ?>"</small>
                                        </li>
                                    <?php } ?>
                                    <?php if (isset($filtro->sels)) { ?>
                                        <?php foreach($filtro->sels as $campo=>$valores) { ?>
                                            <li class="list-inline-item">
                                                <strong><?=($filtro->mapa[$campo]->titulocurto)?$filtro->mapa[$campo]->titulocurto:$filtro->mapa[$campo]->titulo?></strong>: <small><?= implode(', ', getNomesItensSelecionados($valores, $filtro->mapa[$campo]->tipo_form, $filtro->opcoes[$campo]))?></small>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                                <?php } else { ?>
                                <strong>Nenhum filtro selecionado</strong>
                                <?php } ?>
                            </div>
                        </div>
            </div>
        </div>
        <?php
    }*/

    public function mostraSelecaoAtual($qtde, $filtro)
    {
        global $campo, $txt;
        ob_start();
        ?>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <span id="badge-qtdeResultados" class="badge badge-pill">
                            <?= $qtde ?> resultados
                        </span>
                    </li>
                    <?php if (isset($filtro->textoBusca) && $filtro->textoBusca) { ?>
                        <li class="list-inline-item">
                            <span id="textoBusca_0" class="badge badge-pill badge-textoBusca">"<?= $filtro->textoBusca ?>"</span><span aer-target-id="textoBusca_0" aer-value="<?= $filtro->textoBusca ?>" class="fa fa-times-circle badge-fechar"></span>
                        </li>
                    <?php } ?>
                    <?php if (isset($filtro->tipofrmBusca) && $filtro->tipofrmBusca) { ?>
                        <?php foreach ($filtro->tipofrmBusca as $i => $tipofrm) { ?>
                            <li class="list-inline-item">
                                <span id="tipofrm_<?=$i?>" class="badge badge-pill badge-tipofrm">
                                    <?= $txt[$tipofrm] ?>
                                </span>
                                <span aer-target-id="tipofrm_<?=$i?>" aer-value="<?= $tipofrm ?>" class="fa fa-times-circle badge-fechar"></span>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php if (isset($filtro->sels)) { ?>
                        <?php foreach ($filtro->sels as $campoGenerico => $valores) { ?>
                            <?php $campoEspecifico = $campo[$filtro->tab]->$campoGenerico; ?>
                            <?php $badges = getNomesItensSelecionados($valores, $filtro->mapa[$campoEspecifico]->tipo_form, $filtro->opcoes[$campoEspecifico]); ?>
                            <li class="list-inline-item">
                                <?php foreach ($badges as $i => $badge) { ?>
                                    <span id="<?= "{$campoGenerico}_{$i}" ?>" class="badge badge-pill badge-<?= $campoGenerico ?>">
                                        <?= $badge ?>
                                    </span>
                                    <span aer-target-id="<?= "{$campoGenerico}_{$i}" ?>" aer-value="<?= $valores[$i] ?>" class="fa fa-times-circle badge-fechar"></span>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    public function getNomesItensSelecionados($valores, $tipo, $opcoes)
    {
        $valoresNome = array();
        switch ($tipo) {
            case 'georref':
                foreach ($valores as $v) {
                    $geo = $this->organiza_geos($v);
                    $valoresNome[] = $this->estadoParaUF($geo->nome);
                }
                break;
            default:
                foreach ($valores as $v) {
                    $valoresNome[] = $opcoes[$v];
                }
                break;
        }
        return $valoresNome;
    }

    public function estadoParaUF($estado)
    {
        $t = array('acre'=>'ac','alagoas'=>'al','amapá'=>'ap','amazonas'=>'am','bahia'=>'ba','ceará'=>'ce','distrito federal'=>'df','espírito santo'=>'es','goiás'=>'go','maranhão'=>'ma','mato grosso'=>'mt','mato grosso do sul'=>'ms','minas gerais'=>'mg','pará'=>'pa','paraíba'=>'pb','paraná'=>'pr','pernambuco'=>'pe','piauí'=>'pi','rio de janeiro'=>'rj','rio grande do norte'=>'rn','rio grande do sul'=>'rs','rondônia'=>'ro','roraima'=>'rr','santa catarina'=>'sc','são paulo'=>'sp','sergipe'=>'se','tocantins'=>'to');
        $ret = (isset($t[strtolower($estado)]))
            ? strtoupper($t[strtolower($estado)])
            : $estado;
        return $ret;
    }

    public function pega_metadados()
    {
        global $dir,$txt;
        $queriesRaw = explode('&', $_SERVER['QUERY_STRING']);
        $queries = array();
        foreach ($queriesRaw as $qq) {
            $q = explode('=', $qq);
            $queries[$q[0]]=$q[1];
        }
        $titulo = 'Agroecologia em Rede';
        $titulocurto = 'AeR';
        $descricao = 'O Agroecologia em Rede é um sistema de informações da Agroecologia, vinculado aos processos de construção e articulação do movimento agroecológico nos territórios. Nele você pode encontrar, cadastrar e comentar experiências, grupos, coletivos, redes e pessoas relacionados à Agroecologia, tanto em lista como no mapa. Para facilitar as buscas, é possível filtrar por localização, áreas temáticas, identidades, busca livre, entre outros mecanismos... Tudo de forma leve, simples e acessível, tanto para computadores como celulares e tablets com baixo acesso a internet!';
        $script = str_replace($dir['base'], '', $_SERVER['SCRIPT_FILENAME']);
        $url = $dir['base_URL'].$script."?".$_SERVER['QUERY_STRING'];
        $m = (object) [
            "url" => $url,
            "description" => $descricao,
            "title" => $titulo,
            "image" => $dir['base_URL'].'imagens/logoAeR_400.png'
        ];
        switch ($script) {
            case 'experiencias.php':
                if (isset($queries['experiencia']) && $queries['experiencia']) {
                    $sql = 'select * from frm_exp_base_comum where ex_id="'.$queries['experiencia'].'"';
                    $r = $this->query($sql, 'object')[0];
                    if ($r->ex_descricao) {
                        $m->title = $titulocurto.': Conheça a experiência "'.strtolower(stripslashes($r->ex_descricao)).'"';
                        $m->description = stripslashes(strip_tags($this->truncate($r->ex_resumo, 250)));
                        $arqs=array();
                        if ($r->ex_anexos) {
                            $anexos = explode('|', $r->ex_anexos);
                            if ($anexos) {
                                foreach ($anexos as $arq) {
                                    if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
                                        $arqs[]=$arq;
                                    }
                                }
                            }
                            if (count($arqs)) {
                                $arq =  $arqs[0];
                                $m->image = $dir['upload'].$arq;
                            }
                        }
                    }
                }
                break;
            case 'instituicoes.php':
                if (isset($queries['inst']) && $queries['inst']) {
                    $sql = 'select * from instituicao where in_id="'.$queries['inst'].'"';
                    $r = $this->query($sql, 'object')[0];
                    if ($r->in_nome) {
                        $m->title = $titulocurto.': Conheça a instituição ou grupo '.stripslashes(strip_tags($r->in_nome));
                    }
                }
                break;
            case 'usuarios.php':
                if (isset($queries['usu']) && $queries['usu']) {
                    $sql = 'select * from usuario where us_login="'.$queries['usu'].'"';
                    $r = $this->query($sql, 'object')[0];
                    if ($r->us_nome) {
                        $m->title = $titulocurto.': Conheça '.stripslashes(strip_tags($r->us_nome));
                    }
                }
                break;
            case 'usuarios_busca.php':
                break;
            case 'mapeo/index.php':
                switch ($queries['tipofrm']) {
                    case 'experiencias':
                        break;
                    case 'instituicoes':
                        break;
                }
                break;
        }
        return $m;
    }

    public function pegaValorHTML($valor, $cmd)
    {
        if (is_array($valor)) {
            $valor = implode('|', $valor);
        } elseif (is_object($valor)) {
            $valor = json_encode($valor);
        }
        if ($cmd->apoio) {
            $valor = str_replace("|", "', '", $valor);
            $fields = ($cmd->campo_url)
                ? $cmd->campo_nome.", ".$cmd->campo_url
                : $cmd->campo_nome;

            $sql = "select $fields from ".$cmd->apoio." where ".$cmd->campo_id." in ('$valor')";
            $ret = $this->query($sql, 'object');
            $html = [];
            foreach ($ret as $r) {
                $html[] = ($cmd->campo_url)
                    ? "<a href='".$r->{$cmd->campo_url}."'>".$r->{$cmd->campo_nome}."</a>"
                    : $r->{$cmd->campo_nome};
            }
            $valor = implode(", ", $html);
        }
        return $valor;
    }
    public function pegaImagens($imgsRaw, $cmd)
    {
        global $db;
        if ($cmd->apoio) {
            $imgsRaw = str_replace("|", "', '", $imgsRaw);
            $sql = "select ".$cmd->campo_nome.", ".$cmd->campo_url." from ".$cmd->apoio." where ".$cmd->campo_id." in ('$imgsRaw')";
            return query($sql, 'object');
        }
        return $valor;
    }

    /**
     * A method for sorting associative arrays by a key and a direction.
     * Direction can be ASC or DESC.
     *
     * @param $array
     * @param $key
     * @param $direction
     * @return mixed $array
     */
    public function sortAssociativeArrayByKey($array, $key, $direction)
    {
        global $campo;
        switch ($direction) {
            case "ASC":
                usort($array, function ($first, $second) use ($key) {
                    return $first->$key <=> $second->$key;
                });
                break;
            case "DESC":
                usort($array, function ($first, $second) use ($key) {
                    // pR($first->$key); pR($second->$key); pR($second->$key <=> $first->$key); exit;
                    return $second->$key <=> $first->$key;
                });
                break;
            default:
                break;
        }

        return $array;
    }

    public function stripTags($html)
    {
        $html = str_replace('<p', '<br><p', $html);
        $html = strip_tags($html, '<br><a>');
        return $html;
    }

    public function colheitaTraduz($json)
    {
        $titleRaw = json_decode($json);
        if (!$titleRaw) {
            return $json;
        }
        $title = $titleRaw->pt;
        if (!$title) {
            $title = $titleRaw->en;
            if (!$title) {
                foreach ($titleRaw as $translation) {
                    $title = $translation;
                    break;
                }
            }
        }
        return $title;
    }

    public function extraiLista($t)
    {
        if (is_array($t)) {
            return $t;
        }
        $tas = explode('|', $t);
        $nts = [];
        foreach ($tas as $i => $ta) {
            $tbs = explode(';', $ta);
            foreach ($tbs as $j => $tb) {
                $tcs = explode(',', $tb);
                foreach ($tcs as $j => $tc) {
                    $nts[] = $tc;
                }
            }
        }
        return $nts;
    }

    public function extraiCampoGenerico($tab, $key)
    {
        global $campo;
        $campoGenerico = $key;
        foreach (get_object_vars($campo[$tab]) as $k => $v) {
            if ($key == $v) {
                $campoGenerico = $k;
                break;
            }
        }
        return $campoGenerico;
    }

    public function extraiCampoGenericoSemTab($cmpo, $key)
    {
        $campoGenerico = $key;
        foreach (get_object_vars($cmpo) as $k => $v) {
            if ($key == $v) {
                $campoGenerico = $k;
                break;
            }
        }
        return $campoGenerico;
    }

    public function fichaFormataDados($frm, $id)
    {
        global $campo, $dir;
        $json = (object) [];
        $tab = ($frm->tab_base_comum)
            ? $frm->tab_base_comum
            : $frm->nomefrm;
        $cmpo = $campo[$tab];
        $sql = "SELECT * FROM `{$tab}` WHERE `{$cmpo->id}`='{$id}' AND {$cmpo->status} != 'R'";
        $res = $this->query($sql, 'object');
        if (!$res[0]) {
            $json->error = true;
            $json->msg = "Não encontrei " . $this->traduz($frm->tipo_nome) . " com id='{$id}'";
            return $json;
        }
        $raw = $res[0];

        $t = (object) [];
        foreach ($raw as $key => $val) {
            if ($key == $cmpo->tudo) {
                $t->tudo = json_decode($val);
            } else {
                $vals = explode('|', $val);
                foreach ($vals as $i => $v) {
                    $decoded = json_decode($v);
                    if ($decoded) {
                        $vals[$i] = $decoded;
                    }
                }
                if (count($vals) == 1) {
                    $vals = $vals[0];
                }
                $keyGenerico = extraiCampoGenerico($tab, $key);
                $t->$keyGenerico = $vals;
            }
        }
        $json = $t;

        // ABRANGÊNCIA:
        if ($json->abrangencia) {
            $sql = "SELECT * FROM lista_abrangencias WHERE id='{$json->abrangencia}'";
            $res = $this->query($sql, 'object');
            $json->abrangencia = ($res[0])
                ? $res[0]
                : null;
            if ($json->abrangencia) {
                $tipo = null;
                if ($json->abrangencia_comunidades) {
                    $lista = $this->extraiLista($json->abrangencia_comunidades);
                } elseif ($json->abrangencia_cidades) {
                    $lista = $this->extraiLista($json->abrangencia_cidades);
                    $tipo = 'cidade';
                } elseif ($json->abrangencia_estados) {
                    $lista = $this->extraiLista($json->abrangencia_estados);
                    $tipo = 'estado';
                } elseif ($json->abrangencia_paises) {
                    $lista = $this->extraiLista($json->abrangencia_paises);
                    $tipo = 'pais';
                } elseif ($json->abrangencia_macrorregioes) {
                    $lista = $this->extraiLista($json->abrangencia_macrorregioes);
                    $tipo = 'macrorregiao';
                } elseif ($json->abrangencia_outras) {
                    $lista = $this->extraiLista($json->abrangencia_outras);
                }
                $newLista = [];
                switch ($tipo) {
                    case 'cidade':
                    case 'estado':
                    case 'pais':
                        foreach ($lista as $l) {
                            $newLista[] = $this->organiza_geos($l);
                        }
                        break;
                    default:
                        $newLista = $lista;
                }
                $json->abrangencia->lista = $newLista;
            }
        }

        // ANEXOS:
        if ($json->anexos) {
            $anexos = is_array($json->anexos)
                ? $json->anexos
                : [$json->anexos];
            $newAnexos = [];
            foreach ($anexos as $arq) {
                if (is_object($arq)) {
                    $arquivo = $arq->arq;
                    $val = $arq;
                    if (!$val->nome) {
                        $val->nome = substr($arquivo, 0, 20);
                        if (strlen($arquivo) > 20) {
                            $val->nome .= '...';
                        }
                    }
                } else {
                    $arquivo = $arq;
                    $val = (object) [
                        "arq" => $arq
                    ];
                    if (file_exists($dir['upload'].$arquivo)) {
                        $val->mimetype = mime_content_type($dir['upload'].$arquivo);
                        $val->filesize = filesize($dir['upload'].$arquivo);
                    }
                }
                if (file_exists($dir['upload'].$arquivo) && filesize($dir['upload'].$arquivo)) {
                    if (is_array(getimagesize($dir['upload'].$arquivo))) {
                        $val->banner = $this->getResizedImgURL($arquivo, 1024, 800);
                        $val->card = $this->getResizedImgURL($arquivo, 400, 400);
                        $val->thumb = $this->getResizedImgURL($arquivo, 150, 150);
                    }
                }
                $anexoExiste = false;
                foreach ($newAnexos as $newAnexo) {
                    if ($newAnexo->arq == $val->arq) {
                        $anexoExiste = true;
                        break;
                    }
                }
                if (!$anexoExiste) {
                    $newAnexos[] = $val;
                }
            }
            $json->anexos = $newAnexos;
        } else {
            $json->anexos = [];
        }

        // FOTO(S) QUE REPRESENTA(M) O CADASTRO
        if ($json->foto) {
            $fotos = (is_array($json->foto))
                ? $json->foto
                : [$json->foto];
            $newFotos = [];
            foreach ($fotos as $f) {
                $t = $f;
                if (is_array(getimagesize($dir['upload'].$f->arq))) {
                    $t->banner = $this->getResizedImgURL($f->arq, 1024, 800);
                    $t->card = $this->getResizedImgURL($f->arq, 400, 400);
                    $t->thumb = $this->getResizedImgURL($f->arq, 150, 150);
                }
                $newFotos[] = $t;
            }
            $json->foto = $newFotos;
        }

        // LOGOMARCA DO CADASTRO
        if ($json->logo) {
            if (is_array(getimagesize($dir['upload'].$json->logo->arq))) {
                $json->logo->card = $this->getResizedImgURL($json->logo->arq, 400, 400);
                $json->logo->thumb = $this->getResizedImgURL($json->logo->arq, 150, 150);
            }
        }

        // CIDADE DE REFERÊNCIA:
        // $json->localizacao = $this->organiza_geos($json->localizacao);

        // DESCRIÇÃO:
        if ($json->descricao) {
            $json->descricao = str_replace('\\r\\n', '', $json->descricao);
        }

        // SITES RELACIONADOS AO CADASTRO:
        if ($json->sites) {
            $json->sites = is_array($json->sites)
                ? $json->sites
                : [$json->sites];
        }

        // ORGANIZAÇÃO DE REFERÊNCIA (APENAS DISPOSITIVO E EXPERIÊNCIA):
        if ($json->instituicao_referencia) {
            $sql = "SELECT a.in_id, a.in_nome, a.in_liberacao FROM `frm_instituicao` a WHERE `in_id`='{$json->instituicao_referencia}'";
            $res = $this->query($sql, 'object');
            if ($res[0]) {
                $json->instituicao_referencia = $res[0];
            } else {
                unset($json->instituicao_referencia);
            }
        }

        // ORGANIZAÇÕES RELACIONADAS (APENAS DISPOSITIVO E EXPERIÊNCIA):
        $instituicoes = is_array($json->instituicoes)
            ? $json->instituicoes
            : [$json->instituicoes];
        $sql = "SELECT a.in_id, a.in_nome, a.in_liberacao FROM `frm_instituicao` a WHERE `in_id` IN ('" . implode("', '", $instituicoes) . "')";
        $res = $this->query($sql, 'object_assoc', 'in_id');
        if (count($res) > 0) {
            $json->instituicoes = $res;
        } else {
            $json->instituicoes = [];
        }

        // REDES DAS QUAIS FAZ PARTE:
        if ($json->rede) {
            $sql = "SELECT `rede_id`, `rede_nome`, `rede_nome_curto`, `rede_liberacao` FROM `frm_rede` WHERE `rede_id` IN ('" . implode("'. '", $json->rede) . "')";
            $res = $this->query($sql, 'object');
            if (count($res) > 0) {
                $json->rede = $res;
            } else {
                unset($json->rede);
            }
        }

        return $json;
    }

    public function resgataEstadoId($estado)
    {
        $uf = $this->estadoParaUF($estado);
        $sql = "SELECT id FROM _BR_maes WHERE UF='{$uf}'";
        $res = $this->query($sql, 'object');
        return (count($res) > 0) ? $res[0]->id : null;
    }

    public function resgataPaisId($pais)
    {
        $sql = "SELECT id FROM __paises WHERE nome like '%:{$pais}|%'";
        $res = $this->query($sql, 'object');
        return (count($res) > 0) ? $res[0]->id : null;
    }

    public function resgataDadosMunicipio($municipioNome, $estadoNome)
    {
        $uf = $this->estadoParaUF($estadoNome);
        $municipioNome = mysqli_real_escape_string($this->db, $municipioNome);
        $municipio = null;
        if ($municipioNome && $uf) {
            $sql = "SELECT * FROM _BR WHERE UF='{$uf}' AND nome like '{$municipioNome}'";
            $res = $this->query($sql, 'object');
            // echo chr(10).$sql; print_r($res); exit;
            return (count($res) > 0) ? $res[0] : null;
        }
        return null;
    }

    public function prepareString($str)
    {
        return trim(mysqli_real_escape_string($this->db, $str));
    }
}
?>
