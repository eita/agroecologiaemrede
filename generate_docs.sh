#!/bin/bash

phpmd mapeo/ xml unusedcode,cleancode,codesize --reportfile build/pmd.xml

phpcs --standard=PSR2 --report=xml --exclude=Generic.Files.LineLength mapeo/*.php > build/phpcs.xml

phploc --exclude=build --exclude=acervo --exclude=docs --exclude=ETL --exclude=scripts --exclude=old_scripts --exclude=vendor --log-xml build/phploc.xml ./

phpdox
